function resizer( selectors, w ) {
	jQuery(selectors).each(function() {
		var owidth = jQuery(this).attr('width');
		var oheight = jQuery(this).attr('height');
		if (owidth > w) {
			var newH = (oheight * w / owidth);
			jQuery(this).attr({
				width: w,
				height: newH
			});
		}; // endif
	});
}