<?php get_header(); ?>

		<div class="container page content">
			<div class="row">
				<div class="span7 entry">
					<div class="headline">
						<h1 class="pagetitle"><?php the_title(); ?></h1>
					</div>
					<?php $c = 1; ?>
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
						<div class="post post-<? echo $c; ?>">
							<?php the_content(); ?>
							<?php comments_template(); ?>
						</div><!--END post-->
					<?php $c++; endwhile; ?>
					<?php endif; ?>
				</div><!--end span7 -->
				<div class="sidebar offset1 span4">
				<?php dynamic_sidebar('page'); ?>
				</div><!--END sidebar-->
			<div class="clr"></div>
		</div><!--end row -->
	</div><!--end container -->

<?php get_footer(); ?>