<?php
/*
Template Name: Full Width Template
*/
get_header(); ?>
	<div class="container page content full-width">
		<div class="row">
			<div class="span12">
				<div class="entry">
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
						<h1 class="lg"><?php the_title(); ?></h1>
						<?php the_content(); ?>
				<?php endwhile; ?>
				<?php endif; ?>
					</div><!--END entry-->				
			</div><!--end span12 -->
			<div class="clr"></div>
		</div><!--end row -->
	</div>
	
<?php get_footer(); ?>