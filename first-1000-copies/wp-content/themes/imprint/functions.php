<?php
automatic_feed_links();
if (! function_exists('imprint_register_sidebars')) {
	function imprint_register_sidebars() {
		register_sidebar(array(
			'name' => 'Blog Sidebar',
			'id' => 'blog',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'before_title' => '<h3 class="widgettitle"><span class="inside">',
			'after_title' => '</span></h3>',
			'after_widget' => '</div>',
		));
		register_sidebar(array(
			'name' => 'Page Sidebar',
			'id' => 'page',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h3 class="widgettitle"><span class="inside">',
			'after_title' => '</span></h3>',
		));
		register_sidebar(array(
			'name' => 'Book Sidebar',
			'id' => 'book',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h3 class="widgettitle"><span class="inside">',
			'after_title' => '</span></h3>',
		));
		register_sidebar(array(
			'name' => 'Tabber Sidebar',
			'id' => 'ot_tabber',
			'before_widget' => '<div id="%1$s" class="tab-pane widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h3 class="widgettitle"><span class="inside">',
			'after_title' => '</span</h3>',
		));		
	}
}
imprint_register_sidebars();

if ( ! isset( $content_width ) ) 
    $content_width = 540;

include 'functions/themeoptions.php';
include 'functions/themefunctions.php';
include 'functions/themewidgets.php';
include 'functions/themeshortcodes.php';
include 'functions/ot_events.php';
include 'functions/privacy-notice.php';
include 'functions/post-type-reviews.php';
include 'functions/page_excerpt.php';
include 'functions/walker_functions.php';
?>