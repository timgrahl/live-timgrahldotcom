<?php get_header(); ?>

		<div class="container blog content">
			<div class="row">
				<div class="span8 entry">
					<div class="headline">
					<?php /* If this is a category archive */ if (is_search()) { ?>
						<h1 class="pagetitle">Search Results</h1>
					<?php /* If this is a category archive */ } elseif (is_category()) { ?>
						<h1 class="pagetitle"><?php single_cat_title(); ?></h1>
					<?php /* If this is a tag archive */ } elseif( is_tag() ) { ?>
						<h1 class="pagetitle">Posts Tagged &#8216;<?php single_tag_title(); ?>&#8217;</h1>
					<?php /* If this is a daily archive */ } elseif (is_day()) { ?>
						<h1 class="pagetitle">Archive for <?php the_time('F jS, Y'); ?></h1>
					<?php /* If this is a monthly archive */ } elseif (is_month()) { ?>
						<h1 class="pagetitle">Archive for <?php the_time('F, Y'); ?></h1>
					<?php /* If this is a yearly archive */ } elseif (is_year()) { ?>
						<h1 class="pagetitle">Archive for <?php the_time('Y'); ?></h1>
					<?php /* If this is an author archive */ } elseif (is_author()) { ?>
						<h1 class="pagetitle">Author Archive</h1>
					<?php /* If this is a paged archive */ } else { ?>
						<h1 class="pagetitle"><a href="<?php echo get_permalink(get_option('io_blog_page')); ?>"><?php echo get_the_title(get_option('io_blog_page'));?></a> <a class="rss" href="<?php bloginfo('rss2_url'); ?>">RSS</a></h1>
					<?php } ?>
					</div>
					<?php $c = 1; ?>
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
						<?php // found in content.php -- essentially the post data
						get_template_part('content'); ?>						
					<?php $c++; endwhile; ?>
					<div class="pagination pagination-centered">
						<?php
							global $wp_query;
							$big = 999999999; // need an unlikely integer
							echo paginate_links( array(
								'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
								'format' => '/page/%#%',
								'current' => max( 1, get_query_var('paged') ),
								'total' => $wp_query->max_num_pages,
								'type' => 'list',
							));
						?>
					</div>
					<?php endif; ?>
			</div><!--end span8 -->
			<div class="sidebar span4">
				<?php dynamic_sidebar('blog'); ?>
			</div><!--END sidebar-->
			<div class="clr"></div>
		</div><!--end row -->
	</div><!--end container -->

<?php get_footer(); ?>