<form class="form-search" method="get" action="<?php bloginfo('url'); ?>" id="searchform">
  <input type="text" placeholder="search the site" class="input-medium search-query" name="s">
  <button type="submit" class="btn">Search</button>
</form>