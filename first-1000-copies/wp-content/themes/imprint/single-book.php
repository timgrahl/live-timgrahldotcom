<?php get_header(); ?>

	<div class="container books page">
		<?php if (have_posts()) : ?>
		<?
		while (have_posts()) : the_post();
		$amazon = get_post_meta($post->ID, 'amazon_url', true);
		$bn = get_post_meta($post->ID, 'bn_url', true);
		$ceoread = get_post_meta($post->ID, 'ceoread_url', true);
		$indie = get_post_meta($post->ID, 'indie_url', true);
		$ibooks = get_post_meta($post->ID, 'ibooks_url', true);
		$itunes = get_post_meta($post->ID, 'itunes_url', true);
		$audible = get_post_meta($post->ID, 'audible_url', true);
		$bam = get_post_meta($post->ID, 'bam_url', true);
		$subtitle = get_post_meta($post->ID, 'book_subtitle', true);
		?>						
		<div class="row">
			<div class="span4 sidebar">
				<div class="widget book">
					<?php if ($amazon): ?>
						<a href="<?php echo $amazon; ?>"><?php the_post_thumbnail('medium', array('title' => $post->post_title)); ?></a>
					<? elseif($bn) :?>
						<a href="<?php echo $bn; ?>"><?php the_post_thumbnail('medium', array('title' => $post->post_title)); ?></a>
					<? elseif($ceoread) :?>
						<a href="<?php echo $ceoread; ?>"><?php the_post_thumbnail('medium', array('title' => $post->post_title)); ?></a>
					<? else :?>
						<?php the_post_thumbnail('medium', array('title' => $post->post_title)); ?>
					<?php endif; ?>
					<?php if ($amazon or $ceoread or $bn or $indie or $ibooks or $itunes or $audible or $bam): ?>
						<?php if (function_exists('ot_booklinks')): 
							echo ot_booklinks($amazon, $bn, $ceoread, $ibooks, $indie, $itunes, $audible, $bam);
						endif; ?>
					<?php endif; ?>

				</div><!--END widget-->
				<?php dynamic_sidebar('book'); ?>
			</div><!--end span5 -->
			<div class="span8 entry">
				<div class="post">
					<div class="headline">
						<h1 class="pagetitle"><?php the_title(); ?><?php if ($subtitle): echo ': <span class="subtitle">'.$subtitle.'</span>'; endif; ?></h1>
					</div>
					<?php the_content(); ?>
				</div>
			</div><!--END entry-->
		</div><!--end row -->
		<?php endwhile; ?>
		<?php endif; ?>
	</div><!--end container -->
		
	
<?php get_footer(); ?>