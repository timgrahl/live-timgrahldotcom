<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
<title><?php wp_title('-', true, 'right'); ?> <?php bloginfo('name'); ?></title>
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/bootstrap.css" type="text/css" media="screen" charset="utf-8" />
<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen">
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
<?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>
<?php wp_head(); ?>
</head>
<body id="<?php page_handle(); ?>">
	<div class="navbar navbar-static-top">
			<div class="nav-wrap">
				<div class="navbar-inner">				
					<div class="container">
						<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				      </a>
						<div class="nav-collapse">
							<?php if (is_front_page()): ?>
								<h1 class="brand">
									<a href="<?php bloginfo('url'); ?>"><?php bloginfo('name'); ?></a>
								</h1>
							<?php else : ?>
								<div class="brand">
									<a href="<?php bloginfo('url'); ?>"><?php bloginfo('name'); ?></a>
								</div>
							<?php endif; ?>
							<?php imprint_social(); ?>
							<?php if (!is_page_template('template_landing_page.php')): ?>
								<?php wp_nav_menu( array(
									'theme_location' => 'top-navigation',
									'menu_class' 	=> 'nav',
									'container'		=> false,
									'fallback_cb' => false,
									'walker' => new bsNavWalker()
								)); ?>
							<?php endif; ?>
						</div>
					</div><!--END container-->
				</div><!--END navbar-inner-->				
			</div><!--END nav-wrap-->
	</div><!--END navbar-->