<?
// [newsletter text='Text before the newsletter']
function ot_newsletter_function($atts) {
	extract(shortcode_atts(array(
		"text" => ""
    ), $atts));
		$retcont = '';
		$retcont .= '<div class="nlsignup_content">';
		if (!empty($text)) {
			$retcont .= '<p>'.$text.'</p>';
		}
		$retcont .= get_option('io_nlsignup_embed') . '</div>';
		return $retcont;

}
add_shortcode("newsletter", "ot_newsletter_function");

// [button link="http://thelink.com" text="text to show on the button"]
if (!function_exists('button_style')) {
	function button_style($atts) {
			extract(shortcode_atts(array(
				'link' => '',
				'text' => 'Click Here',
				'width' => '', // width -- full would make this button 100% width
				'style' => '', // style -- this refers to bootstraps button styles
				'size' => '', // size -- this refers to bootstrap's button sizes (ex. btn-medium, btn-large)
	        ), $atts));
			$return = '';
			$classes = '';
			if (strlen($width) > 0) {
				$inlinestyle = 'style="display:block;" '; 
			}
			if (strlen($style) > 0) {
				$classes .= ' '.$style;
			}
			if (strlen($size) > 0) {
				$classes .= ' '.$size;
			}
			$return .='<a href="'.$link.'" '.$inlinestyle.'class="btn'.$classes.'"';
			$return.= '">'.$text.'</a>';
			return $return;
	}
}
add_shortcode("button", "button_style");

?>