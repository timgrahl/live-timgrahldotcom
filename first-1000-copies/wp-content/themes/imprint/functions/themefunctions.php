<?php
// General Theme stuff
	add_editor_style();
	// Enqueuing Jquery

	// Adding Featured Image Support
	add_theme_support( 'post-thumbnails' );

	if (! function_exists( 'imprint_image_sizes' ) ) { 
		function imprint_image_sizes() {
		add_image_size( 'about-thumb', 108, 150, true ); // adding about thumb for sidebar
		}
	}
	imprint_image_sizes();

	// Adding a header Menu
	function imprint_init() {
		register_nav_menus(array('top-navigation' => __( 'Top Navigation' )));
	    wp_enqueue_script('jquery');
	}
	add_action( 'init', 'imprint_init' );

	// Adding Shortcode Functionality in Widgets
	add_filter('widget_text', 'do_shortcode');

	function tutorials_widget() { ?>
	<h4>Helpful Links and Tutorials</h4>

	<p>For several helpful videos for WordPress, visit <a href="http://wordpress.tv/how-to">http://wordpress.tv/how-to</a></p>
	<p>For specific help in getting started, see the following links:</p>
	<ul>
		<li><a target="_blank" href="http://support.outthinkgroup.com/2011/05/wordpress-overview/" title="WordPress Overview">WordPress Overview</a></li>
		<li><a target="_blank" href="http://wordpress.tv/2009/01/15/writing-and-publishing-a-post/">Writing and Publishing a Post</a></li>
		<li><a target="_blank" href="http://wordpress.tv/2009/01/14/publishing-your-post-at-a-later-date/">Saving your Post as a Draft</a></li>
		<li><a target="_blank" href="http://wordpress.tv/2009/01/14/using-quick-edit-and-bulk-edit-to-manage-your-posts-in-half-the-time/">Using Quick Edit</a></li>
		<li><a target="_blank" href="http://wordpress.tv/2009/07/24/wpcom-widgets/">Using Widgets</a></li>
		<li><a target="_blank" href="http://support.outthinkgroup.com/2012/02/adding-editing-books/" title="Adding / Editing Books">Adding / Editing Books</a></li>
		<li><a target="_blank" href="http://support.outthinkgroup.com/?p=135" title="Using the Reviews Plugin">Using the Reviews Plugin</a></li>
		<li><a target="_blank" href="http://support.outthinkgroup.com/2011/10/the-wordpress-menu-system/" title="">Using The WordPress Menu System</a></li>
		<li><a target="_blank" href="http://support.outthinkgroup.com/2012/02/using-the-event-manager/" title="Using the Event Manager">Using the Event Manager</a></li>
		<li><a target="_blank" href="http://support.outthinkgroup.com/2011/10/wordpress-contact-form-7-plugin/" title="">Using the Contact Form</a></li>
		<li><a target="_blank" href="http://support.outthinkgroup.com/2011/10/using-the-conditional-widget-plugin/" title="Conditional Widgets Plugin">Conditional Widgets Plugin</a></li>
	</ul>
	<?php };
	function add_tutorials_widget() {
	  wp_add_dashboard_widget( 'tutorials_widget', __( 'Tutorials' ), 'tutorials_widget' );
	}
	add_action('wp_dashboard_setup', 'add_tutorials_widget' );

	if (! function_exists('imprint_share')) {
			function imprint_share() { ?>
				<div class="share-links">
					<div class="postmeta">
						<div class="tweet float-left">
							<a href="http://twitter.com/share" class="twitter-share-button" data-url="<?php the_permalink(); ?>" data-text="<?php the_title(); ?>" data-count="horizontal" data-via="<?php echo get_option("io_twitter_handle"); ?>">Tweet This</a>
						</div>
						<div class="float-left facebook">
							<span class="facelike" src="http://www.facebook.com/plugins/like.php?href=<?php the_permalink(); ?>&amp;layout=button_count&amp;show_faces=false&amp;width=85&amp;action=like&amp;colorscheme=light&amp;height=21"></span>
						</div>
						<div class="plusone float-left">
							<g:plusone size="medium" annotation="inline" width="95" href="<?php the_permalink(); ?>"></g:plusone>

						</div>
						<div class="float-left linkedin">
							<script type="IN/Share" data-url="<?php the_permalink(); ?>" data-counter="right"></script>
						</div>
						<?php if(function_exists('wp_email')) { ?>
						<div class="float-left email">
								<?php email_link(); ?>
						</div>
						<?php } ?>
						<div class="clr"></div>
					</div>
				</div>
		<?	}
	}
	// end imprint_share

	// Imprint Social Buttons
	if (! function_exists( 'imprint_social' )) {
		function imprint_social() {
			$facebook = get_option('io_facebook_url');
			$twitter = get_option('io_twitter_handle');
			$linkedin = get_option('io_linkedin_url');
			$youtube = get_option('io_youtube_user');
			$pinterest = get_option('io_pinterest');
			$flickr = get_option('io_flickr');
			$stumble = get_option('io_stumbleupon');
			$tumblr = get_option('io_tumblr');
			?>
			<div class="social">
				<?php if ($facebook != '') { ?>
					<a class="facebook" rel="me" href="<?php echo $facebook; ?>" alt="Connect with <?php echo get_option('io_author_name'); ?> on Facebook">Facebook</a>
				<?php } ?>
				<?php if ($tumblr != '') { ?>
					<a class="tumblr" rel="me" href="<?php echo $tumblr; ?>" alt="Follow on Tumblr">Tumblr</a>
				<?php } ?>
				<?php if ($twitter != '') { ?>
					<a class="twitter" rel="me" href="http://twitter.com/<?php echo $twitter; ?>" alt="Follow <?php echo $twitter; ?> on Twitter">Twitter</a>
				<?php } ?>
				<?php if ($linkedin != '') { ?>
					<a class="linkedin" rel="me" href="<?php echo $linkedin; ?>" alt="Connect with <?php echo get_option('io_author_name'); ?> on LinkedIn">LinkedIn</a>
				<?php } ?>
				<?php if ($youtube != '') { ?>
					<a class="youtube" rel="me" href="<?php echo $youtube; ?>" alt="Visit <?php echo get_option('io_author_name'); ?>'s Youtube Channel">Youtube</a>
				<?php } ?>
				<?php if ($pinterest != '') { ?>
					<a class="pinterest" rel="me" href="<?php echo $pinterest; ?>" alt="Follow <?php echo get_option('io_author_name'); ?> on Pinterest">Pinterest</a>
				<?php } ?>
				<?php if ($flickr != '') { ?>
					<a class="flickr" rel="me" href="<?php echo $flickr; ?>" alt="<?php echo get_option('io_author_name'); ?>'s Flickr">Flickr</a>
				<?php } ?>
				<?php if ($stumbleupon != '') { ?>
					<a class="stumbleupon" rel="me" href="<?php echo $stumbleupon; ?>" alt="<?php echo get_option('io_author_name'); ?>'s Stumbleupon">StumbleUpon</a>
				<?php } ?>				
			</div>
		<?
		} // END function imprint_social()
	} // end check for function

	if (! function_exists('page_handle')) {
		function page_handle() {
			global $post;
			$queue = '';
			if (is_front_page()) {
				$queue = 'home';
			} elseif (is_post_type_archive('book')) {
				$queue = 'home';
			} elseif (is_page() or get_post_type() == 'book') {
				$queue = $post->post_name;
			} elseif (is_home() or is_single() or is_category()) {
				$queue = 'blog';
			}
			echo $queue;
		}
	}
	function remove_parent_classes($class)
	{
	  // check for current page classes, return false if they exist.
		return ($class == 'current_page_item' || $class == 'current_page_parent' || $class == 'current_page_ancestor'  || $class == 'current-menu-item') ? FALSE : TRUE;
	}

	function add_class_to_wp_nav_menu($classes)
	{
	     switch (get_post_type())
	     {
	     	case 'book':
	     		// we're viewing a custom post type, so remove the 'current_page_xxx and current-menu-item' from all menu items.
	     		$classes = array_filter($classes, "remove_parent_classes");

	     		// add the current page class to a specific menu item (replace ###) 'menu-item-28', 'menu-item-29'
	     		if (in_array('menu-item-555', $classes))
	     		{
	     		   $classes[] = 'current_page_parent current-menu-item';
	         }
	     		break;

	      // add more cases if necessary and/or a default
	     }
		return $classes;
	}
	add_filter('nav_menu_css_class', 'add_class_to_wp_nav_menu');
	
	function bookdata($pid) {
		$data = array();
		$data['amazon'] = get_post_meta($pid, 'amazon_url', true);
		$data['bn'] = get_post_meta($pid, 'bn_url', true);
		$data['ceoread'] = get_post_meta($pid, 'ceoread_url', true);
		$data['indie'] = get_post_meta($pid, 'indie_url', true);
		$data['ibooks'] = get_post_meta($pid, 'ibooks_url', true);
		$data['itunes'] = get_post_meta($pid, 'itunes_url', true);
		$data['audible'] = get_post_meta($pid, 'audible_url', true);
		$data['bam'] = get_post_meta($pid, 'bam_url', true);
		$data['subtitle'] = get_post_meta($pid, 'book_subtitle', true);
		return $data;
	}