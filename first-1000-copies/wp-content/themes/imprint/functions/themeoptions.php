<?php
$themename = "Imprint";
$shortname = "impr";
add_action('admin_menu', 'add_welcome_interface');
add_action('admin_init', 'imprint_head');

function add_welcome_interface() {
  add_menu_page($themename . 'Imprint Options', 'Theme Options', '8', 'functions', 'editoptions');
  }

// Content on Options Page
function editoptions() {  ?>
	<div class='wrap'>
		<h2 class="imprint-title">Imprint Options</h2>
		<?php if($_GET['settings-updated'] == 'true') : ?>
			<div id="message" class="updated">
				<p>Settings updated!</p>
			</div>
		<?php endif; ?>
		<form method="post" action="options.php">
		<?php wp_nonce_field('update-options') ?>
		<div class="options book-info">
			<h3>Content Settings</h3>
			<div>
				<p><strong>Customize your theme!</strong></p>
				<p>Using this theme, you can add new books, add reviews to your books, set a featured book to show on the homepage, and have your about page blurb automatically appear in your desired locations.</p>
			</div>
			<div class="col first">
				<p>Author Name</p>
				<p><input type="text" name="io_author_name" id="io_author_name" value="<?php echo get_option('io_author_name'); ?>"></p>
				<p><strong>Blog Page</strong></p>
				<?php
					$arr = array(
						'option_none_value' => 0,
						'show_option_none' => 'Please Choose your Blog Page',
						'name' => 'io_blog_page'
					);
				if (get_option('io_blog_page') != '0') {
					$arr['selected'] = get_option('io_blog_page');
				} else {
					echo '<p class="notify">You must set this option for this theme to work properly.</p>';
				} ?>
				<p><?php wp_dropdown_pages($arr); ?></p>

				<p><strong>About Page</strong></p>
				<?php
					$arg = array(
						'option_none_value' => 0,
						'show_option_none' => 'Please Choose your About Page',
						'name' => 'io_about_page'
					);
				if (get_option('io_about_page') != '0') {
					$arg['selected'] = get_option('io_about_page');
				} else {
					echo '<p class="notify">You must set this option for this theme to work properly.</p>';
				} ?>
				<p><?php wp_dropdown_pages($arg); ?></p>

				<p><strong>Featured Book</strong><br><small>Please Select the book you would like to feature on the home page</small></p>
				<?php
				// get all the books
				$books = get_posts('numberposts=-1&orderby=menu_order&order=ASC&post_type=book&post_status=publish');
				// if the featured book hasn't been assigned
				$fbook = get_option('io_featured_book');
				if ($fbook == '0') {
					// and if there are no books
					if(empty($books)) {
						echo '<p class="notify">You must first add books in order to set this option.</p>';
					} else {
					// otherwise, just tell the person to set the book
					echo '<p class="notify">You must set this option for this theme to work properly.</p>';
					}
				}?>
				<p>
					<select name="io_featured_book" id="io_featured_book">
					<?php
					// creating drop down for books
					// if no books are present, it only says "p lease choose your featured book" ?>
					<option value="0"<? if($fbook == '0') { echo ' selected="selected"'; } ?>>Please choose your featured book</option>
					<?
					foreach ($books as $book) { ?>
						<option class="level-0" <? if($fbook == $book->ID) { echo 'selected="selected" '; } ?> value="<? echo $book->ID; ?>"><? echo $book->post_title; ?></option>
					<? } // end loop through books
					?>
					</select>
				<?php //wp_dropdown_pages($args); ?></p>
			</div>
			<div class="col third">
				<p>Social Media</p>
				<p><label for="io_facebook_url">Facebook URL</label><input type="text" name="io_facebook_url" value="<?php echo get_option('io_facebook_url'); ?>" id="io_facebook_url" /></p>
				<p><label for="io_twitter_handle">Twitter Handle</label><input type="text" name="io_twitter_handle" value="<?php echo get_option('io_twitter_handle'); ?>" id="io_twitter_handle"></p>
				<p><label for="io_linkedin_url">LinkedIn URL</label><input type="text" name="io_linkedin_url" value="<?php echo get_option('io_linkedin_url'); ?>" id="io_linkedin_url"></p>
				<p><label for="io_goodreads_url">Goodreads URL</label><input type="text" name="io_goodreads_url" value="<?php echo get_option('io_goodreads_url'); ?>" id="io_goodreads_url"></p>
				<p><label for="io_pinterest">Pinterest</label><input type="text" name="io_pinterest" value="<?php echo get_option('io_pinterest'); ?>" id="io_pinterest"></p>
				<p><label for="io_youtube_user">Youtube User</label><input type="text" name="io_youtube_user" value="<?php echo get_option('io_youtube_user'); ?>" id="io_youtube_user"></p>
				<p><label for="io_flickr">Flickr URL</label><input type="text" name="io_flickr" value="<?php echo get_option('io_flickr'); ?>" id="io_flickr"></p>
				<p><label for="io_tumblr">Tumblr URL</label><input type="text" name="io_tumblr" value="<?php echo get_option('io_tumblr'); ?>" id="io_tumblr"></p>
				<p><label for="io_stumbleupon">Stumble Upon URL</label><input type="text" name="io_stumbleupon" value="<?php echo get_option('io_stumbleupon'); ?>" id="io_stumbleupon"></p>
			</div>
			<div class="clr"></div>
			<div>
				<h3>Newsletter Signup Forms</h3>
				<p>Newsletter Signup Blurb<br><small>(Text you want to appear before your newsletter signup forms)</small></p>
				<textarea rows="2" cols="40" name="io_nlsignup_text" id="io_nlsignup_text"><?php echo get_option('io_nlsignup_text'); ?></textarea>
				<p>Newsletter Embed Code</p>
				<textarea name="io_nlsignup_embed" rows="4" cols="40"><?php echo get_option('io_nlsignup_embed'); ?></textarea>
			</div>
			<div>
				<p><label for="io_popover_toggle"><input style="padding-right:10px;" type="checkbox" name="io_popover_toggle" value="true"<?php if (get_option('io_popover_toggle') == 'true'): ?> checked="checked"<?php endif; ?> id="io_popover_toggle">
					Turn Newsletter Signup Popover On</label><br><small>If this option is checked, the code entered in the field below will show to the visitor after 20 seconds</small></p>
				<p><strong>Newsletter Signup Popover Code</strong></p>


				<textarea name="io_nlsignup_popover" rows="10" cols="40"><?php echo get_option('io_nlsignup_popover'); ?></textarea>
			</div>
		</div>
		<p><input class="imprint_submit" type="submit" name="Submit" value="Update Options" /></p>
		<input type="hidden" name="action" value="update" />
		<input type="hidden" name="page_options" value="
		io_author_name,
		io_book_image,
		io_facebook_url,
		io_twitter_handle,
		io_linkedin_url,
		io_goodreads_url,
		io_nlsignup_embed,
		io_nlsignup_popover,
		io_popover_toggle,
		io_nlsignup_text,
		io_blog_page,
		io_about_page,
		io_featured_book,
		io_pinterest,
		io_youtube_user,
		io_flickr,
		io_tumblr,
		io_stumbleupon" />

		</form>
		<div style="clear:both;height:1px;margin-bottom:100px;"></div>
	</div>
<?php }

	function imprint_head() {
		$file_dir = get_bloginfo('template_directory');
		wp_enqueue_style("functions", $file_dir."/functions/themeoptions.css", false, "1.0", "all");
		wp_enqueue_script("rm_script", $file_dir."/functions/js/impr_script.js", false, "1.0");
}
imprint_head();
?>