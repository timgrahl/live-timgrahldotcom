<?php
function pageexcerpt($id) {
	$excerpt = get_post_meta($id, 'pageexcerpt', true);
	return $excerpt;
}
/* Define the custom box */

// WP 3.0+
// add_action('add_meta_boxes', 'outthink_meta_box');

// backwards compatible
add_action('admin_init', 'outthink_meta_box', 1);

/* Do something with the data entered */
add_action('save_post', 'outthink_save_postdata');

/* Adds a box to the main column on the Post and Page edit screens */
function outthink_meta_box() {
    add_meta_box( 'outthink_sectionid', __( 'Page Excerpt', 'outthink_textdomain' ), 'outthink_inner_custom_box','page', 'normal');
}

/* Prints the box content */
function outthink_inner_custom_box() {

  // Use nonce for verification
  wp_nonce_field( plugin_basename(__FILE__), 'outthink_noncename' );

	global $post;
	$pageexcerpt = get_post_meta($post->ID, 'pageexcerpt', true);

  // The actual fields for data entry ?>
<table border="0" cellspacing="5" cellpadding="5" width="100%">
	<tr>
	<td>
		<?php
		 wp_editor( $content = $pageexcerpt, $editor_id = 'pageexcerpt', $settings = array(
			'textarea_name' => 'pageexcerpt',
			'textarea_rows' => '3'
		)); ?>
	</tr>
</table>
  
	
<?php
}

/* When the post is saved, saves our custom data */
function outthink_save_postdata( $post_id ) {

  // verify this came from the our screen and with proper authorization,
  // because save_post can be triggered at other times

  if ( !wp_verify_nonce( $_POST['outthink_noncename'], plugin_basename(__FILE__) ) )
      return $post_id;
  // verify if this is an auto save routine. 
  // If it is our form has not been submitted, so we dont want to do anything
  if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) 
      return $post_id;

  
  // Check permissions
  if ( 'page' == $_POST['post_type'] ) 
  {
    if ( !current_user_can( 'edit_page', $post_id ) )
        return $post_id;
  }
  else
  {
    if ( !current_user_can( 'edit_post', $post_id ) )
        return $post_id;
  }

  // OK, we're authenticated: we need to find and save the data

  	$pageexcerpt = $_POST['pageexcerpt'];

  // update the data
		update_post_meta($post_id, 'pageexcerpt', $pageexcerpt);
}
?>