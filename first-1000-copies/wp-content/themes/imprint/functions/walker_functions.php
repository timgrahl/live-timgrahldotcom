<?php
/** custom walker function -- this should be fun **/
class bsNavWalker extends Walker_Nav_Menu
{
	// add classes to ul sub-menus
	function start_lvl( &$output, $depth ) {
	    // depth dependent classes
	    $indent = ( $depth > 0  ? str_repeat( "\t", $depth ) : '' ); // code indent
	    $display_depth = ( $depth + 1); // because it counts the first submenu as 0
	    $classes = array(
	        'dropdown-menu',
	        ( $display_depth % 2  ? 'menu-odd' : 'menu-even' ),
	        ( $display_depth >=2 ? 'dropdown-submenu' : '' ),
	        'menu-depth-' . $display_depth
	        );
		if ($display_depth > 0) {
			$attribute = 'role="menu" aria-labelledby="dropdownMenu"';
		}
	    $class_names = implode( ' ', $classes );

	    // build html
	    $output .= "\n" . $indent . '<ul class="' . $class_names . '" '.$attribute.'>' . "\n";
	}
      function start_el(&$output, $item, $depth, $args)
      {
           global $wp_query;

			// indent is how deep the thing should be I believe
           $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

			// class string...
           $class_names = $value = '';
           $classes = empty( $item->classes ) ? array() : (array) $item->classes;

           $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );
           $class_names = ' class="'. esc_attr( $class_names ) . '"';

           $output .= $indent . '<li id="menu-item-'. $item->ID . '"' . $value . $class_names .'>';

           $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
           $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
           $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';

			if ($item->url == '#') {
	           $attributes .= ' href="javascript:void(null);"';			
			} else {
	           $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';
			}

			if(in_array('dropdown', $classes))
			{
				$chevron = ' <b class="caret"></b>';
				$attributes .= ' class="dropdown-hover nav-dropdown"';
			} else  {
				$chevron = '';
			}

            $item_output = $args->before;
            $item_output .= '<a'. $attributes .'>';
            $item_output .= $args->link_before .apply_filters( 'the_title', $item->title, $item->ID );
            $item_output .= $description.$args->link_after;
			$item_output .= $chevron;
            $item_output .= '</a>';
            $item_output .= $args->after;

            $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
		}
}
add_filter( 'wp_nav_menu_objects', 'add_menu_parent_class' );
function add_menu_parent_class( $items ) {
	$parents = array();
	foreach ( $items as $item ) {
		// building array of "parent" menu items.
		if ( $item->menu_item_parent && $item->menu_item_parent > 0 ) {
			$parents[] = $item->menu_item_parent;
		}
	}
	// looping through items and checking to see if they are "parents"
	foreach ( $items as $item ) {
		if ($item->object_id == get_the_ID()) {
			$item->classes[] = 'active';
		}
		if ( in_array( $item->ID, $parents ) ) {
			$item->classes[] = 'dropdown';
		}
	}
	return $items;    
}