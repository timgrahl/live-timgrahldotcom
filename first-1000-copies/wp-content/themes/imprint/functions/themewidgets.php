<?php
// initializes the widget on WordPress Load
add_action('widgets_init', 'nlsignup_init_widget');

	// ********
	// This is a widget for the newsletter signup

	function nlsignup_init_widget() {
		register_widget( 'NL_Signup_Widget' );
	}

	// new class to extend WP_Widget function
	class NL_Signup_Widget extends WP_Widget {
		/** Widget setup.  */
		function NL_Signup_Widget() {
			/* Widget settings. */
			$widget_ops = array(
				'classname' => 'nlsignup_widget',
				'description' => __('Widget for Newsletter Signup', 'nlsignup_widget') );

			/* Widget control settings. */
			$control_ops = array( 'width' => 300, 'height' => 350, 'id_base' => 'nl-signup-widget' );

			/* Create the widget. */
			$this->WP_Widget( 'nl-signup-widget', __('IMPRINT: Newsletter Signup Widget', 'Options'), $widget_ops, $control_ops );
		}
		/**
		* How to display the widget on the screen. */
		function widget( $args, $instance ) {
			extract( $args );
			$title = apply_filters('widget_title', $instance['title'] );

			/* Before widget (defined by themes). */
			echo $before_widget;
			if ( $title )
				echo $before_title . $title . $after_title;
			/* Display name from widget settings if one was input. */
		
			// Settings from the widget
			global $post
		?>
			<div class="newsletter-widget">
				<p class="nl-text">
				<?php if (get_post_meta($post->ID, 'nl_blurb', true)):
					echo get_post_meta($post->ID, 'nl_blurb', true);
				else : 
					echo get_option('io_nlsignup_text');
				endif; ?></p>
				<?php if (get_post_meta($post->ID, 'nl_embed', true)):
					echo get_post_meta($post->ID, 'nl_embed', true);
				else :
					include (TEMPLATEPATH . '/nl-signup.php');	
				endif; ?>
				
			</div>
			<?php
			/* After widget (defined by themes). */
			echo $after_widget;
		}

		/**
		 * Update the widget settings.
		 */
		function update( $new_instance, $old_instance ) {
			$instance = $old_instance;

			/* Strip tags for title and name to remove HTML (important for text inputs). */
			$instance['title'] = strip_tags( $new_instance['title'] );

			return $instance;
		}

	/**
	 * Displays the widget settings controls on the widget panel.
	 * Make use of the get_field_id() and get_field_name() function
	 * when creating your form elements. This handles the confusing stuff.
*/
		function form($instance) {
			$defaults = array( 'title' => __('Newsletter Signup', 'nlsignup_widget'));
			$instance = wp_parse_args( (array) $instance, $defaults ); ?>
			<!-- Widget Title: Text Input -->
			<p>
				<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e('Title:', 'nlsignup_widget'); ?></label>
				<input type="text" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" class="widefat" />
			</p>
			<p>This widget pulls the content from the <a href="<?php bloginfo('url'); ?>/wp-admin/admin.php?page=functions">Newsletter Embed</a> code.</p>
		<?php
		}
	}
	
// This is a widget for Say Hello

// initializes the widget on WordPress Load
add_action('widgets_init', 'ot_social_init_widget');

	// Should be called above from "add_action"
	function ot_social_init_widget() {
		register_widget( 'OT_Social_Widget' );
	}

	// new class to extend WP_Widget function
	class OT_Social_Widget extends WP_Widget {
		/** Widget setup.  */
		function OT_Social_Widget() {
			/* Widget settings. */
			$widget_ops = array(
				'classname' => 'ot_social_widget',
				'description' => __('Say Hello', 'ot_social_widget') );

			/* Widget control settings. */
			$control_ops = array( 'width' => 300, 'height' => 350, 'id_base' => 'ot_social-widget' );

			/* Create the widget. */
			$this->WP_Widget( 'ot_social-widget', __('IMPRINT: Say Hello Widget', 'Options'), $widget_ops, $control_ops );
		}
		/**
		* How to display the widget on the screen. */
		function widget( $args, $instance ) {
			extract( $args );
			$title = apply_filters('widget_title', $instance['title'] );

			/* Before widget (defined by themes). */
			echo $before_widget;
			/* Display name from widget settings if one was input. */

			if ( $title )
				echo $before_title . $title . $after_title;
		
			// Settings from the widget
			imprint_social();
			
			/* After widget (defined by themes). */
			echo $after_widget;
		}
		
		function update( $new_instance, $old_instance ) {
			$instance = $old_instance;

			/* Strip tags for title and name to remove HTML (important for text inputs). */
			$instance['title'] = strip_tags( $new_instance['title'] );

			return $instance;
		}
	/**
	 * Displays the widget settings controls on the widget panel.
	 * Make use of the get_field_id() and get_field_name() function
	 * when creating your form elements. This handles the confusing stuff.
*/
		function form($instance) {
			$defaults = array( 'title' => __('Say Hello', 'ot_social_widget'));
			$instance = wp_parse_args( (array) $instance, $defaults ); ?>
		<!-- Widget Title: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e('Title:', 'ot_social_widget'); ?></label>
			<input type="text" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" class="widefat" />
		</p>
			<p>This widget pulls your social media links using your profile links in your <a href="<?php bloginfo('url'); ?>/wp-admin/admin.php?page=functions">Theme Options</a>.</p>
		<?php
		}
	} // END OT_Social_Widget


	// This is a widget for About the Author
	// initializes the widget on WordPress Load
	add_action('widgets_init', 'ot_about_init_widget');

	// Should be called above from "add_action"
	function ot_about_init_widget() {
		register_widget( 'OT_About_Widget' );
	}

	// new class to extend WP_Widget function
	class OT_About_Widget extends WP_Widget {
		/** Widget setup.  */
		function OT_About_Widget() {
			/* Widget settings. */
			$widget_ops = array(
				'classname' => 'ot_about_widget',
				'description' => __('About the Author', 'ot_about_widget') );
			/* Widget control settings. */
			$control_ops = array( 'width' => 300, 'height' => 350, 'id_base' => 'ot_about-widget' );
			/* Create the widget. */
			$this->WP_Widget( 'ot_about-widget', __('IMPRINT: About the Author Widget', 'Options'), $widget_ops, $control_ops );
		}
	/* How to display the widget on the screen. */
	function widget( $args, $instance ) {
		extract( $args );
		$title = apply_filters('widget_title', $instance['title'] );

		/* Before widget (defined by themes). */
		echo $before_widget;
		/* Display name from widget settings if one was input. */

		if ( $title )
			echo $before_title . $title . $after_title;

			// Settings from the widget
			if(get_option('io_about_page') != '0') : ?>
				<div>
					<p>
						<a href="<?php echo get_permalink(get_option('io_about_page')); ?>"><?php echo get_the_post_thumbnail(get_option('io_about_page'), 'about-thumb', array('class' => 'alignleft')); ?></a>
						<? echo get_post_meta(get_option('io_about_page'), 'pageexcerpt', true); ?><br>
						<a class="more" href="<? echo get_permalink(get_option('io_about_page')); ?>">Learn More...</a>
					</p>
				</div>
			<?php endif;

			/* After widget (defined by themes). */
			echo $after_widget;
		}
		// updates the widget
		function update( $new_instance, $old_instance ) {
			$instance = $old_instance;

			/* Strip tags for title and name to remove HTML (important for text inputs). */
			$instance['title'] = strip_tags( $new_instance['title'] );

			return $instance;
		}
	/**
	 * Displays the widget settings controls on the widget panel.
	 * Make use of the get_field_id() and get_field_name() function
	 * when creating your form elements. This handles the confusing stuff.
	*/
	function form($instance) {
		$defaults = array( 'title' => __('About the Author', 'ot_about_widget'));
		$instance = wp_parse_args( (array) $instance, $defaults ); ?>
	<!-- Widget Title: Text Input -->
			
			<!-- Widget Title: Text Input -->
			<p>
				<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e('Title:', 'ot_about_widget'); ?></label>
				<input type="text" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" class="widefat" />
			</p>
			<p>This widget displays a short blurb about the author, along with the featured image from the <a href="<?php echo get_bloginfo("wp_url").'/wp-admin/post.php?post='.get_option("io_about_page").'&action=edit'; ?>">About page</a>.</p>
		<?php
		}
	} // END OT_About_Widget


// This is a widget for Tabber Widget

// initializes the widget on WordPress Load
add_action('widgets_init', 'ot_tabber_widget_init_widget');

// Should be called above from "add_action"
function ot_tabber_widget_init_widget() {
	register_widget( 'ot_tabber_widget' );
}

// new class to extend WP_Widget function
class ot_tabber_widget extends WP_Widget {
	/** Widget setup.  */
	function ot_tabber_widget() {
		/* Widget settings. */
		$widget_ops = array(
			'classname' => 'ot_tabber_widget_widget',
			'description' => __('Tabber Widget', 'ot_tabber_widget_widget') );

		/* Widget control settings. */
		$control_ops = array( 'width' => 300, 'height' => 350, 'id_base' => 'ot_tabber_widget_widget' );

		/* Create the widget. */
		$this->WP_Widget( 'ot_tabber_widget_widget', __('Tabber Widget', 'Options'), $widget_ops, $control_ops );
	}
	/**
	* How to display the widget on the screen. */
	function widget( $args, $instance ) {
		extract( $args );

		/* Before widget (defined by themes). */
		echo $before_widget;
		
		// Settings from the widget
		?>
		<ul class="nav nav-tabs" id="myTab" style="display:none;">
		</ul>
		<div class="tab-content">
			<?php dynamic_sidebar('ot_tabber'); ?>
		</div>
		<script>
		jQuery(document).ready(function() {
			jQuery('.tab-content .tab-pane').each(function() {
				var t = jQuery(this);
				var tID = t.attr('id');
				var theading = t.find('h3.widgettitle');
				// removing the class widget because it's not really a standalone widget anymore...
				t.removeClass('widget');
				// adding the class "active" so that the first selected widget shows up.
				theading.remove();
				jQuery('#myTab').append('<li><a href="#'+tID+'">'+theading.text()+'</a></li>');
				jQuery('.tab-content div').first().addClass('active');
				jQuery('#myTab li').first().addClass('active')
				jQuery('#myTab').show();
				t.find('li:even').addClass('alt');

			});			
			jQuery('#myTab a').click(function (e) {
			  e.preventDefault();
			  jQuery(this).tab('show');
			});
			
		});
		</script>
<?		/* After widget (defined by themes). */
		echo $after_widget;
	}


/**
 * Displays the widget settings controls on the widget panel.
 * Make use of the get_field_id() and get_field_name() function
 * when creating your form elements. This handles the confusing stuff.
*/
	function form() { ?>
		<!-- Widget Title: Text Input -->
		<p>Any widgets you drag to the "Tabber" sidebar will show in this widget.</p>
	<?php
	}
} // END ot_tabber_widget
