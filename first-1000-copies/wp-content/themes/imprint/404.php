<?php get_header(); ?>

		<div class="container content page">
			<div class="row">
				<div class="span7 entry">
					<div class="headline">
						<h1 class="pagetitle">Page not found.</h1>
					</div>
						<div class="post post-<? echo $c; ?>">
							<p>Sorry, the page you're looking for can't be found.</p>
						</div><!--END post-->
				</div><!--end span7 -->
				<div class="sidebar span4">
				</div><!--END sidebar-->
			<div class="clr"></div>
		</div><!--end row -->
	</div><!--end container -->

<?php get_footer(); ?>