<div class="navbar navbar-static-top footer">
	<div class="navbar-inner">
		<div class="container">
			<p class="pull-left legal">
				All Content &copy; <?php echo date('Y');?> <?php bloginfo('name'); ?>
			</p>
			<p class="credits pull-right">site by <a href="http://outthinkgroup.com">Out:think Group</a></p>
		</div>
	  </div>
	</div>
	<script type="text/javascript">
	  (function() {
	    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
	    po.src = 'https://apis.google.com/js/plusone.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
	  })();
	</script>
	<script src="<?php bloginfo('template_url'); ?>/js/setup.js" type="text/javascript" charset="utf-8"></script>
	<script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>
	<script src="//platform.linkedin.com/in.js" type="text/javascript"></script>
	<script src="<?php bloginfo('template_url'); ?>/js/bootstrap.min.js" type="text/javascript" charset="utf-8"></script>
	
	<?php if (get_option('io_popover_toggle')): ?>
		<script type="text/javascript" charset="utf-8" src="<?php bloginfo('template_url'); ?>/js/jq-cookie.js"></script>
		<script type="text/javascript" charset="utf-8">
			jQuery(document).ready( function() {
				// if a user doesn't have the cookie 'user_visited'm show the modal after 20 seconds
				// then cookie them so they won't see it again for 6 months
				if (!jQuery.cookie('user_visited')) {
					setTimeout('jQuery("#myModal").modal()', 20000);
					jQuery.cookie('user_visited', 'true', { expires: 182 });	
				}
			});
		</script>
		<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-header" style="border-bottom:none;">
				<button style="border-radius: 20px;" class="pull-right btn" data-dismiss="modal" aria-hidden="true">X</button>
			</div><!--END modal-header-->
		<div class="modal-body">
			<?php echo get_option('io_nlsignup_popover'); ?>
		</div><!--END modal-body-->
	</div><!--END myModal-->
	<?php endif; ?>
	<?php wp_footer(); ?>
</body>
</html>