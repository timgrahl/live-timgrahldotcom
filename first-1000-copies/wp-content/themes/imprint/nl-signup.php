<?php if (get_option('io_nlsignup_embed')): 
	echo get_option('io_nlsignup_embed');
else : ?>
<div id="mc_embed_signup">
<form action="#" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate="">
	
<div class="mc-field-group">
	<label for="mce-EMAIL" style="display: none;">Email Address </label>
	<input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Email Address ">
</div>
<div class="mc-field-group">
	<label for="mce-FNAME" style="display: none;">First Name </label>
	<input type="text" value="" name="FNAME" class="" id="mce-FNAME" placeholder="First Name ">
</div>
	<div id="mce-responses" class="clear">
		<div class="response" id="mce-error-response" style="display:none"></div>
		<div class="response" id="mce-success-response" style="display:none"></div>
	</div>
	<div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button btn"></div>
</form>
</div>
<?php endif; ?>
