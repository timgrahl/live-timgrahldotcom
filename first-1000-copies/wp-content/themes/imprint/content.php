<?php
global $c;
if ($c > 1): ?>
	<hr>
<?php endif; ?>
<div class="post post-<? echo $c; ?>">
	<h3 class="post_title">
		<?php if (is_single()): ?>
			<?php the_title(); ?>
		<?php else : ?>
			<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
		<?php endif; ?>
		</h3>
	<p class="byline"><?php the_time('F jS, Y'); ?> | <?php the_author(); ?><!-- | <?php the_category(', ') ?> --> <?php edit_post_link('edit', '<span> | ', '</span>'); ?></p>
	<? if (has_post_thumbnail()) { ?>
		<a href="<?php the_permalink(); ?>">
		<? the_post_thumbnail('thumbnail', array('class'=>'alignleft')); ?>
		</a>
	<? } ?>
	<div class="post-content">
	<?php
	if (is_single()) {
		the_content();
		imprint_share();
		echo '<hr>';
	} else {
		the_excerpt();
	}
?>	</div><!--END post-content-->
</div><!--END post-->