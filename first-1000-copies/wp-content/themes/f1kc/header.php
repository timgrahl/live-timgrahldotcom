<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
<title><?php wp_title('-', true, 'right'); ?> <?php bloginfo('name'); ?></title>
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/bootstrap.css" type="text/css" media="screen">
<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen">
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
<script src="//use.typekit.net/zjr4ica.js"></script>
<script>try{Typekit.load();}catch(e){}</script>
<?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>
<?php wp_head(); ?>
</head>
<body id="<?php page_handle(); ?>">
	<div class="headline-wrap">
		<div class="container">
			<div class="row">
				<div class="span12 headline">
					<a class="tab-top" href="http://timgrahl.com">A project of Tim Grahl</a>
					<h1 class="pagetitle">
						YOUR FIRST 1000 COPIES: <span class="subtitle">the step-by-step guide to marketing your book</span> <span class="caps">by Tim Grahl</span>
					</h1>
				</div>
			</div><!--end row-->
		</div><!--END container-->
	</div><!--end headline-wrap-->
	<div class="clr"></div>