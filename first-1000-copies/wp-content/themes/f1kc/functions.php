<?php
function imprint_social() {
	$facebook = get_option('io_facebook_url');
	$twitter = get_option('io_twitter_handle');
	$linkedin = get_option('io_linkedin_url');
	$youtube = get_option('io_youtube_user');
	$pinterest = get_option('io_pinterest');
	$flickr = get_option('io_flickr');
	$stumble = get_option('io_stumbleupon');
	$tumblr = get_option('io_tumblr');
	?>
	<div class="social">
		<?php if ($facebook != '') { ?>
			<a class="facebook" rel="me" href="<?php echo $facebook; ?>" alt="Connect with <?php echo get_option('io_author_name'); ?> on Facebook">Facebook</a>
		<?php } ?>
		<?php if ($tumblr != '') { ?>
			<a class="tumblr" rel="me" href="<?php echo $tumblr; ?>" alt="Follow on Tumblr">Tumblr</a>
		<?php } ?>
		<?php if ($twitter != '') { ?>
			<a class="twitter" rel="me" href="http://twitter.com/<?php echo $twitter; ?>" alt="Follow <?php echo $twitter; ?> on Twitter">Follow Dan Pink on Twitter</a>
		<?php } ?>
		<?php if ($linkedin != '') { ?>
			<a class="linkedin" rel="me" href="<?php echo $linkedin; ?>" alt="Connect with <?php echo get_option('io_author_name'); ?> on LinkedIn">LinkedIn</a>
		<?php } ?>
		<?php if ($youtube != '') { ?>
			<a class="youtube" rel="me" href="<?php echo $youtube; ?>" alt="Visit <?php echo get_option('io_author_name'); ?>'s Youtube Channel">Youtube</a>
		<?php } ?>
		<?php if ($pinterest != '') { ?>
			<a class="pinterest" rel="me" href="<?php echo $pinterest; ?>" alt="Follow <?php echo get_option('io_author_name'); ?> on Pinterest">Pinterest</a>
		<?php } ?>
		<?php if ($flickr != '') { ?>
			<a class="flickr" rel="me" href="<?php echo $flickr; ?>" alt="<?php echo get_option('io_author_name'); ?>'s Flickr">Flickr</a>
		<?php } ?>
		<?php if ($stumbleupon != '') { ?>
			<a class="stumbleupon" rel="me" href="<?php echo $stumbleupon; ?>" alt="<?php echo get_option('io_author_name'); ?>'s Stumbleupon">StumbleUpon</a>
		<?php } ?>				
	</div>
	
	
<?php
} // END function imprint_social()

function timgrahldotcom_notice_admin_bar_render() {
	global $wp_admin_bar;
	if (get_option('siteurl') == 'http://timgrahl.com/first-1000-copies') {
		$wp_admin_bar->add_menu( array(
			'parent' => false, // use 'false' for a root menu, or pass the ID of the parent menu
			'id' => 'timgrahldotcom-alert', // link ID, defaults to a sanitized title value
			'title' => __("<span style='color:red;'>YOU ARE ON TIMGRAHL.COM! NO CHANGES!!!</span>"), // link title
			'href' => admin_url( 'options-reading.php' ), // name of file
		));
	}
}
add_action( 'wp_before_admin_bar_render', 'timgrahldotcom_notice_admin_bar_render' ); 

?>