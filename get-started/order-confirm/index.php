<?php

$redirect_url = empty($_GET['url']) ? 'timgrahl.com' : $_GET['url'];

?>
<!DOCTYPE HTML>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/>
    <meta http-equiv="Refresh" content="2;URL=http://<?= $redirect_url ?>"/>
    <title>Thanks for registering!</title>
    <style type="text/css">
        body{background: #ccc; font-family: arial, FreeSans, Helvetica, sans-serif}
        .container{ max-width: 400px; margin: 20px auto; text-align: center; color: #646464; line-height: 1.4; font-size: 12px; }
    </style>
    <?php require( realpath($_SERVER['DOCUMENT_ROOT']) . '/get-started/includes/scripts_header.php') ?>
</head>
<body>
<?php require( realpath($_SERVER['DOCUMENT_ROOT']) . '/get-started/includes/scripts_body.php') ?>

<div class="container">
    We're redirecting you now.<br/>
    Not Redirected? <a href="http://<?= $redirect_url ?>">Click here</a> to continue.
</div>

<?php require( realpath($_SERVER['DOCUMENT_ROOT']) . '/get-started/includes/scripts_footer.php') ?>

</body>
</html>
