jQuery(document).ready(function() {
	// this adds "placeholders", or if the browser doesn't support it, adds the label to the text value
	jQuery('.nl-signup-wide label, .front-page-cols label, .mc-field-group label').each(function() {
		var t = jQuery(this);
		var valtext = t.text();
		t.hide();
		if (jQuery.browser.msie) {
			t.next('input').val(valtext);
		} else {
			t.next('input').attr('placeholder', valtext);
		}
	});
	// this function clears text when the text field is focused
	jQuery('input[type=text], textarea, input[type=email]').each( function() {
		var t = jQuery(this);
		var thisval = t.val();
		t.blur( function() {
			if (t.val() === '') {t.val(thisval);}
		}); // end blur function
		t.focus( function() {
			if (t.val() === thisval) { t.val('');}
		});// end focus function
	}); //END each function

	// this replaces an HR with a div class hr...'cause IE sucks
	if (jQuery.browser.msie) {
		jQuery('hr').before('<div class="hr"></div>').remove();
	}
	
	// this makes a form that is in a class called "newsletter-wrap" inlined
	jQuery('.nl-signup-wide form').addClass('form-inline');

	// add placeholdert text for the search field...esp when hiding the label
	if (jQuery('input#s').val() === '') {
		jQuery('input#s').val('search this site...');
	}
	// loads facebook like buttons after the content
	jQuery("span.facelike").each(function() {
		var url = jQuery(this).attr("src");
		jQuery(this).replaceWith('<iframe src=' + url + ' scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:85px; height:21px;" allowTransparency="true" />');
	});
	
	// if the window height is more than the content height, pin the footer to the bottom.
	var bh = jQuery('body').height();
	var wh = jQuery(window).height() + 30;
	if ( bh < wh) {
		jQuery('.footer').addClass('navbar-fixed-bottom');
	}
	// all external links open in  new window
	 jQuery("a").filter(function() {
		 return this.hostname && this.hostname !== location.hostname;
	 }).attr('target', '_blank');
}); // END document ready function