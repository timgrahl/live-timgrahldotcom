function resizer( selectors ) {
	jQuery(selectors).each(function() {
		var w = jQuery(this).parents('div').width();
		var owidth = jQuery(this).attr('width');
		var oheight = jQuery(this).attr('height');
		if (owidth === '100%') {
			owidth = jQuery(this).width();
		}
		if (oheight === '100%') {
			oheight = jQuery(this).height();
		}
		var newH = (oheight * (w / owidth));
		//	alert('new height:'+newH);
//		alert(jQuery(selectors).parents('div').attr('class'));
//		alert('width is:' + w);
		jQuery(this).attr({
			width: w,
			height: newH
		});
	});
}
jQuery(document).ready(function() {
	var resizedElement = 'iframe';
	resizer(resizedElement);
});
jQuery(window).resize(function() {
	var resizedElement = 'iframe';
	resizer(resizedElement);
});
