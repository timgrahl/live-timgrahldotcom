<?php
// Get the days left if set in the URL,
//  else use the value in the cookie
// --------------------------------------
date_default_timezone_set('America/Los_Angeles');
if(isset($_GET['d'])){ $daysLeft = $_GET['d']; }
elseif(isset($_COOKIE['timgrahl_instantbestseller_d'])){ $daysLeft = $_COOKIE['timgrahl_instantbestseller_d']; }
else{ $daysLeft = null; }

if($daysLeft !== null){
    setcookie('timgrahl_instantbestseller_d',$daysLeft, strtotime('+7 days')); // 7 days
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name='robots' content='noindex,follow' />
    <meta name="description"  content="“If I start putting my work out into the world, people are going to realize the truth…” That I’m a fraud... That I’m not the writer I wish I were... That" />
    <link rel="canonical" href="http://timgrahl.com/get-started/instant-bestseller/" />
    <title>Instant Bestseller Course Limited Time Offer | Tim Grahl</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.2/css/font-awesome.min.css">
    <link rel="stylesheet" href="./css/style.css" />


    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

<?php if($daysLeft !== null): ?>
    <style>
        body{padding-top: 34px;}
        .count-down {
            background: #FFCC00;
            position: fixed;
            width: 100%;
            top: 0;
            left: 0;
            z-index: 100;
        }
        .count-down p{
            text-align: center;
            text-transform: uppercase;
            font-weight: 300;
            margin: 0;
            padding:10px 0;
            line-height: 1;
        }
        .count-down .count-down-container{
            max-width: 1280px;
            margin: 0 auto;
            padding: 0 15px;
        }
    </style>
<?php else: ?>
    <style>.count-down{display: none;}</style>
<?php endif ?>

    <?php require( realpath($_SERVER['DOCUMENT_ROOT']) . '/get-started/includes/scripts_header.php') ?>
</head>
<body>
<?php require( realpath($_SERVER['DOCUMENT_ROOT']) . '/get-started/includes/scripts_body.php') ?>
<div class="count-down"><div class="count-down-container" id="countdownTimer"></div></div>

<section class="primary hero" style="padding-bottom: 40px;">
    <div class="container">

        <h1>Learn how to overcome your Imposter Syndrome, Become the Writer You Want to Be, and Launch Your Author Platform.</h1>
        <div class="text-center"><img src="./images/ib-icon.png" style="opacity: .7"></div>

    </div>
</section>


<section class="white">
    <div class="container">

        <h2 class="section-title">“If I start putting my work out into the world, people are going to realize the truth…”</h2>
                <p>That I’m a fraud.</p>
                <p>That I’m not the writer I wish I were&#8230;</p>
                <p>That this whole “being a writer” thing is just a stupid fantasy&#8230;</p>
                <p>We all have feelings like this; and if you’re struggling with doubt of your own “authorness” I have something to tell you:</p>
                <p><strong>Congratulations: you’re officially a writer.</strong></p>

        <div style="position: relative; margin-bottom: 40px;">
            <div class="embed-responsive embed-responsive-16by9">
                <iframe class="embed-responsive-item" src="//player.vimeo.com/video/120493188?color=000000&#038;title=0&#038;byline=0&#038;portrait=0"></iframe>
            </div>
            <div class="callout callout-right" style="top: -60px;">I want to tell you something about launching an instant best seller. Check out this video.</div>
        </div>



            <p>There is not a single writer alive who, when it comes time to face the idea of readers and (worse) reviewers, doesn’t battle doubt, cower in the presence of potential rejection and buckle under the weight of possible public humiliation&#8230;</p>
            <p>I’ve worked with a huge range of authors &#8211; from top New York Times bestselling authors to those just trying to get their first project off the ground &#8211; and they all deal with the exact same paralyzing fears.</p>
            <p>But here’s the thing that separates successful writers from onlookers:</p>

        <h2 class="pullquote">Successful writers take control rather than allowing the fear to make their decisions</h2>
        <p>You’ve been paying attention. You know how publishing works these days.</p>
        <p>You know that marketing, building a platform and actually selling your book isn’t up to the publisher…</p>
        <p>It’s up to you.</p>

                <p><strong>You have to take control of your destiny as a published writer. Nobody—not your publisher, your publicist or your website guy—is going to do it for you.</strong></p>

                <p>And if you don’t do it, the outcome is frankly going to be pretty disastrous.</p>
                <p>I’m not even talking about failing to make money. While you obviously want to make money with your writing, you didn’t get into writing just to get rich. There are lots easier ways to make money.</p>
                <p>No, the first thing that occurs if you don’t actively market your book is…</p>
                <p>&#8230;<b><i>shame</i></b>.</p>
                <p>You’ve been working on that book for months or years. Everybody in your life knows you’re working on a new book&#8230;</p>
                <p>So then, when it comes out and nobody buys it . . . what now?</p>
    </div>
</section>


<section class="alt">
    <div class="container">
        <h2 class="section-title">People are going to ask: “How’s your book doing?”And what are you going to say?</h2>
        <p>And that’s not even the worst part of not selling copies.</p>
        <p>The worst part is this: that beloved book over which you sweated and into which you bled is finally out there, ready for the world… but not a single person is holding it in their hands and <i>reading </i>it.</p>
        <p>Think back to the time you first read a book that really hit you&#8230;</p>
        <p>For me, that was <i>Jurassic Park</i>. It was so terrifying, it seemed to embed itself like teeth clamped onto my memory. I’ll never forget how I loved that book&#8230;</p>
        <p>Fast forward to my adult years, and my tastes are a little different. The books that hit me are different. For example, I remember sitting in a closet at an office job I hated and picking up a copy of <i>Purple Cow </i>by Seth Godin. That book set me on the path to becoming a writer and entrepreneur.</p>
        <p>Surely you remember the tingles books have given you.</p>
        <p>And surely you want to have the same effect on a world of readers&#8230;</p>
        <p><b>That’s why the worst part about your book not selling is that people won’t get to read it.</b></p>
        <p>And because of that, their life lacks some of the shine, some of the magic, maybe some of the hope and encouragement they would have gained had they read it&#8230;</p>
    </div>
</section>


<section class="white">
    <div class="container">
        <h2 class="section-title">The emotional anguish of <i>not selling</i> runs deep, but the financial loss is just as troubling&#8230;</h2>

        <p>Once you’ve invested so much time and energy into a book, it’s not just nice to get paid for it.</p>
        <p>It’s imperative.</p>
        <p><strong>It’s OK to want your book to make money.</strong> <i>Selling </i>does not mean <i>selling out</i>. Selling copies of your book means you can afford to keep writing. After all, you can’t be a full-time writer if you’re not earning an income from it, right?</p>
        <p>But if you don’t sell copies, you don’t make money, and you can’t afford to write.</p>
        <p>So here’s the question: How can you get away from that embarrassment… from that failure&#8230; and from that loss of income?</p>
        <p>You have two clear options here:</p>
        <ol>
            <li><b>Give into the fear:</b> Write your book, put it up for sale and hope it sells.</li>
            <li><b>Conquer the fear:</b> Learn how to do the marketing your book needs using simple, proven methods.</li>
        </ol>
        <p>Give into the fear and get nothing out of it.</p>
        <p>Conquer the fear and put out a book you <i>know </i>will sell.</p>
        <p>Big difference.</p>
        <p>And, yes, it’s 100% possible to put out a book you KNOW will sell. I know it’s possible. Because I’ve done it not once but on repeat &#8211; which I’ll get into shortly. But first:</p>
    </div>
</section>


<section class="alt">
    <div class="container">
                <h2 class="section-title">How many authors would rather nervously <em><u>hope</u></em> their book becomes a bestseller than <em><u>know</u></em> their book will be a success...</h2>

                <p>So what is holding you back from actually knowing your book will sell?</p>
                <p><b>While you ponder that question, listen to this story. </b>One day not long ago, I was sitting on a train heading into Washington DC, getting ready to share a crucial plan with my biggest client.</p>
                <p>I was about to show him how we were going to take all the work we’d done for the previous three years and use it to launch his book as an instant bestseller. That’s what he’d hired me for, after all. No pressure, right? <strong>I just have to turn his book into an instant bestseller</strong>. Sure. Easy.</p>
                <p>On the overhead shelf was my flipchart with my plan sketched out in black Sharpie. In front of me were my notes. I was going over them for the twelfth time, wondering if I’d missed anything.</p>
                <p>Three years of work had led to this point.</p>
                <p>And, let’s not kid ourselves: I was mind-numbingly, gut-churningly scared I would totally blow it.</p>
                <p>You see, New York Times bestselling author Daniel Pink had told me that he was foregoing his usual book launch strategies.</p>
                <p>He was trusting my strategies alone to make his book a bestseller.</p>
                <p>Like many authors, he’d had success in the past. But there was no guarantee he’d repeat it.</p>

                <h2 class="pullquote">He needed a way to guarantee a bestseller each time he launched something new &#8211; and it was my job to help him do just that</h2>

                <p>I shared the plan with Dan. He signed off on it.</p>
                <p>And, three years after I started working on his platform, we launched his book <i>To Sell Is Human</i>.</p>
                <p>The payoff? Huge.</p>
                <p>It quickly hit #1 on the New York Times, Wall Street Journal and Washington Post bestseller lists—all at the same time.</p>
                <p>My campaign was the determining factor behind the book launching so high—higher than any of Dan’s previous books.</p>
                <p>You’d think I’d be excited at the prospect of creating a gangbuster. And I was. For a little while.</p>
                <p>Then I thought, “Well, maybe it was just a fluke. Maybe his book would have hit #1 without my efforts.”</p>
                <p>Thankfully, I got a chance to test my methods again a few months later, when I ran the book launch for <i>Decisive </i>by Chip Heath and Dan Heath.</p>
                <p>Once again, they didn’t hire a publicist or any other outside firm. They just hired me.</p>
                <p>The result? An instant #2 New York Times bestseller&#8230;</p>
    </div>
</section>


<section class="white">
    <div class="container">
        <h2 class="section-title">That same week I had not one&#8230; not two&#8230; but <i>five </i>clients sitting with bestsellers, all at the same time</h2>

        <hr class="thin" />
        <div class="speakers clearfix">
            <div class="speaker-img" title="Daniel Pink"><img src="images/hs-daniel-pink.jpg" class="img-responsive" /></div>
            <div class="speaker-img" title="Charles Duhigg"><img src="images/hs-duhigg.jpg" class="img-responsive" /></div>
            <div class="speaker-img" title="The Heath Brothers"><img src="images/hs-heath-brothers.jpg" class="img-responsive" /></div>
            <div class="speaker-img" title="Hugh Howey"><img src="images/hs-hugh-howey.jpg" class="img-responsive" /></div>
            <div class="speaker-img" title="Michael Moss"><img src="images/hs-michael-moss.jpg" class="img-responsive" /></div>
        </div>
        <hr class="thin" />

        <p><strong>My clients Daniel Pink, Hugh Howey, Charles Duhigg, the Heath brothers and Michael Moss all had books on the New York Times bestseller list that week. Take a look:</strong></p>
        <img class="img-responsive" src="/get-started/instant-bestseller/images/bestseller-image.png" style="margin: 20px auto;" />

        <h2 class="pullquote" style="font-family: sans-serif;">Σ(゜ロ゜;)</h2>

        <p>And that’s when it really sunk in…</p>
        <p><strong>This stuff works!</strong></p>
        <p>After three years of development, I finally had real-world proof that the author platforms I was building helped writers get a mammoth boost in book sales.</p>
        <p>But maybe you’re thinking: “Of course it works for traditionally published authors getting huge advances&#8230;</p>

        <h3 class="pullquote">“But what about me, a no-name author just getting started?”</h3>

        <p>That exact thought crept into my head when I launched my self-published book <i>Your First 1000 Copies: The Step-by-Step Guide to Marketing Your Book</i>.</p>
        <p>My platform was tiny because I’d spent nearly all my time building my clients’ followings instead of my own&#8230;</p>
        <p>I had less than 3% of the number of readers many of my clients had.</p>
        <p>And the more I dwelled on that fact, the more nervous I got.</p>
        <p>One thought just kept grinding away inside my head&#8230;</p>

        <hr class=""/>
        <h2>What if my book about<br />
            <i>book marketing</i> was a total flop?<br />
            I’d be a laughing stock</h2>
        <hr class=""/>

        <p>I could practically hear my competitors snickering already.</p>
        <p>But I <b>had to prove</b> that my techniques for building author platforms worked just as well for lesser-known writers.</p>
        <p>So I implemented the exact same process I use for every one of my clients. And right away, those anxious feelings faded…</p>
        <p>…because my small efforts to build an author platform paid off big time.</p>
        <p><b>My little self-published book soared to the #1 spot in all of its Amazon categories.</b></p>
        <p>This included the extremely competitive Business &amp; Investing &gt; Marketing category.</p>
        <p>And that’s when I finally became confident that the platform-building techniques I had spent years refining worked not just for traditional authors – but all types of writers.</p>
        <p>The whole nerve-wracking experience taught me that…</p>
    </div>
</section>

<section class="alt">
    <div class="container">
        <h2 class="section-title">Building an author platform to launch<br />
            a successful book is something <b>any author, in any category, with any level of fame</b> can do</h2>
        <p>Maybe you’re afraid right now, just like I was when building up my own author platform.</p>
        <p>Perhaps you’re worried about not making any money.</p>
        <p>Or disappointing the people who believe in you.</p>
        <p>All those things crossed my mind when I started my business too.<br/><b>And that fear distracted me from doing the things that I knew I needed to do in order to be successful.</b></p>
        <p>Now, here’s the stark truth: that fear never goes away. But you can learn to work through it.</p>
        <p>The trick is to have a system in place that you can rely on. A friend once taught me that the way to work and produce while being afraid is to know exactly what you should be working on and how to go about doing it well.</p>
        <p>Once you have that framework – that guidance – in place, suddenly, the fear doesn’t stop you anymore. You become razor-focused on your goals.</p>
        <p><b>This is what my Instant Bestseller course is all about.</b></p>
        <p>If you’re in a place where all of that swirling, confusing advice about marketing tools—like Twitter, Facebook, blogging, podcasting, advertising, emailing—it will all swiftly calm down.</p>
        <p>With Instant Bestseller, you’ll know <i>exactly</i> what you should be doing to sell as many books as possible&#8230;</p>
        <p>Just imagine watching your e-mail subscriber list swell or your Twitter following grow daily—all by taking just a few small steps in the right direction. Hundreds of other writers have already experienced this by applying what they learned from Instant Bestseller. In fact&#8230;</p>
    </div>
</section>

<section class="white">
    <div class="container">

        <h2 class="section-title">You might be surprised by what some authors have achieved after taking my course…</h2>

        <blockquote class="testimonial">
            <h3>Tim helped build my successful indie career</h3>
            <p>&#8220;As an indie fiction author, I wasn&#8217;t sure if Tim&#8217;s ideas would work for me, but now I&#8217;m a firm believer. His advice and wise counsel has helped build my successful indie career.&#8221;</p>
            <cite>&mdash; Michael Bunker, bestselling author of <em>Pennsylvania</em> and <em>WICK</em></cite>
        </blockquote>

        <blockquote class="testimonial">
            <h3>I used to circle endlessly from blog post to newsletter signup</h3>
            <p>These days there are as many people telling us how to market our books as there are books being self-published. It is indeed a Wild West frontier. I was one of those who circled endlessly from blog post to newsletter signup to the next great article headline and then to the next.</p>
            <p><strong>I have two vivid and invaluable understandings from Tim that are now embedded in my marketing perspectives:</strong></p>
            <p><strong>First: </strong>It&#8217;s OK, even preferable, to conduct marketing in phases and stages over time. There is only so much that even we overachieving, perpetually overwhelmed bootstrappers can accomplish at once. Take a step back, place your endless ideas into separate baskets and then take Basket One. Do it right and do it well. Then go to Basket Two.</p>
            <p><strong>Second:</strong> As a reasonably experienced online entrepreneur, I thought I understood the importance of an email list. After working with Tim, this idea has become an inviolate fundamental of every step that I take with my business.</p>
            <cite>&mdash; Jennifer Lyle</cite></blockquote>

        <blockquote class="testimonial">
            <h3>It helped me connect with readers in a meaningful way</h3>
            <p>&#8220;Instant Bestseller came along right when I needed it, as I was in the middle of designing a new author site to coincide with three book releases in 2015. This course was invaluable in helping me figure out how to shape my new site and connect with my readers in a meaningful and mutually beneficial way. Tim’s been my go-to source for all things book marketing, and for me this course took my level of learning and strategic planning to the next level. Tim’s a generous and knowledgeable teacher and his content is simply the best. I can’t recommend highly enough!&#8221;</p>
            <cite>&mdash; Debbie Reber, author of <em>Doable</em></cite></blockquote>

        <blockquote class="testimonial">
            <h3>Who else wants to double their list of email subscribers?</h3>
            <p>Tim is a sage when it comes to growing your influence and connection with readers and future readers. My only regret is not learning from him sooner.</p>
            <p>Since starting the Instant Bestseller course, my newsletter subscribers have more than doubled and my &#8220;open&#8221; rates on newsletter emails have increased by 33 percent.</p>
            <cite>&mdash; David Burkus, author of <em>The Myths of Creativity</em></cite></blockquote>

    </div>
</section>

<section class="primary thin">
    <div class="container">
        <h2>And that’s on top of what bestselling authors are saying about the platforms I built for them…</h2>

        <blockquote class="testimonial white" style="max-width: 680px; margin: 60px auto 0 auto;">
            <h3>Tim Grahl is fast becoming a legend, almost single-handedly changing the way authors around the world spread ideas and connect with readers.</h3>
            <cite>&mdash; Dan Pink, Bestselling Author of <em>Drive</em></cite>
        </blockquote>

    </div>
</section>

<section class="alt">
    <div class="container">
        <h1 class="section-title">Introducing: Instant Bestseller</h1>
        <h3>The <u>only</u> proven step-by-step online guide that has helped hundreds of authors launch their first bestseller.</h3>

        <p>And it’s enabled the sale of hundreds of thousands of copies of books in every genre, both fiction and nonfiction…</p>
        <p>Here’s how it’s going to work.</p>
        <p>In 5 video sessions, you’ll get the step-by-step lessons you’ll need to build and grow your platform. I’ll walk you through every part of the process. In fact, you’ll learn everything I would do if you hired me to do this work for you. (My client roster is full, but I used to charge $300+ per hour.)</p>
        <p><b>You’ll discover precise and fluff-free strategies, tools and systems that will help you: </b></p>
        <ul class="checkmark">
            <li> <strong>Establish your fan base</strong> &mdash; even if you don't have one already</li>
            <li> <strong>Connect with your fans easily and regularly</strong> to build a group of dedicated followers</li>
            <li> <strong>Continually grow your fan base</strong> and avoid the ever-looming revenue cliff of death</li>
            <li> <strong>Reliably sell more books</strong> month after month, and year after year</li>
        </ul>

        <hr/>


        <h2 style="text-align: center;">There’s a glut of author courses promising similar results&#8230;<br />
            <b>But most of them have two major problems:</b></h2>
        <ol>
            <li><b>They’re based on methods </b><b><i>that worked for just one author</i></b>—not hundreds of authors in dozens of different genres like Instant Bestseller. Just because one person was successful with a certain method doesn’t mean you will be too.</li>
            <li><strong>They’re based on untested “expert theory” instead of real-world experience</strong>. The content in Instant Bestseller <strong>has been tested in real life over and over again</strong>. Plus it has worked for authors with a wide variety of experience levels.</li>
        </ol>
        <p>So when you enroll in Instant Bestseller, you’ll be getting proven advice to help you create an author platform that works—no matter what type of writer you are.</p>
    </div>
</section>

<section class="white thin">
    <div class="container">

        <blockquote class="testimonial">
            <h3>For authors who want to be ahead of the curve</h3>
            <p>Tim Grahl’s course is a treasure trove of insight about how to build lasting connections with an audience. I recommend this course for any author who wants to be ahead of the curve for platform development.”</p>
            <cite>&mdash; Scott James, Brand Strategist at Greenleaf Book Group</cite>
        </blockquote>

        <blockquote class="testimonial">
            <h3>Field-tested strategies that get results</h3>
            <p>&#8220;Instant Bestseller is a tantalizing concoction of best practices prepared just for authors. Tim Grahl&#8217;s field-tested strategies are distilled into a proven system that gets results. No wonder he&#8217;s a top choice in our industry.&#8221;</p>
            <cite>&mdash; Kary Oberbrunner, author of <em>Day Job to Dream Job</em>, <em>The Deeper Path</em>, and <em>Your Secret Name</em></cite>
        </blockquote>

    </div>
</section>

<section class="alt">
    <div class="container">

        <h2 style="text-align: center;">By the time you’re done with this course, you’ll know exactly how to…</h2>
        <ul class="checkmark">
            <li><b>Use social media, from Facebook to Instagram, to build a platform that sells </b>— we explain our method using case studies and specific tests</li>
            <li><b>Multiply your fan base</b> — we show you, step by step, how to find new readers where you never thought to look and connect them to your writing</li>
            <li><b>Handle the technology needed for success</b> — we give you both the tools and the direction to easily create an online platform even if tech scares you and your “smart phone” is a brick phone from the 80s</li>
        </ul>
        <p>When I built the Instant Bestseller course, I focused on making a system that any author could start implementing immediately&#8230;</p>
        <p>I broke it down into a step-by-step, easy-to-follow process, so you can jump right in and start benefiting from this method as soon as you sign in.</p>
    </div>
</section>

<section class="white">
    <div class="container">

        <h2>Here’s what we’ll cover in more than 40 bite-sized video sessions</h2>
        <p>The course spans more than 8 hours, diced up into easy-to-consume lessons that you can squeeze in while taking a break from writing.</p>

        <hr />

        <div class="row">
            <div class="col-sm-5"><img class="img-responsive" src="/get-started/instant-bestseller/images/icon-module1.png" /></div>
            <div class="col-sm-7">
                        <p>Learn how to lay the groundwork for an author platform that will make it easier to connect with (and grow) your audience.</p>
                        <ul class="chevron">
                            <li>Book marketing &#8211; it’s not what you think it is</li>
                            <li>Every book is a business &#8211; just ask best-selling authors</li>
                            <li>A little-known technique to make your life easier as a writer</li>
                            <li>Why guess and test? Use my proven blueprints</li>
                            <li>Not everything works every time &#8211; how to know when to switch</li>
                        </ul>
            </div>
        </div>
    </div>
</section>

<section class="gray">
    <div class="container">
        <div class="row">
            <div class="col-sm-5"><img class="img-responsive" src="/get-started/instant-bestseller/images/icon-module2.png" /></div>
            <div class="col-sm-7">
                        <p>I share exactly what you should be doing to build long-lasting relationships with readers.</p>
                        <ul class="chevron">
                            <li>The secret to how social media really works</li>
                            <li>Your #1 marketing goal (hint: you check it 25 times a day)</li>
                            <li>How to connect with thousands of new readers</li>
                            <li>The nuts and bolts of sending winning emails</li>
                            <li>Unexpected but proven ways to win more readers</li>
                        </ul>
            </div>
        </div>
    </div>
</section>

<section class="white">
    <div class="container">
        <div class="row">
            <div class="col-sm-5"><img class="img-responsive" src="/get-started/instant-bestseller/images/icon-module3.png" /></div>
            <div class="col-sm-7">
                        <p>Top-notch content is essential if you want to build an author platform. But creating it often takes a lot of work… unless you know what to do. See how to easily – and consistently – create content your readers will be scrambling to consume, share and comment on.</p>
                        <ul class="chevron">
                            <li>Time-saving tactics for creating new content</li>
                            <li>Where to find a practically endless supply of writing ideas</li>
                            <li>Simple ways to maximize your content’s reach &amp; impact</li>
                            <li>Email marketing shortcuts that will captivate your readers</li>
                            <li>The busy writer’s guide to creating a content plan</li>
                        </ul>
            </div>
        </div>
    </div>
</section>

<section class="gray">
    <div class="container">
        <div class="row">
            <div class="col-sm-5"><img class="img-responsive" src="/get-started/instant-bestseller/images/icon-module4.png" /></div>
            <div class="col-sm-7">
                        <p>You’ll get a simple-to-follow plan for turning strangers into friends and friends into loyal readers.</p>
                        <ul class="chevron">
                            <li>Where to find new readers &#8211; no matter what genre you write in</li>
                            <li>How to connect with influencers who can multiply your following</li>
                            <li>The art of the pitch &#8211; every author needs to get this right</li>
                            <li>Yes, you should do live events… and yes, I’ll show you how</li>
                            <li>A step-by-step plan for outreach that produces “inreach”</li>
                        </ul>
            </div>
        </div>
    </div>
</section>

<section class="white">
    <div class="container">
        <div class="row">
            <div class="col-sm-5"><img class="img-responsive" src="/get-started/instant-bestseller/images/icon-module5.png" /></div>
            <div class="col-sm-7">
                        <p>This is where you create a reliable plan – based on everything you’ve learned – that could easily amplify any author’s platform.</p>
                        <ul class="chevron">
                            <li>8 ways to build a successful platform (discover which one of them is perfect for you)</li>
                            <li>Lesson-packed case studies that reveal how top authors found success</li>
                            <li>The most common platform pitfalls&#8230; and how to sidestep them</li>
                        </ul>
            </div>
        </div>
    </div>
</section>

<section class="alt">
    <div class="container">
        <hr class="thin" />
        <p><b>Instant Bestseller is action-focused.</b> Its instantly applicable homework assignments and worksheets
            will help you ensure that every minute you spend learning is a minute you put toward growing your book
            business. Plus:</p>
        <ul class="chevron">
            <li>Every single lesson is available as an MP3 you can download to your mobile device</li>
            <li>You can go through the entire course on a desktop computer, tablet or smartphone</li>
            <li>You can download every PDF, spreadsheet and worksheet</li>
            <li>You’ll get 3 years of unlimited access to the course &#8211; plus updates, upgrades and new content as I add it</li>
        </ul>
        <hr class="thin" />
    </div>
</section>

<section class="primary">
    <div class="container">
        <h2 class="section-title">Plus, for this special offer, I’m adding in two special bonuses that will make building your bestseller platform easier.</h2>
        <p>The hardest part of getting started isn’t <strong>not knowing what to do</strong>, but rather not having a jump-off spot to launch your platform. These two content building kits that I use for my consulting clients will help you kick-start growing your audience, in an easy copy & paste format.</p>
        <p>Even if you spent weeks researching online, you still wouldn’t be able to find the type of in-depth information, worksheets and tools that I’m giving you for free with the course.</p>

        <hr />

        <div class="row">
            <div class="col-sm-2">
                <img class="img-responsive" src="./images/icon-email.png" />
            </div>
            <div class="col-sm-10">
                <h3 class="" style="margin-top: 0; text-align: left; margin-bottom: 10px;"><strong>Bonus 1</strong>: The Essential E-mail Kit For Authors</h3>
                <p>Stop wasting time trying to figure out ‘what works’ on your audience. Instead, use the very same email frameworks and templates I’ve been using and refining for years.</p>
                <ul class="plus li-white">
                    <li><b>Autoresponders – </b>The first three emails you should send to every subscriber.</li>
                    <li><b>Email Signup –  </b>The 5 different templates I use to get more email subscribers.</li>
                    <li><b>Email Templates –</b> The 4 types of emails to send to your subscribers, and exactly what content to put in them.</li>
                    <li><b>The Pitch – </b>The exact emails I use to connect with influencers, which opens up opportunities to write guest posts or do interviews.</li>
                </ul>
            </div>
        </div>

        <hr />

        <div class="row">
            <div class="col-sm-2">
                <img class="img-responsive" src="./images/icon-worksheet.png" />
            </div>
            <div class="col-sm-10">
                <h3 class="" style="margin-top: 0; text-align: left; margin-bottom: 10px;"><strong>Bonus 2</strong>: The Exact Worksheets I Use With Clients</h3>
                <p>These are the documents I used to help authors like Daniel Pink, Hugh Howey and Charles Duhigg get on the <i>New York Times</i> bestseller list.</p>
                <ul class="plus li-white">
                    <li><b>Outreach</b> – A worksheet to identify new readers, where they are located and how to connect with them.</li>
                    <li><b>Outreach Spreadsheet</b> – The spreadsheet I use to plan and execute my outreach.</li>
                    <li><b>Content Worksheet</b> – My step-by-step guide to coming up with valuable content for readers.</li>
                    <li><strong>Email Checklist</strong> – Use this to ensure you have everything in place to build your list and email your readers.</li>
                </ul>
            </div>
        </div>

    </div>
</section>

<section class="white">
    <div class="container">

        <h2 class="section-title"><strong>This isn’t just marketing theory &mdash;</strong> <br/>
            You’ll learn tested techniques that have already helped hundreds of authors sell more books
        </h2>
        <p>In all the work I’ve done over the last six years with hundreds of authors, I’ve learned what works amazingly well&#8230; and what doesn’t work at all.</p>
        <p>I’ve gone out into the world, tried everything I can think of, learned by trial and error, and figured out the most effective, time-saving ways to connect with readers and sell more books.</p>
        <p><b>And now, I’ve put everything I know into this newly upgraded course.</b></p>
        <p>You can keep going the way you’re going—a few unstructured, unproven actions here and there. But if you’re still reading this, you know you need some help to get you where you want to go.</p>
        <p><b>This course is the help you’ve been looking for.</b></p>
        <p>It will help you cut through all of the misinformation out there and work on the things that will get you the results you’ve dreamt of. Join hundreds of authors who have already discovered that…</p>

    </div>
</section>

<section class="alt">
    <div class="container">

        <h2 class="section-title">The <b>Instant Bestseller Course</b> is the fastest, easiest way to build a sales-boosting author platform</h2>
        <p><strong>If you don’t agree after trying it, I’ll give you a full refund within 30 days.</strong></p>
        <p>I’m confident in that guarantee because no other online course shows authors tested techniques that allow you to:</p>
        <ul class="checkmark">
            <li>Build a fan base of loyal readers who will be scrambling to hear from you</li>
            <li> Easily grow that base using the latest tools, strategies and marketing templates</li>
            <li> Watch your book sales soar as you get more attention, gain more influence and earn more respect as an author</li>
        </ul>

        <p><strong>And on top of the video course, you’ll get 2 bonuses&#8230;</strong></p>
        <ul>
            <li>The Essential E-mail Kit For Authors</li>
            <li>The Exact Worksheets I Use With Clients</li>
        </ul>

        <h3 class="pullquote" id="get-started">To get started, choose which option is right for you:</h3>

        <div class="product-box">
            <h2 class="section-title">Option #1:<br/>The Ultimate Instant Bestseller</h2>
            <p class="text-center">
                <img style="max-width: 100%;" src="/get-started/instant-bestseller/images/img-tier2-2.png" />
            </p>
            <p>
                With the Ultimate Instant Bestseller, you'll get the full Instant Bestseller course &mdash; including more than 8 hours of video and audio,
                ready-to-use worksheets, email swipe files and more &mdash; <strong>PLUS</strong> access to the Ultimate Author Platform to help you get
                your marketing site up and ready to start building your followers.
            </p>

            <h3>Join Today & Get...</h3>
            <ul class="check">
                <li>More than 8 hours of video covering all the topics (<b>Foundation Marketing, Permission, Content, Outreach, Pulling it all together</b>)</li>
                <li>Downloadable audios of each lesson</li>
                <li>Customizable, downloadable worksheets for building your own plan</li>
                <li>Online homework to ensure you get the most out of the lessons</li>
                <li>On-page note-taking to save all the important points as you go through the lessons</li>
                <li>The exact copy and wording I use for emails and websites to get results</li>
            </ul>

            <h4 class="pullquote">PLUS, the Ultimate Author Platform</h4>

            <p>Joseph Hinson and Tim Grahl have worked together for almost a decade and have built the online platforms for some of today&#8217;s top authors. In this program, you will get access to:</p>
            <ul class="plus">
                <li><strong>The exclusive, proprietary Imprint WordPress theme</strong>. Easy to use and makes your site beautiful out-of-the-box.</li>
                <li><strong>Marketing training from Tim Grahl</strong> on how to get the most out of your platform.</li>
                <li><strong>Step-by-Step Walkthroughs</strong>. Is technology tough for you? No more worries there. Joseph walks you through every part of your author platform to make sure you can get it up and running quickly.</li>
                <li><strong>Your Website</strong>. Everything you need to get a beautiful author website up and running quickly that is built to connect with your fans and sell books.</li>
                <li><strong>Your Email List</strong>. Step-by-step instructions on how to get your email list setup AND integrate it with your website so you can quickly turn website visitors into email subscribers.</li>
                <li><strong>Your Social Media</strong>. Integrate your social media with your online platform so you can continue to build your audience</li>
                <li><strong>MyBooks plugin</strong>. Make it easy to manage your books on your website. Easily add your cover, blurbs, descriptions, etc.</li>
                <li><strong>MemberGate plugin</strong>. Put any content behind an email list signup. Create your own resource center or free book section that is only accessible if people join your email list. <em>This is the best way to build your email list!</em>.</li>
                <li><strong>Easy Reviews plugin</strong>. Keeping your book reviews, testimonials, and blurbs organized on your site can be extremely challenging. This plugin makes it a simple process.</li>
            </ul>

            <hr class="thin" />

            <div class="text-center">
                <p style="margin: 0;">Get Instant Access For</p>
                <h3 style="margin: 5px 0 0 0; color: #ccc; text-decoration: line-through">$1400</h3>
                <h2 class="section-title">6 Payments of $99</h2>
                <a href="https://timgrahl.samcart.com/products/instant-bestseller--ultimate-author-platformh6B3J" class="btn btn-primary btn-cta">Get Instant Access</a><br/>
                <img src="./images/cards.png" class="cc-icons">
                <a href="https://timgrahl.samcart.com/products/instant-bestseller-ultimate-author-platform-single-payment"><small>Or Save $98 &mdash; pay in full</small></a>
            </div>

        </div>

        <div class="product-box">

            <h2 class="section-title">Option #2:<br/>The Instant Bestseller Course</h2>
            <p class="text-center">
                <img style="max-width: 100%;" src="/get-started/instant-bestseller/images/img-tier1.png" />
            </p>
            <p>
                With the Instant Bestseller Course, you'll get the full Instant Bestseller course &mdash; including more than 8 hours of video and audio,
                ready-to-use worksheets, email swipe files and more.
            </p>

            <ul class="strike">
                <li><strong>The exclusive, proprietary Imprint WordPress theme</strong>. Easy to use and makes your site beautiful out-of-the-box.</li>
                <li><strong>Marketing training from Tim Grahl</strong> on how to get the most out of your platform.</li>
                <li><strong>Step-by-Step Walkthroughs</strong>. Is technology tough for you? No more worries there. Joseph walks you through every part of your author platform to make sure you can get it up and running quickly.</li>
                <li><strong>Your Website</strong>. Everything you need to get a beautiful author website up and running quickly that is built to connect with your fans and sell books.</li>
                <li><strong>Your Email List</strong>. Step-by-step instructions on how to get your email list setup AND integrate it with your website so you can quickly turn website visitors into email subscribers.</li>
                <li><strong>Your Social Media</strong>. Integrate your social media with your online platform so you can continue to build your audience</li>
                <li><strong>MyBooks plugin</strong>. Make it easy to manage your books on your website. Easily add your cover, blurbs, descriptions, etc.</li>
                <li><strong>MemberGate plugin</strong>. Put any content behind an email list signup. Create your own resource center or free book section that is only accessible if people join your email list. <em>This is the best way to build your email list!</em>.</li>
                <li><strong>Easy Reviews plugin</strong>. Keeping your book reviews, testimonials, and blurbs organized on your site can be extremely challenging. This plugin makes it a simple process.</li>
            </ul>

            <h3>Join Today & Get...</h3>
            <ul class="check">
                <li>More than 8 hours of video covering all the topics (<b>Foundation Marketing, Permission, Content, Outreach, Pulling it all together</b>)</li>
                <li>Downloadable audios of each lesson</li>
                <li>Customizable, downloadable worksheets for building your own plan</li>
                <li>Online homework to ensure you get the most out of the lessons</li>
                <li>On-page note-taking to save all the important points as you go through the lessons</li>
                <li>The exact copy and wording I use for emails and websites to get results</li>
            </ul>

            <hr class="thin" />

            <div class="text-center">
                <p style="margin: 0;">Get Instant Access For</p>
                <h3 style="margin: 5px 0 0 0; color: #ccc; text-decoration: line-through">$600</h3>
                <h2 class="section-title">4 Payments of $57</h2>
                <a href="https://timgrahl.samcart.com/products/instant-bestseller9ab6p" class="btn btn-primary btn-cta">Get Instant Access</a><br/>
                <img src="./images/cards.png" class="cc-icons">
                <a href="https://timgrahl.samcart.com/products/instant-bestseller-single-payment"><small>Or Save $32 &mdash; pay in full</small></a>

            </div>

        </div>

    </div>
</section>

<section class="grey">
    <div class="container">
        <h2 class="section-title">Instant Bestseller Isn't For Everyone...</h2>

        <hr class="thin" />

        <p><b>Q: I write fiction. Will this work for me?</b></p>
        <p><b>A:</b> I’ve worked with a lot of fiction writers, and I can assure you that building an author platform is just as important for you as it is for nonfiction writers.</p>
        <p>Imagine if you were directly connected to thousands of your readers, and you knew they were excited about reading your next book, even before you’d penned the first word. That’s what this course can do for you.</p>

        <hr class="thin" />

        <p><b>Q: I’ve already read your book, should I buy the course too?</b></p>
        <p><b>A:</b> My book is the 101 course. This program is the 201 and 301 course.</p>
        <p>In each level, I dive deeper into all of the topics, include a lot more examples, and offer worksheets, spreadsheets and homework to help you implement all of the ideas.</p>

        <hr class="thin" />

        <p><b>Q: I’m not very technical. Will I be able to do everything you teach?</b></p>
        <p><b>A: </b>I’ve worked very hard to simplify all of the ideas and offer specific tools on how to build a great platform.</p>
        <p>The amazing thing about this day and age is that there are so many easy-to-use digital and online tools, with great support to get everything set up.</p>
        <p>Throughout the course, I offer advice on what tools to use and how to use them. You may not implement every single tool, but the vast majority of the technology I recommend is very user-friendly.</p>

        <hr class="thin" />

        <p><b>Q: Can I get my money back if I’m not happy?</b></p>
        <p><b>A:</b> Yes, of course! If you buy the course and it’s not a good fit, just let me know within 30 days, and I’ll give you a full refund.</p>
        <p>The only thing I’ll ask is that when you request the refund, you let me know a couple specifics as to why the course wasn’t a good fit for you – I’m always looking to improve the course.</p>

        <hr class="thin" />

        <p><b>Q: I have a more specific question. Can you answer it?</b></p>
        <p><b>A:</b> I sure can. Just email me directly at <a href="mailto:tim.grahl@gmail.com">tim.grahl@gmail.com</a> or or click that little “snap chat” button in the lower right-hand corner, and ask away!</p>
        <p>And through the whole course you’ll get regular email check-ins from me to make sure you’re moving in the right direction.</p>
    </div>
</section>

<section class="alt">
    <div class="container">

        <h2 class="section-title">Launching a bestseller is tough. It’s even tougher when you don’t have a roadmap.</h2>
        <p>It’s time to make a decision &mdash; are you prepared to do what it takes, and launch a bestseller that  will establish your reputation as an author while providing income and stability for you and your family for years to come?</p>
        <p>If you’re ready to invest in yourself, and believe in your work, don’t wait for that magical day in the future. Don’t wait for some day when you’ve gotten your book “just right” &mdash; that day will never come.</p>
        <p>You have the proof that Instant Bestseller works. You’ve seen results from newly bestselling authors in every genre: novelists, business writers, marketers, technical writers, and people just like you.</p>
        <p>If you’re not ready now, when will you be? As they say: They best time to plant a tree was 20 years ago. <strong>The second best time is today</strong>.</p>
        <p>The decision is yours to make.</p>

        <p>
            Tim Grahl<br/>
            <img src="/get-started/instant-bestseller/images/tim-signature-e1424719026488.png" alt="tim-signature"/>
        </p>

        <p>
            <small><strong>PS:</strong> If you’re completely lost about how online marketing works – and don’t want to waste
            time trying to figure all this stuff out – Instant Bestseller is perfect for you. Just imagine watching your
            subscribers, social media followers and book sales grow right from the moment you start implementing a few
            easy-to-follow techniques.</small>
        </p>

        <div class="text-center">
            <a href="#get-started" class="btn btn-primary btn-cta">Get Started Today</a><br/>
        </div>

    </div>
</section>

<section class="footer text-center">
    <div class="container">
        All Content &copy; <?= date('Y') ?> Tim Grahl
    </div>
</section>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
<script type='text/javascript' src='/get-started/instant-bestseller/dym.timer.js'></script>
<script type="text/javascript">
    jQuery(function(){
        jQuery('#countdownTimer').DYMTimer({
            endTime: <?= strtotime("+{$daysLeft} days 23:59:59 America/Los_Angeles") ?>,
            endMessage: "We're redirecting you now!",
            template: '<p>This Offer ends in <strong>{{DAY}}</strong> days <strong>{{HOUR}}</strong> hours and <strong>{{MIN}}</strong> min</p>',
            redirectUrl: 'https://www.timgrahl.com/'
        });
    });
</script>

<?php require( realpath($_SERVER['DOCUMENT_ROOT']) . '/get-started/includes/scripts_footer.php') ?>

</body>
</html>
