<div id="registerBox" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="myModalLabel">Register for Resources</h3>
	</div><!--END modal-header-->
	<div class="modal-body">
		<?php
		$otmg_settings = get_option('otmg_settings');
		if (!is_page($otmg_settings['login_page'])): 
			echo do_shortcode('[otmg_register redirect="'.get_permalink(get_the_ID()).'"]');
		else :
			echo do_shortcode('[otmg_register]');
		endif; ?>
	</div>
</div>