<?php get_header(); ?>		
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="wrapper">
		<div class="resource-single">			
			<?php $c = 1; ?>
			<?php if (have_posts()) : ?>
				<?php while (have_posts()) : the_post(); ?>
					<div class="resource-thumb">
						<?php the_post_thumbnail('medium', array('title' => $post->post_title)); ?>
					</div><!--END span4-->
					<div class="resourcecontent">
						<h1><?php the_title(); ?></h1>
				
						<div class="details">
							<?php
								if (!isset($_COOKIE["tgcookie"]) and (get_post_meta(get_the_ID(),'email_block',true) != 'false')) { 
									the_excerpt();
									?>
									<?php if(isset($_GET['ERR'])) { 
										if($_GET['ERR'] == 1) { ?>
											<div class="flash-error">
											  <span>I can't find that email address. Try clicking the register button instead.</span>
											</div>
									<?php } else if($_GET['ERR'] == 2) { ?>
											<div class="flash-error">
											  <span>There was a problem with your registration. Give it another try.</span>
											</div>
									<?php }
									} ?>
									 <p><strong>Enter your email address to immediately access:</strong></p>
									<form id="ck_subscribe_form" method="POST" class="ck_subscribe_form otmg_form otmg_login_form" action="/resources-thanks" data-remote="true">
									      <input type="hidden" value="{&quot;form_style&quot;:&quot;naked&quot;,&quot;embed_style&quot;:&quot;inline&quot;,&quot;embed_trigger&quot;:&quot;scroll_percentage&quot;,&quot;scroll_percentage&quot;:&quot;70&quot;,&quot;delay_seconds&quot;:&quot;10&quot;,&quot;display_position&quot;:&quot;br&quot;,&quot;display_devices&quot;:&quot;all&quot;,&quot;days_no_show&quot;:&quot;15&quot;,&quot;converted_behavior&quot;:&quot;show&quot;}" id="ck_form_options"></input>
									      <input type="hidden" name="id" value="10045" id="landing_page_id"></input>
									      <div class="ck_errorArea">
									        <div id="ck_error_msg" style="display:none">
									          <p>There was an error submitting your subscription. Please try again.</p>
									        </div>
									      </div>
									      <div class="ck_control_group ck_email_field_group">
									        <label class="ck_label" for="ck_emailField" style="display: none">Email Address</label>
									          <input type="text" name="first_name" class="ck_first_name" id="ck_firstNameField" placeholder="First Name"></input>
									          <input type="email" name="email" class="ck_email_address" id="ck_emailField" placeholder="Email Address" required></input>
											  <input type="hidden" name="RESURL" value="<?php echo get_the_ID(); ?>" />
											  <input type="hidden" name="CKTAGID" value="<?php echo get_post_meta(get_the_ID(),'CKTAGID',true); ?>" />
									      </div>

									      <button class="subscribe_button ck_subscribe_button btn fields" id='ck_subscribe_button'>
									        Access Now
									      </button>
									 </form>
								<?php
								} else if(has_term( 'course', 'type' ) and !isset($_GET['start'])) {
									the_excerpt();
									?>
									<a class="btn btn-blue" rel="nofollow" onclick="analytics.track('Download Resource', { Resource: '<?php echo $post->post_title; ?>' });" href="<?php echo get_post_meta($post->ID, 'resource_url', true); ?>?start=true">Start the Course</a>
									<?php
								} else if(has_term( 'course', 'type' ) and isset($_GET['start'])) {
									the_content();
								    $general_options = get_option('_wp_convertkit_settings');
								    $api_key         = $general_options && array_key_exists("api_key", $general_options) ? $general_options['api_key'] : null;
								    $CKapi       = new ConvertKitAPI($api_key);
									$tag_info = $CKapi->tag_subscribe(
									  	        get_post_meta($post->ID, 'CKTAGID', true),
									  	        array(
									  	          'email' => $_COOKIE["tgemail"],
												  'fname' => ''
									  	        )
									  	      );
									
								} else {
									the_content();
								    $general_options = get_option('_wp_convertkit_settings');
								    $api_key         = $general_options && array_key_exists("api_key", $general_options) ? $general_options['api_key'] : null;
								    $CKapi       = new ConvertKitAPI($api_key);
									$tag_info = $CKapi->tag_subscribe(
									  	        get_post_meta($post->ID, 'CKTAGID', true),
									  	        array(
									  	          'email' => $_COOKIE["tgemail"],
												  'fname' => ''
									  	        )
									  	      );
									if (get_post_meta($post->ID, 'resource_url', true)) : ?>
										<p>
											<a class="btn btn-blue" rel="nofollow" onclick="analytics.track('Download Resource', { Resource: '<?php echo $post->post_title; ?>' });" href="<?php echo get_post_meta($post->ID, 'resource_url', true); ?>">Download Resource</a>
										</p>
									<?php endif;
								}
							?>
							<hr />
							
						</div><!--END details-->
					</div><!--END span8-->
				<?php $c++; endwhile; ?>
			<?php endif; ?>
		</div>
	</div>
</article>
<?php get_footer(); ?>