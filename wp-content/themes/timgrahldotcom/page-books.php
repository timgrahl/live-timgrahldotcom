<?php
/**
 * Template Name: My Books
 *
 */

get_header(); ?>

	<article class="books">
		<div class="wrapper">
			<h2>My Books</h2>
			<div class="book">
				<div class="bimage">
					<img src="<?php bloginfo('template_directory'); ?>/img/book-yf1c.png" width="250" height="375" alt="Book Yf1c">
				</div>
				<div class="bdesc">
					<h1>Your First 1000 Copies</h1>
					<h3>The Step-by-Step Guide to Marketing Your Book</h3>
					<p>How would it feel to know that every time you started a new book project, you already had people excited to buy it and ready to recommend it to others?</p>
					<p>In Your First 1000 Copies, seasoned book marketing expert Tim Grahl walks you through how successful authors are using the online marketing tools to build their platform, connect with readers and sell more books.</p>
					<ul>
						<li><a class="btn amazonlink btn-grey" href="http://www.amazon.com/gp/product/B00DMIWAIC/ref=as_li_tl?ie=UTF8&camp=1789&creative=390957&creativeASIN=B00DMIWAIC&linkCode=as2&tag=outthink-20&linkId=AE3AT5QMNEERP6KD">Amazon</a></li>
						<li><a class="btn audiblelink btn-grey" href="http://www.audible.com/pd/Business/Your-First-1000-Copies-Audiobook/B00G3EHIAM/ref=a_search_c4_1_1_srTtl?qid=1424197848&sr=1-1">Audible</a></li>
						<li><a class="btn btn-grey" href="http://first1000copies.com">Read More</a></li>
					</ul>
				</div>
			</div> 
			<div class="book">
				<div class="bimage">
					<img src="<?php bloginfo('template_directory'); ?>/img/book-blb.png" width="250" height="411" alt="Book BLB">
				</div>
				<div class="bdesc">
					<h1>Book Launch Blueprint</h1>
					<h3>The Step-by-Step Guide to a Bestselling Launch</h3>
					<p>The hardest part of writing a book isn't writing it, it's launching it.</p>
					<p>Nothing is worse than pouring your soul into a manuscript only to have no one read it.</p>
					<p>In <em>Book Launch Blueprint</em>, you'll learn the steps to launching your next book.</p>
					<ul>
						<li><a class="btn amazonlink btn-grey" href="http://amzn.to/2gescQm">Amazon</a></li>
					</ul>
				</div>
			</div>
		</div>
	</article>
<?php get_footer(); ?>
