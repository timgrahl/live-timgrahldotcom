<?php
/**
 * Template Name: Sales Page B Productive Writer
 */
$thesiteurl = get_site_url();
?>

<!DOCTYPE html>
<html lang="en-US"
 xmlns:fb="http://ogp.me/ns/fb#">
    <head>
        <meta charset="UTF-8" />
        <title>Productive Writer</title>
        <link rel="profile" href="http://gmpg.org/xfn/11" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="google-site-verification" content="inL2BzG2ZTKEYL75v4rHe0jAuxS6Mu4LifWV-HjpS7M" />
		<!-- FAVICON STUFF -->
		<link rel="apple-touch-icon" sizes="57x57" href="<?php echo $thesiteurl; ?>/wp-content/themes/timgrahldotcom/img/favicon/apple-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="<?php echo $thesiteurl; ?>/wp-content/themes/timgrahldotcom/img/favicon/apple-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="<?php echo $thesiteurl; ?>/wp-content/themes/timgrahldotcom/img/favicon/apple-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="<?php echo $thesiteurl; ?>/wp-content/themes/timgrahldotcom/img/favicon/apple-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="<?php echo $thesiteurl; ?>/wp-content/themes/timgrahldotcom/img/favicon/apple-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="<?php echo $thesiteurl; ?>/wp-content/themes/timgrahldotcom/img/favicon/apple-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="<?php echo $thesiteurl; ?>/wp-content/themes/timgrahldotcom/img/favicon/apple-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="<?php echo $thesiteurl; ?>/wp-content/themes/timgrahldotcom/img/favicon/apple-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="<?php echo $thesiteurl; ?>/wp-content/themes/timgrahldotcom/img/favicon/apple-icon-180x180.png">
		<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo $thesiteurl; ?>/wp-content/themes/timgrahldotcom/img/favicon/android-icon-192x192.png">
		<link rel="icon" type="image/png" sizes="32x32" href="<?php echo $thesiteurl; ?>/wp-content/themes/timgrahldotcom/img/favicon/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="96x96" href="<?php echo $thesiteurl; ?>/wp-content/themes/timgrahldotcom/img/favicon/favicon-96x96.png">
		<link rel="icon" type="image/png" sizes="16x16" href="<?php echo $thesiteurl; ?>/wp-content/themes/timgrahldotcom/img/favicon/favicon-16x16.png">
		<link rel="manifest" href="<?php echo $thesiteurl; ?>/wp-content/themes/timgrahldotcom/img/favicon/manifest.json">
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="<?php echo $thesiteurl; ?>/wp-content/themes/timgrahldotcom/img/favicon/ms-icon-144x144.png">
		<meta name="theme-color" content="#ffffff">
		<meta name="google-site-verification" content="GkYNHGMrRq8AH9s1SyoP2D_aeKwUE3EHptfMAr4nPqA" />
		<!-- END FAVICON STUFF -->
		
        <link rel="stylesheet" href="<?php echo $thesiteurl; ?>/wp-content/themes/timgrahldotcom/style.css?v=1" type="text/css" media="screen" />
		<link rel="stylesheet" href="<?php echo $thesiteurl; ?>/wp-content/themes/timgrahldotcom/print.css" type="text/css" media="print" />
        <link rel="pingback" href="<?php echo $thesiteurl; ?>/xmlrpc.php" />
		<script type="text/javascript">
		!function(){var analytics=window.analytics=window.analytics||[];if(analytics.invoked)window.console&&console.error&&console.error("Segment snippet included twice.");else{analytics.invoked=!0;analytics.methods=["trackSubmit","trackClick","trackLink","trackForm","pageview","identify","group","track","ready","alias","page","once","off","on"];analytics.factory=function(t){return function(){var e=Array.prototype.slice.call(arguments);e.unshift(t);analytics.push(e);return analytics}};for(var t=0;t<analytics.methods.length;t++){var e=analytics.methods[t];analytics[e]=analytics.factory(e)}analytics.load=function(t){var e=document.createElement("script");e.type="text/javascript";e.async=!0;e.src=("https:"===document.location.protocol?"https://":"http://")+"cdn.segment.com/analytics.js/v1/"+t+"/analytics.min.js";var n=document.getElementsByTagName("script")[0];n.parentNode.insertBefore(e,n)};analytics.SNIPPET_VERSION="3.0.0";
		window.analytics.load("SjbpZGMQ3N1bl7jgOpRxsjl5etkdZNGn");
		window.analytics.page();
		}}();
		analytics.track("Viewed Post", {"title":"Productive Writer Sales Page"});
		</script>
				<script>
		(function(d) {
		  var tkTimeout=3000;
		  if(window.sessionStorage){if(sessionStorage.getItem('useTypekit')==='false'){tkTimeout=0;}}
		  var config = {
		    kitId: 'yul1bwz',
		    scriptTimeout: tkTimeout
		  },
		  h=d.documentElement,t=setTimeout(function(){h.className=h.className.replace(/\bwf-loading\b/g,"")+"wf-inactive";if(window.sessionStorage){sessionStorage.setItem("useTypekit","false")}},config.scriptTimeout),tk=d.createElement("script"),f=false,s=d.getElementsByTagName("script")[0],a;h.className+="wf-loading";tk.src='//use.typekit.net/'+config.kitId+'.js';tk.async=true;tk.onload=tk.onreadystatechange=function(){a=this.readyState;if(f||a&&a!="complete"&&a!="loaded")return;f=true;clearTimeout(t);try{Typekit.load(config)}catch(e){}};s.parentNode.insertBefore(tk,s)
		})(document);
		</script>
        <script src="//load.sumome.com/" data-sumo-site-id="3ca5e11df1e04c01a48166aa86989d0330521927aa3d3625ee74d2c7132361d9" async="async"></script>
<!-- All in One SEO Pack 2.2.6.2 by Michael Torbert of Semper Fi Web Design[121,156] -->
<link rel="author" href="http://google.com/+TimGrahl23" />

<link rel="canonical" href="<?php echo $thesiteurl; ?>/productive-writer/" />
<!-- /all in one seo pack -->
<link rel="alternate" type="application/rss+xml" title="Tim Grahl &raquo; Launch a Bestseller Comments Feed" href="<?php echo $thesiteurl; ?>/programs/launch-a-bestseller/feed/" />
		
<script type='text/javascript' src='<?php echo $thesiteurl; ?>/wp-includes/js/jquery/jquery.js?ver=1.11.2'></script>
<script type='text/javascript' src='<?php echo $thesiteurl; ?>/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.2.1'></script>

<meta property="fb:app_id" content="130357656259"/>	
		<script src="<?php echo $thesiteurl; ?>/wp-content/themes/timgrahldotcom/js/modernizr.custom.76227.js" type="text/javascript" charset="utf-8"></script>
		<script src="<?php echo $thesiteurl; ?>/wp-content/themes/timgrahldotcom/js/jquery.cookie.js" type="text/javascript" charset="utf-8"></script>
		<script src="<?php echo $thesiteurl; ?>/wp-content/themes/timgrahldotcom/js/min/includes-min.js" type="text/javascript" charset="utf-8"></script>
    </head>
		<div class="fixedhead">
			Registration closes in... &nbsp;&nbsp;<span class="showbutton" style="display:none;"><span class="topbuy">Join Now</span></span><br /><span class="puttime"></span>
		</div>
		<article id="post-<?php the_ID(); ?>" class="salespage pw-sales">
			<div class="topper">
				<div class="tcontent">
					<iframe src="https://player.vimeo.com/video/179224920?color=000000&title=0&byline=0&portrait=0" width="666" height="375" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
					<br />
					<h1>Find the Time...<br />Overcome Your Fear...<br />and Finish Your Book</h1>
					<h2>You’ve started and stopped, and started again, dozens of times.</h2>
				
				</div>
				<div style="clear:left">&nbsp;</div>
			</div>
			<div class="wrapper">
				<div class="entry">
					
					<p>You’ve tried, and tried, and tried.</p>
					
					<p>And each time, you tell yourself, “This time, I’m going to stick with it.”</p>

					<p>Or, “Next week, first thing Monday morning, I’m definitely going to do it.”</p>

					<p>And yet, it happens again...</p>

					<p>You don't get your writing done.</p>

					<p>It’s so crazy.</p>

					<p>You want to be a writer.</p>

					<p>You want to finish that book.</p>

					<p>You want to reach your dreams.</p>

					<p>But for some reason, you just can’t muster the willpower to actually sit down and type.</p>

					<p><strong>Imagine What It Would Be Like If You Could Actually Get Your Writing Done.</strong></p>

					<p>You’d be doing what you love.</p>

					<p>Your words would be helping, encouraging, or entertaining readers.</p>

					<p>You’d be interacting with readers who love your work.</p>

					<p>You’d have Amazon automatically putting money in your bank account every month.</p>

					<p>But then another week goes by...</p>

					<p>No words written...</p>

					<p>We get it — life is crazy!</p>

					<p>You’re working a full-time job, have kids, a demanding boss, your mom is sick, and you’re the only sibling who can take care of her.</p>

					<p>How can you possibly get everything done?</p>
					
					<h3>Meet Your Guides</h3>
						
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sales/pw/hiimjeff.png" width="228" height="228" alt="Hiimtim" class="Hiimtim">
					<p>Hi, I'm <strong>Jeff Goins</strong>. I am the best-selling author of four books including the recent national best seller, The Art of Work. I also run a popular writing blog called Goins, Writer, teach online courses, and host the annual Tribe Conference. Every year, my words reach millions of readers.</p>
					
					<p>I successfully wrote for years. Then, everything changed - I stopped writing.</p>

					<p>I wanted to be a writer. But I wasn't writing. Not consistently. And not well.</p>

					<p>So I began to study the habits of famous writers, I realized something:</p>

					<p>Success did not create the habits. The habits created the success.</p>

					<p>Successful writers don't wait for luck before they start acting like a professional. It's quite the opposite. And as I began to observe my writing heroes I saw another trend:</p>

					<p>Great writers have systems.</p>

					<p>They use daily disciplines to get he writing done. So that's what I did.</p>

					<p>Every morning I got up to write. Every day I captured ideas. And every day, I shared my writing with someone.</p>

					<p>I developed a system that worked for me. And you know what? I stopped feeling stuck. I stopped not writing. I wrote more in a year than I had the previous decade. What's more, now I had an audience. And then came the books and all that would follow. All because I created a system.</p>
					
					<hr />

					<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sales/lab/hiimtim.png" width="228" height="228" alt="Hiimtim" class="Hiimtim">
					<p>My name is <strong>Tim Grahl</strong>. I founded Out:think marketing group, and I’m the author of the bestseller Your First 1000 Copies: The Step-by-Step Guide to Marketing Your Book.</p>
					
					<p>I’ve worked with and trained thousands of authors on effective book marketing, and worked with some of the best-selling authors in the world, such as Hugh Howey, Dan Pink, Chip and Dan Heath, Barbara Corcoran, and many more.</p>

					<p>But back up ten years, and you’d find a broke version of me sitting in a church closet, procrastinating doing my work by playing World of Warcraft on my computer all day.</p>

					<p>I desperately needed to get my work done so I could pay my bills. I dreamed of running a business that both helped writers and made good money. And I dreamed of writing my own book.</p>

					<p>And every day, I would come into the office promising myself I was going to get a ton done that day, and then … Eight hours would pass with no forward progress.</p>

					<p>Finally, in a state of desperation, I reached out to a business coach and asked him to help me.</p>

					<p>I started paying thousands of dollars I didn’t have (hello, credit card!) to help me pull out of this sucking vortex of unproductivity. He started teaching me about systems and habits, how the brain worked, and how I could increase motivation.</p>

					<p>I also had the good fortune to work one-on–one with more than a hundred different writers, and to learn about their work methods, writing habits, and more.</p>

					<p>Here’s what I learned:</p>

					<p>It’s possible to set up your life so that it’s easier to succeed than to fail.</p>

					<p>Every successful writer has a system. They have learned to find the time to write.</p>

					<p>They have developed the mindset and the habits to sit down and get the writing done.</p>
					
					<p><strong>We would like to share the system we discovered:</strong></p>
					
					
					<h3>Introducing Productive Writer...</h3>
					
					<iframe src="https://player.vimeo.com/video/179224918?color=000000&title=0&byline=0&portrait=0" width="666" height="375" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

					<p>Productive Writer is a simple and proven system to help you get your writing done and out into the world.</p>
					
					<p>Productive Writer is the only productivity program built specifically to help you get your writing done and finish your book. It includes the knowledge and expertise gathered from years of working with and training thousands of authors.</p>

					<h3>How Productive Writer System Works:</h3>

					<p><strong>FIRST: Overcome Fear and Think Like a Writer.</strong></p>
					<p>After years of working alongside some of the most successful writers of our day, we've learned the mindsets they bring to the blank page.</p>

					<p>We’ll walk you through the mental barriers that hold you back from success, and teach you how to break them down and destroy them for good.</p>

					<p><strong>SECOND: Find The Time to Write</strong></p>
					<p>What if you automatically sat down every day and got your writing done?</p>

					<p>Learn how to break the bad habits and build new ones in your life.</p>

					<p>Based on scientific research, our experience and other writer's, we’ll show you how you can build up <strong>new habits to get your writing done</strong>.</p>

					<p>We will also help you create a personalized <strong>writing schedule</strong> that guarantees you will get your writing done.</p>

					<p><strong>THIRD: Finish Your Book</strong></p>
					<p>Learn <strong>Jeff's Three Bucket System</strong> to go from blank page to finished book.</p>
					<ul>
						<li>Bucket 1 – Find ideas</li>
						<li>Bucket 2 – Take ideas to a rough draft</li>
						<li>Bucket 3 – Turn your rough draft into a publishable piece. </li>
					</ul>

					<p>Discover Jeff’s 5 Draft Method to prepare your writing to share with the world. Write something you will be proud of.</p>

					<p>This is writing advice you’ve never heard before.</p>

					<p>When you join Productive Writer, you’ll learn the exact mindsets, habits, scheduling, and writing techniques we use to get writers to change their life and write their book.</p>
					
					<p style="text-align:center;"><a href="#" class="btn-blue topbuy">Join Productive Writer Now</a></p>
					
					<p>&nbsp;</p>

					<h3>A peek inside Productive Writer</h3>
					
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sales/pw/productshot.png" width="561" height="329" alt="Productshot" class="noborder aligncenter">
					<br /><br />

					<p>Inside Productive Writer, you’ll find 30 video lessons, downloadable worksheets and checklists, and receive daily emails—clean, quick and simple, and all available in mobile-friendly, easy-to-use format.</p>
					
					<p>Productive Writer is a six-week course that will walk you through your entire system in bite-size pieces (about 15 minutes a day).</p>

					<p>Every weekday for six weeks, you’ll receive a new email and video lesson that includes action steps you can take today to make progress.</p>

					<p>Here is a few example of what you’ll find inside:</p>
					<ul>
						<li><strong>The Three Bucket System.</strong> Jeff’s exact writing system he used to write five books.</li>
						<li><strong>The 5 Draft System.</strong> How to write a book you can be proud of.</li>
						<li><strong>Take Control of Your Schedule.</strong> Exactly how you take back control of your schedule and ensure that you make time for writing.</li>
						<li><strong>The Micro Habit Method.</strong> Science backed methods for creating new writing habits and making it automatic to start getting your work done.</li>
						<li><strong>Beat Decision Fatigue.</strong> A method Tim learned from President Obama about how systems can save you.</li>
						<li><strong>Speed Up Your Writing.</strong> Learn how to get your words down faster, and stop wasting time staring at a blinking cursor.</li>
						<li><strong>Change Your Mindset.</strong> A simple habit that will change the way you see yourself and grow your confidence as a writer.</li>
						<li><strong>Make Your Writing an Automatic Habit.</strong> The proven way to create the habit of writing consistently—and make creativity an automatic part of your life.</li>
						<li><strong>The Three Biggest Enemies of Productivity.</strong> How to identify them, and how to beat them.</li>
						<li><strong>The Magic Wand Technique.</strong> The method for creating a schedule and a plan for reaching your writing goals.</li>
						<li><strong>The Idea Handbook.</strong> The one tool we use to ensure that we never lose track of what we should be writing, and that we always have things to write about.</li>
					</ul>

					<h3>What Makes Productive Writer different?</h3>
					
					<p>There are thousands of blog posts, books, podcasts, and more out there on helping your write your book.</p>

					<p>So what makes this course any different?</p>

					<p>There are three fundamental things that set Productive Writer apart from everything else:</p>

					<p><strong>1. It’s designed purely for writers.</strong></p>

					<p>Most productivity systems are written for business people with a 9 to 5 job. Or they focus on how to work in teams. Or they’re aimed at all creative people.</p>

					<p>We created Productive Writer specifically for writers.</p>

					<p>The advice is based on our system and strategies, along with everything we’ve learned working hundreds of writers over the years.</p>

					<p>This course is only for writers, because helping writers is what we do.</p>

					<p><strong>2. It’s simple.</strong></p>

					<p>We’ll walk you through the system step-by-step in bite-size pieces (about 15 minutes a day). </p>

					<p><strong>3. It's a lifetime writing plan.</strong></p>

					<p>You will not only complete your book. You'll create habits and systems that will help you keep writing book after book.</p>

					<p>Productive Writer will change your entire approach to writing and make it a constant part of the rest of your life.</p>
					
					
					<div class="testimonials" style="padding: 25px;background: #ffffeb;border: 1px solid #f0f0f0;">
						<h3 style="margin-top:.4em;">What Students Say:</h3>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sales/pw/deborah.png" width="100" height="100" alt="Deborah" style="float:right;margin:0 0 50px 10px;">
						<p>"My writing habits have changed. They continue to change. At times I experience my writing life goes dark as life, family and health challenges narrowed my energy and time. I'm so grateful that you made this possible for writers who want to be more productive!" </p>
					
						<p>- Deborah Taylor-French</p>

						<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sales/pw/linda.png" width="100" height="100" alt="Linda" style="float:right;margin:0 0 50px 10px;">
						<p>"Thank you for your words in this course. 'Perfection' does mean things to people. It stops us even before we get started. For the passed 3 mornings I have gotten up and sat down at my desk and started writing again for the first time in a long time. Thank you, Tim, for offering this course!! It is making a difference to me." </p>

						<p>- Linda Freeto</p>
					</div>
					
					<p>&nbsp;</p>
					
					<div class="bonuses">
						
						<h3 style="margin-top:.4em;">Free Bonuses Included</h3>
						
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sales/pw/screens.png" width="750" height="192" alt="Screens" class="noborder">

						<p>Along with the video lessons and daily emails, you’ll get access to the worksheets, spreadsheets, and PDFs we’ve created to help authors reach their writing and productivity goals.</p>
						
						
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sales/pw/icon-video-map.png" width="150" height="111" alt="Icon Video Map" class="noborder alignright">
						<p><strong>90 Day Book Roadmap</strong></p>
						<p>Exactly what you should be doing each step of the way to finish your book in 90 days.</p>
						<p>Includes Productive Writer coursework along with guides on how to get your writing done, even with a busy schedule.</p>
						
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sales/pw/icon-manu2pub.png" width="200" height="205" alt="Icon Manu2pub" class="noborder alignright">
						<p><strong>Manuscript to Published Checklist</strong><p>
						<p>Once your manuscript is done, now what?</p>
						<p>This checklist will take you through the entire process of turning your manuscript into a published book, both ebook and print.</p>
						<p>Everything you need from start to finish, including suggested resources for book conversion, layout, design, and other necessities.</p>

						<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sales/pw/icon-video-friend.png" width="150" height="111" alt="Icon Video Map" class="noborder alignright">
						<p><strong>The Writing Accountability System that <em>Works</em></strong></p>
						<p>It's easy to get excited and partner with another writer on keeping each other accountable to the daily word count.</p>
						<p>Unfortunately, far to often, these agreements don't actually work in the long run.</p>
						<p>We'll teach you the common pitfalls, how to avoid them, and the 3 steps towards an accountability system that works.</p>
						
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sales/pw/icon-video-toolbox.png" width="150" height="111" alt="Icon Video Map" class="noborder alignright">
						<p><strong>Productive Writer Toolkit</strong></p>
						<p>Every tool, device, and app we use to get our writing done.</p>
						<p>Take out the guesswork on what you need to be productive. We provide the exact tools you need and where to get them.</p>
						
						<p><strong>Just added...</strong></p>
						
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sales/pw/yaaw.png" width="150" height="225" alt="Yaaw" class="noborder alignright">
						<p><strong>You Are a Writer (So Start Acting Like One)</strong></p>
						<p>In You Are a Writer, Jeff Goins shares his own story of self-doubt and what it took for him to become a professional writer. He gives you practical steps to improve your writing, get published in magazines, and build a platform that puts you in charge.</p>

						<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sales/pw/writersstudio.png" width="150" height="150" alt="Writersstudio" class="noborder alignright">
						<p><strong>The Writer's Studio Program</strong></p>
						<p>Four unique audio sessions, each in a dynamic Q&amp;A format (it’s like we’re sitting across the table from each other) covering:</p>
						<ul>
							<li>You Need to Become a Writer — We discuss the overlooked importance to becoming confident in your craft: believing you are something before you can act like it.</li>
							<li>You Need the Right Tools — Forget theory; it's time to practice. Here, we talk about how you can use online tools to reach your audience. We'll discuss some specific strategies like how to launch a website and start blogging.</li>
							<li>You Need Relationships — It’s not just what you know but who you know that really matters. Here, we discuss how to make the right connections and build influence that leads to amazing opportunities.</li>
							<li>You Need to Get Started — In this final session, we talk about the actual craft of writing and how you can land a publishing contract in less time than you think.</li>
						</ul>
						
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sales/pw/9hacks.png" width="150" height="150" alt="9hacks" class="noborder alignright">
						<p><strong>9 Writing Hacks by Top Authors</strong></p>
						<p>The quirks, tips and tricks successful authors have used to get their writing done and finish their books.</p>
						
					</div>

					<h3>What’s next for you?</h3>

					<p>Are you going to finish your book?</p>
					
					<p>Now is the time to make the decision about what’s next for you as a writer.</p>

					<p>You can keep on doing what you’ve been doing, and keep getting the same results. You can keep pushing your dream back day after day.</p>

					<p>Or you can learn from our experience working with 100-plus writers, and being obsessed with productivity — and save yourself years of frustration and spinning your wheels.</p>

					<p>To reach your goals of finishing your book, you have to make a change.</p>

					<p>This is what you’ve been looking for: a simple and proven system to help you get your writing done and out into the world.</p>
					
					<div class="gaurantee">
						<h3>Our Guarantee</h3>

						<p>You have our 60-day, full-course access, 100% money-back guarantee.</p>
						
						<div style="text-align:center;"><p><img class="noborder" style="display:inline;" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sales/lab/icon-guarantee.png" width="128" height="128" alt="Icon Guarantee"></p></div>
						
						<p>The short version: If you don’t love Productive Writer, then we want to give you your money back.</p>

						<p>The reason we can give such a strong guarantee is because we know Productive Writer really works.</p>

						<p>This is the system we’ve developed that will finally help you break out of your rut and start moving forward.</p>

						<p>But if you go through Productive Writer and are unhappy with it within the first 60 days, simply send us an email and we’ll immediately refund you your money.</p>

						<p>The only thing we’ll ask of you is some feedback on why you’re asking for the refund, so we can make the program even better and more applicable to more authors.</p>
						
					</div>
					<br /><br />
					<?php get_template_part( 'page-sales-prodwrite-tiers-b' ); ?>

					<h3>Frequently Asked Questions</h3>
					
					<iframe src="https://player.vimeo.com/video/179224919?color=000000&title=0&byline=0&portrait=0" width="666" height="375" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

					<p><strong>Q. How is the program delivered?</strong> </p>
					<p><strong>A.</strong> Every weekday for six weeks, you will receive an email with access to the next lesson’s video. Along with the video are daily next steps, so you can put the content into action.</p>
					
					<p><strong>Q. What if I can’t commit to the next six weeks?</strong> </p>
					<p><strong>A.</strong> The content will be rolled out and made available to you over the next six weeks. But once you receive it, you will have lifetime access to it.</p>
					
					<p><strong>Q: Can I be successful with this program?</strong></p>
					<p><strong>A:</strong> First, we have to define the word success.</p>

					<p>If you’re hoping to a find a silver bullet that requires very little work on your part and still guarantees you'll become a prolific writer, then this program isn't for you.</p>

					<p>However, if you are looking for a system that has a proven track record of helping writers get more done, then Productive Writer is a perfect fit!</p>
					
					<p><strong>Q: How long do I have access to the course?</strong></p>
					<p><strong>A:</strong> You have unlimited, lifetime access to this version of the course. You can retake the course as many times as you'd like. All the material will be continuously updated.</p>
					
					<p><strong>Q: How much time does the course take?</strong></p>
					<p><strong>A:</strong> Each lesson is designed to be taken in less than 20 minutes. However, each day's application may take longer.</p>
					
					<p style="text-align:center;"><a href="#" class="btn-blue topbuy">Join Productive Writer Now</a></p>
				</div>
			</div>
		</article>


		<section class="footer">
			<div class="wrapper">
				<div>
					<div class="sitemap">
						&nbsp;
					</div>
					<div class="training">
						&nbsp;
					</div>
					<div class="totop">
						<h4 class="scrollToTop">Back to Top</h4>
						<p>© Common Insights - Tim Grahl</p>
						<img src="<?php bloginfo('template_directory'); ?>/img/icon-backtotop.png" class="scrollToTop" width="46" height="46" alt="Icon Backtotop">
					</div>
				</div>
			</div>		
		</section>
		<script src="<?php bloginfo('template_directory'); ?>/js/jquery.fitvids.js" type="text/javascript" charset="utf-8"></script>
		<script src="<?php bloginfo('template_directory'); ?>/js/jquery.countdown.min.js" type="text/javascript" charset="utf-8"></script>
		<script type="text/javascript">
		jQuery(".puttime").countdown('2016/08/27 01:59:59', function(event) {
			jQuery(this).text(event.strftime('%-D day%!D, %-H hour%!H, %-M minute%!M, and %-S second%!S'));
		});
		jQuery(document).scroll(function() {
		  var y = jQuery(this).scrollTop();
		  if (y > 4000) {
		    jQuery('.showbutton').fadeIn();
		  } else {
		    jQuery('.showbutton').fadeOut();
		  }
		});

		jQuery(".topbuy").click(function(e) { 
			jQuery('html, body').animate({scrollTop : jQuery("#toptier").offset().top-120},800);
		});
		jQuery(document).ready(function(){
		// Target your #container, #wrapper etc.
		    jQuery(".salespage").fitVids();
		});
		</script>
		


			<script type="text/javascript">
			jQuery(document).ready(function(){
				//Click event to scroll to top
				jQuery('.scrollToTop').click(function(){
					jQuery('html, body').animate({scrollTop : 0},800);
					return false;
				});
				jQuery('.salespage').css("margin-top",jQuery(".fixedhead").outerHeight());
			});
			</script>
		</body>
		</html>
