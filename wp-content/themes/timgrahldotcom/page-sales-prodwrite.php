<?php
/**
 * Template Name: Sales Page Productive Writer
 */
$thesiteurl = get_site_url();
?>

<!DOCTYPE html>
<html lang="en-US"
 xmlns:fb="http://ogp.me/ns/fb#">
    <head>
        <meta charset="UTF-8" />
        <title>Productive Writer</title>
        <link rel="profile" href="http://gmpg.org/xfn/11" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="google-site-verification" content="inL2BzG2ZTKEYL75v4rHe0jAuxS6Mu4LifWV-HjpS7M" />
		<!-- FAVICON STUFF -->
		<link rel="apple-touch-icon" sizes="57x57" href="<?php echo $thesiteurl; ?>/wp-content/themes/timgrahldotcom/img/favicon/apple-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="<?php echo $thesiteurl; ?>/wp-content/themes/timgrahldotcom/img/favicon/apple-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="<?php echo $thesiteurl; ?>/wp-content/themes/timgrahldotcom/img/favicon/apple-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="<?php echo $thesiteurl; ?>/wp-content/themes/timgrahldotcom/img/favicon/apple-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="<?php echo $thesiteurl; ?>/wp-content/themes/timgrahldotcom/img/favicon/apple-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="<?php echo $thesiteurl; ?>/wp-content/themes/timgrahldotcom/img/favicon/apple-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="<?php echo $thesiteurl; ?>/wp-content/themes/timgrahldotcom/img/favicon/apple-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="<?php echo $thesiteurl; ?>/wp-content/themes/timgrahldotcom/img/favicon/apple-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="<?php echo $thesiteurl; ?>/wp-content/themes/timgrahldotcom/img/favicon/apple-icon-180x180.png">
		<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo $thesiteurl; ?>/wp-content/themes/timgrahldotcom/img/favicon/android-icon-192x192.png">
		<link rel="icon" type="image/png" sizes="32x32" href="<?php echo $thesiteurl; ?>/wp-content/themes/timgrahldotcom/img/favicon/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="96x96" href="<?php echo $thesiteurl; ?>/wp-content/themes/timgrahldotcom/img/favicon/favicon-96x96.png">
		<link rel="icon" type="image/png" sizes="16x16" href="<?php echo $thesiteurl; ?>/wp-content/themes/timgrahldotcom/img/favicon/favicon-16x16.png">
		<link rel="manifest" href="<?php echo $thesiteurl; ?>/wp-content/themes/timgrahldotcom/img/favicon/manifest.json">
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="<?php echo $thesiteurl; ?>/wp-content/themes/timgrahldotcom/img/favicon/ms-icon-144x144.png">
		<meta name="theme-color" content="#ffffff">
		<meta name="google-site-verification" content="GkYNHGMrRq8AH9s1SyoP2D_aeKwUE3EHptfMAr4nPqA" />
		<!-- END FAVICON STUFF -->
		
        <link rel="stylesheet" href="<?php echo $thesiteurl; ?>/wp-content/themes/timgrahldotcom/style.css?v=1" type="text/css" media="screen" />
		<link rel="stylesheet" href="<?php echo $thesiteurl; ?>/wp-content/themes/timgrahldotcom/print.css" type="text/css" media="print" />
        <link rel="pingback" href="<?php echo $thesiteurl; ?>/xmlrpc.php" />
		<script type="text/javascript">
		!function(){var analytics=window.analytics=window.analytics||[];if(analytics.invoked)window.console&&console.error&&console.error("Segment snippet included twice.");else{analytics.invoked=!0;analytics.methods=["trackSubmit","trackClick","trackLink","trackForm","pageview","identify","group","track","ready","alias","page","once","off","on"];analytics.factory=function(t){return function(){var e=Array.prototype.slice.call(arguments);e.unshift(t);analytics.push(e);return analytics}};for(var t=0;t<analytics.methods.length;t++){var e=analytics.methods[t];analytics[e]=analytics.factory(e)}analytics.load=function(t){var e=document.createElement("script");e.type="text/javascript";e.async=!0;e.src=("https:"===document.location.protocol?"https://":"http://")+"cdn.segment.com/analytics.js/v1/"+t+"/analytics.min.js";var n=document.getElementsByTagName("script")[0];n.parentNode.insertBefore(e,n)};analytics.SNIPPET_VERSION="3.0.0";
		window.analytics.load("SjbpZGMQ3N1bl7jgOpRxsjl5etkdZNGn");
		window.analytics.page();
		}}();
		analytics.track("Viewed Post", {"title":"Productive Writer Sales Page"});
		</script>
				<script>
		(function(d) {
		  var tkTimeout=3000;
		  if(window.sessionStorage){if(sessionStorage.getItem('useTypekit')==='false'){tkTimeout=0;}}
		  var config = {
		    kitId: 'yul1bwz',
		    scriptTimeout: tkTimeout
		  },
		  h=d.documentElement,t=setTimeout(function(){h.className=h.className.replace(/\bwf-loading\b/g,"")+"wf-inactive";if(window.sessionStorage){sessionStorage.setItem("useTypekit","false")}},config.scriptTimeout),tk=d.createElement("script"),f=false,s=d.getElementsByTagName("script")[0],a;h.className+="wf-loading";tk.src='//use.typekit.net/'+config.kitId+'.js';tk.async=true;tk.onload=tk.onreadystatechange=function(){a=this.readyState;if(f||a&&a!="complete"&&a!="loaded")return;f=true;clearTimeout(t);try{Typekit.load(config)}catch(e){}};s.parentNode.insertBefore(tk,s)
		})(document);
		</script>
        <script src="//load.sumome.com/" data-sumo-site-id="3ca5e11df1e04c01a48166aa86989d0330521927aa3d3625ee74d2c7132361d9" async="async"></script>
<!-- All in One SEO Pack 2.2.6.2 by Michael Torbert of Semper Fi Web Design[121,156] -->
<link rel="author" href="http://google.com/+TimGrahl23" />

<link rel="canonical" href="<?php echo $thesiteurl; ?>/productive-writer/" />
<!-- /all in one seo pack -->
<link rel="alternate" type="application/rss+xml" title="Tim Grahl &raquo; Launch a Bestseller Comments Feed" href="<?php echo $thesiteurl; ?>/programs/launch-a-bestseller/feed/" />
		
<script type='text/javascript' src='<?php echo $thesiteurl; ?>/wp-includes/js/jquery/jquery.js?ver=1.11.2'></script>
<script type='text/javascript' src='<?php echo $thesiteurl; ?>/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.2.1'></script>

<meta property="fb:app_id" content="130357656259"/>	
		<script src="<?php echo $thesiteurl; ?>/wp-content/themes/timgrahldotcom/js/modernizr.custom.76227.js" type="text/javascript" charset="utf-8"></script>
		<script src="<?php echo $thesiteurl; ?>/wp-content/themes/timgrahldotcom/js/jquery.cookie.js" type="text/javascript" charset="utf-8"></script>
		<script src="<?php echo $thesiteurl; ?>/wp-content/themes/timgrahldotcom/js/min/includes-min.js" type="text/javascript" charset="utf-8"></script>
    </head>
		<div class="fixedhead">
			Registration closes in... &nbsp;&nbsp;<span class="showbutton" style="display:none;"><span class="topbuy">Buy Now</span></span><br /><span class="puttime"></span>
		</div>
		<article id="post-<?php the_ID(); ?>" class="salespage pw-sales">
			<div class="topper">
				<div class="tcontent">
					<iframe src="https://player.vimeo.com/video/179224920?color=000000&title=0&byline=0&portrait=0" width="666" height="375" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
					<br />
					<h1>Find the Time...<br />Overcome Your Fear...<br />and Finish Your Book...<br />in the Next 90 Days</h1>
					<h2>You’ve started and stopped, and started again, dozens of times.</h2>
				
				</div>
				<div style="clear:left">&nbsp;</div>
			</div>
			<div class="wrapper">
				<div class="entry">
					<p>You’ve tried, and tried, and tried.</p>
					<p>And each time, you tell yourself, “This time, I’m going to stick with it.”</p>
					<p>Or, “Next week, first thing Monday morning, I’m definitely going to do it.”</p>
					<p>And yet, it happens again...</p>

					<h3>You don't get your writing done.</h3>

					<p>It’s so crazy.</p>
					<p>You want to be a writer.</p>
					<p>You want to finish that book.</p>
					<p>You want that freedom.</p>
					<p>You want to reach your dreams.</p>
					<p>But for some reason, you just can’t muster the willpower to actually sit down and type.</p>
					<p>Maybe you can't figure out how to start.</p>
					<p>Maybe you’re 80% done with your book, but can’t hammer out that last 20%.</p>
					<p>Maybe you’ve finished your manuscript, but it’s been sitting on your hard drive for six months, waiting for you to do your first-pass edit.</p>
					<p><strong>You desperately want to finish your book, but something keeps stopping you.</strong></p>
					<p>At night, you lie awake and dream of what it would be like to actually be a writer.</p>
					<p>You’d get so many books finished.</p>
					<p>You’d be doing what you love.</p>
					<p>You’d be interacting with readers who love your work.</p>
					<p>You’d have Amazon automatically putting money in your bank account every month.</p>
					<p>But then another week goes by...</p>
					<p>No words written...</p>
					<p>Your book no closer to being finished.</p>
					<p>Your dream is just as far away as it was last week, last month, last year.</p>

					<h3>We get it — life is <em>crazy</em>!</h3>

					<p>You’re working a full-time job.</p>
					<p>You’ve got kids.</p>
					<p>You’ve got a demanding boss.</p>
					<p>Your mom is sick, and you’re the only sibling who can take care of her.</p>
					<p>How can you possibly get everything done?</p>
					<p>It definitely seems impossible, when you’re busy from the time you get up until the time you finally plop down on the couch exhausted at the end of the day.</p>
					<p>And each day seems so small.</p>
					<p>So you didn’t write today. Big deal, right? You can start tomorrow.</p>
					<p>But those days stack up to become months, then years.</p>
					<p><strong>And here’s the worst thing — way worse than simply not writing today, or not publishing your book this year.</strong></p>
					<p>By far the worst thing is:</p>
					
					<h3>Dying with your music still in you</h3>
					
					<blockquote>Don’t die with your music still inside you. Listen to your intuitive inner voice and find what passion stirs your soul. Listen to that inner voice, and don’t get to the end of your life and say, “What if my whole life has been wrong?”<br/>
						– Dr. Wayne Dyer, bestselling author</blockquote>
					
					<!--
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sales/pw/icon-game.png" width="128" height="128" alt="Icon Game" class="noborder pushleft" style="margin-left:-150px;">	
					<h4>Play this game with me</h4>
					<br />
					<p>Picture the 85-year-old version of yourself.</p>
					<p>Now picture the current you going to to visit the 85-year-old you.</p>
					<p>Explain to the 85-year-old you what you've done in the last year. What you've spent your time doing. How far you've gotten towards your goals and dreams. What you've let hold you back.</p>
					<p>Here's the question... what would happen? Would the</p>
					<ol>
						<li>85-year-old you be proud of you, or</li>
						<li>85-year-old you  pull out their cane and beat you senseless for being so wasteful with the time you've been given?
					</ol>
					<p><em>I think about this all of the time...</em></p>
					<p>And, I think about how most writers face this dilemma, and I ask:</p>
					-->
					<p><strong>What if all of those days of not writing add up to a life of never fulfilling your calling to be a writer?</strong></p>
					<!--
					<p>Or a life where you never at least tried to see if you could do it.</p>
					<p>And then, what about all those people you told you were going to be a writer?</p>
					<p>What will they think of you?</p>
					<p>Will you prove right the ones who thought you could never do it?</p>
					<p>What will you tell your mom when she asks again how your book is going?</p>

					<h3>But there’s just so much to do!</h3>
					
					<p>Let’s say your manuscript is done. </p>
					<p>The list of things to do now is so extraordinarily long.</p>
					<p>Book proposals, websites, formatting, agents, social media, book cover …</p>
					<p>The list seems endless.</p>
					<p><strong>How are you possibly going to find the time to do all this stuff?</strong></p>
					<p>No wonder we’re not writing! Even if we finish the book, how are we ever going to find time to get all the other stuff done?</p>
					<p>So what can you do?</p>
					
						
					<h3>I was in that very place a few years ago.</h3>
					-->
						
					<p>We understand how hard it is to find the time, overcome your fear, and finish your book.</p>
						
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sales/pw/hiimjeff.png" width="228" height="228" alt="Hiimtim" class="Hiimtim">
					<p>Hi, I'm Jeff Goins. I am the best-selling author of four books including the recent national best seller, The Art of Work. I also run a popular writing blog called Goins, Writer, teach online courses, and host the annual Tribe Conference. Every year, my words reach millions of readers. </p>
					
										<p>I've been writing my whole life but only recently did I become a writer. For the longest time, I didn't understand what it took to be a professional. I waited for my big break, and it never came.</p>
					
										<p>Then, everything changed. </p>
					
										<p>In 18 months, I built a popular blog with hundreds of thousands of readers, published my first two books, and quit a full-time job to write for a living—because I was now making triple our household income. My wife quit her job to live out her dream of being a mom and since then I've been doing the unbelievable: writing for a living. </p>
					
										<p>I still can't believe it. This was what I had dreamed of doing for years, and in 18 months it had become  a reality. </p>
					
										<p>But here's the thing: none of it happened the way I thought it would. </p>
					
										<p>I thought you had to wait for inspiration. </p>
					
										<p>I thought that you had to "just write" and the words would come to you. </p>
					
										<p>I thought that if you were going to succeed, you had to get lucky. </p>
					
										<p>Turns out, that's not really how it works. Becoming a writer is about taking small but important steps every day. And when I was wishing of being a writer for all those years, that's exactly what I wasn't doing.</p>
					
										<p>I wanted to be a writer. But I wasn't writing. Not consistently. And not well. </p>
					
										<p>But when I began to study the habits of famous writers, I realized something: </p>
					
										<p>Success did not create the habits. The habits created the success. </p>
					
										<p>Successful writers don't wait for luck before they start acting like a professional. It's quite the opposite. And as I began to observe my writing heroes I saw another trend:</p>
					
										<p>Great writers have systems.</p>
					
										<p>They use daily disciplines to get he writing done. </p>
					
										<p>So that's what I did. </p>
					
										<p>Every morning I got up to write. Every day I captured ideas. And every day, I shared my writing with someone. </p>
					
										<p>I developed a system that worked for me. And you know what? I stopped feeling stuck. I stopped not writing. I wrote more in a year than I had the previous decade. What's more, now I had an audience. And then came the books and all that would follow. All because I created a system. </p>
					
										<p>Funny story: once I found all that success, ego got the best of me and I stopped writing so much. Then, I stopped altogether. And slowly but surely everything I had built began to decline. So I went back to my system, to writing every day. And it worked. Sometimes the habits that get you here are the same ones that sustain you.</p>
										
										<hr />

					<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sales/lab/hiimtim.png" width="228" height="228" alt="Hiimtim" class="Hiimtim">
					<p>My name is Tim Grahl. I founded Out:think marketing group, and I’m the author of the bestseller <em>Your First 1000 Copies: The Step-by-Step Guide to Marketing Your Book</em>. </p>
					<p>I’ve worked with and trained thousands of authors on effective book marketing, and worked with some of the best-selling authors in the world, such as Hugh Howey, Dan Pink, Chip and Dan Heath, Barbara Corcoran, and many more.</p>
					<p>But back up ten years, and you’d find a broke version of me sitting in a church closet, procrastinating doing my work by playing World of Warcraft on my computer all day.</p>
					<p>I desperately needed to get my work done so I could pay my bills. I dreamed of running a business that both helped writers and made good money. And I dreamed of writing my own book.</p>
					<p>And every day, I would come into the office promising myself I was going to get a ton done that day, and then …</p>
					<p>Eight hours would pass with no forward progress.</p>
					<p>Weeks would pass where I would do just enough to keep the bills paid, but nothing to actually reach my dreams.</p>
					<p>And I was always teetering on the edge of disaster.</p>
					<p>And disaster meant a lot of bad things, including:</p>
					<ul>
						<li>Losing my house</li>
						<li>My wife having to go back to work</li>
						<li>Me having to go back to a “real” job</li>
						<li>Being embarrassed beyond belief</li>
					</ul>
					<p>And yet, I still procrastinated. I still put off work. I still put off going after my dreams.</p>
					
					<h3>I was a desperate case if there ever was one.</h3>
					
					<p>Finally, in a state of desperation, I reached out to a business coach and asked him to help me.</p>
					<p>I started paying thousands of dollars I didn’t have (hello, credit card!) to help me pull out of this sucking vortex of unproductivity.</p>
					<p>He started teaching me about systems and habits, how the brain worked, and how I could increase motivation.</p>
					<p>Then I got obsessed …</p>
					<p><strong>I began to use my everyday life as a lab, to experiment on how I could increase my productivity. </strong></p>
					<p>I started learning what causes procrastination, and how I could trick myself into getting more done.</p>
					<p>I started figuring out how to cut the crap out of my life so I could focus on actually getting useful things done.</p>
					<p>I also had the good fortune to work one-on–one with more than a hundred different writers, and to learn about their work methods, writing habits, and more.</p>
					
					<h3>Fast-forward to today...</h3>
					
					<p>Today I work less than 20 hours a week, yet get more done than most people do in a 40-plus-hour work week.</p>
					<p>I also:</p>
					<ul>
						<li>Volunteer daily in a local homeless ministry</li>
						<li>Spend every evening with my family</li>
						<li>Work out every day</li>
						<li>And even have leisure time left over to read, watch TV, and do my other hobbies</li>
					</ul>
					<p>I don’t say all of this to brag.</p>
					<p>Far from it.</p>
					<p>If left to my own devices, I would still be that guy from ten years ago, wasting away every hour, day, week, month, year of my life barely scraping by.</p>
					<p><strong>But here’s what we learned:</strong></p>
					
					<h3>It’s possible to set up your life so that it’s easier to succeed than to fail.</h3>
					<p>Every successful writer has a system.</p>
					<p>They have learned to find the time to write.</p>
					<p>They have developed the mindset and the habits to sit down and get the writing done.</p>
					<p>They might now share the exact same system, but they share the same principles.</p>
					<p>We would like to share them with you.</p>
					<!--
					<p>I don’t get a ton done because I’m a workaholic, type-A nutjob.</p>
					<p>Far from it.</p>
					<p><strong>I get a lot done because I’ve learned three very important lessons:</strong></p>
					
					<h3>Very Important Lesson #1: Mindset Matters Most</h3>
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sales/pw/icon-brain.png" width="128" height="128" alt="Icon Bag" class="alignright noborder">
					<p>I don’t mean the stuff you read all the time, like: “Envision your dreams!” or “Decide you’re a winner!”</p>
					<p>In fact, one of the pieces of advice I give to most writers is, “Assume you’re going to be a lazy, procrastinating loser, and then work from there.”</p>
					<p>That’s what I still do for myself.</p>
					<p>And it’s one of the biggest secrets to my success.</p>
					
					<h3>Very Important Lesson #2: Willpower Won’t Work</h3>
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sales/pw/icon-hamer.png" width="128" height="128" alt="Icon Hammer" class="alignright noborder">					
					<p>Almost all productivity plans require “willpower,” and that is a huge mistake.</p>
					<p>I define willpower as “the ability to make myself do something I don’t want to do.”</p>
					<p>Sure, you can download the latest, greatest to-do app. You can get an accountability partner.</p>
					<p>But none of it is going to work long-term if you’re really hoping that tomorrow or next week or whatever, you’re going to feel like doing it, or you’ll finally have the willpower to pull it off.</p>
					<p>That is not a long-term strategy that will work.</p>
					<p>If you want to actually complete your writing projects and all of the other things you want to do as a writer, you’ve got to be able to get stuff done even when you really, really don’t feel like it.</p>
					<p><strong>Willpower doesn’t work, so stop counting on it.</strong></p>
					
					<h3>Very Important Lesson #3: You Already Have Everything You Need</h3>
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sales/pw/icon-bag.png" width="128" height="128" alt="Icon Bag" class="alignright noborder">
					
					<p>I’m still the same person I was ten years ago.</p>
					<p>I didn’t magically replace my personality with someone new.</p>
					<p>And when I look back ten years to the super-unproductive me, I realize that there was nothing wrong with me. I already had everything I needed to succeed.</p>
					<p>I had all of the tools.</p>
					<p><strong>I just had no idea how to use them.</strong></p>
					
					<hr />

					<p>Over the last eight years, I’ve trained thousands of writers to do online marketing.</p>
					<p>I’ve coached hundreds of authors one-on-one.</p>
					<p>I’ve spoken in front of large groups of writers.</p>
					<p>I’ve trained tens of thousands of writers through my book, courses, and online content.</p>
					<p>But here’s what I keep seeing over and over:</p>
					<p>I can teach you exactly what to do.</p>
					<p>I can make it simple, clear, and straightforward.</p>
					<p>But if you don’t ever finish your book, all of it will fail.</p>
					<p><strong>If you can’t figure out how to actually make yourself work toward your dreams, then it doesn’t matter what you know.</strong></p>
					-->
					
					
					<h3>Introducing...</h3>
					
					<iframe src="https://player.vimeo.com/video/179224918?color=000000&title=0&byline=0&portrait=0" width="666" height="375" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

					<p><strong>Productive Writer</strong> is a proven system that will help you find the time, overcome your fear, and finish your book in the next 90 days.</p>

					<p><strong>Productive Writer</strong> is the only productivity program built specifically to help you finish your book. It includes the knowledge and expertise gathered from years of working with and training thousands of authors.</p>
					<p>When you join <strong>Productive Writer</strong>, you’ll access a 6-week program that will ensure that you finally finish writing your book.</p>
					<p>You’ll learn the exact mindsets, habits, scheduling, and writing techniques that we use to get writers to change their life and write their book.</p>

					<h3>What Makes Productive Writer different?</h3>

					<p>There are thousands of blog posts, books, podcasts, and more out there on helping your write your book. </p>
					<p>So what makes this course any different?</p>

					<p><strong>There are two fundamental things that set Productive Writer apart from everything else:</strong></p>
					
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sales/lab/icon-check.png" width="48" height="48" alt="Icon Check" class="pushleft noborder">
					<p><strong>1. It’s designed purely for writers.</strong></p>
					<p>Most productivity systems are written for business people with a 9 to 5 job. Or they focus on how to work in teams. Or they’re aimed at all creative people.</p>
					<p><strong>We created Productive Writer specifically for writers.</strong> </p>
					<p>The advice is based on our system and strategies, along with everything we’ve learned working hundreds of writers over the years.</p>
					<p>This course is only for writers, because helping writers is what we do.</p>

					<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sales/lab/icon-check.png" width="48" height="48" alt="Icon Check" class="pushleft noborder">
					<p><strong>2. It’s simple.</strong></p>
					<p>If it takes  200-plus pages to explain it, then it’s too complicated.</p>
					<p>If we have to have weekly and bi-weekly check-ins, inboxes/outboxes, color-coded folders, and/or complicated apps, then we're just not going to do it.</p>
					<p>We need simple tools and methods that do just enough to: a) free up some of our time and b) make sure we get the writing done.</p>
					<p><strong>Productive Writer</strong> is a <em>simple</em> system designed to help you finish your book.</p>

					<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sales/lab/icon-check.png" width="48" height="48" alt="Icon Check" class="pushleft noborder">
					<p><strong>3. It is a step-by-step plan.</strong></p>
					<p>We give you everything you need to ensure you get your book finished.</p>
					<p>This is not generic "write 1000 words every day" advice. We created a systematic plan that will help you stop procrastinating, get your writing done, and finish your book.</p>
					<p><strong>Productive Writer</strong> will provide the exact system you can use to finish your book in the next 90 days.</p>

					<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sales/lab/icon-check.png" width="48" height="48" alt="Icon Check" class="pushleft noborder">
					<p><strong>4. It's a lifetime writing plan.</strong></p>
					<p>You will not only complete your book in the next 90 days.</p>
					<p>You'll create habits and systems that will help you keep writing book after book.</p>
					<p><strong>Productive Writer</strong> will change your entire approach to writing and make it a constant part of the rest of your life.</p>

					<h3>This is a customizable, step-by-step plan for developing a productive writing system that will work for your unique situation.</h3>
					
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sales/pw/steps.png" width="850" height="170" alt="Steps" class="noborder aligncenter">

					<p>You have a unique set of circumstances that you need to change or overcome in order to reach your goals.</p>
					<p>Whether you have a full-time job or are already making a living from writing, Productive Writer will help you develop a plan that works for you.</p>

					<h3>A peek inside Productive Writer</h3>
					
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sales/pw/productshot.png" width="561" height="329" alt="Productshot" class="noborder aligncenter">
					<br /><br />

					<p>Inside <strong>Productive Writer</strong> you’ll find high-definition video lessons, downloadable worksheets and checklists, and receive daily emails—clean, quick and simple, and all available in mobile-friendly, easy-to-use format.</p>
					<p><strong>Productive Writer</strong> is a six-week course that will walk you through your entire productivity plan.</p>
					<p>Every weekday for six weeks, you’ll receive a new email and video lesson that includes action steps you can take today to make progress.</p>

					<h3>How it works</h3>

					<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sales/lab/icon-check.png" width="48" height="48" alt="Icon Check" class="pushleft noborder">
					<p><strong>FIRST: Think Like a Writer</strong></p>
					<p>Most of the writers we work with have a mindset that constantly hinders their writing success.</p>
					<p>After years of working alongside some of the most successful writers of our day, we've learned the mindsets they bring to the blank page.</p>
					<p>We’ll walk you through the mental barriers that hold you back from success, and teach you how to break them down and destroy them for good.</p>

					<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sales/lab/icon-check.png" width="48" height="48" alt="Icon Check" class="pushleft noborder">
					<p><strong>SECOND: Make Your Writing a Habit</strong></p>
					<p>What if you automatically sat down every day and got your writing done?</p>
					<p>Learn how to break the bad habits and build new ones in your life.</p>
					<p>Based on scientific research, our experience and other writer's, we’ll show you how you can build up new habits to get things done.</p>

					<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sales/lab/icon-check.png" width="48" height="48" alt="Icon Check" class="pushleft noborder">
					<p><strong>THIRD: Create an Ironclad Writing Schedule</strong></p>
					<p>Successful writers create a writing schedule that guarantees they get time to write.</p>
					<p>However, this is much harder than it sounds with all of the pressures of life.</p>
					<p>I’ll walk you through how to create a schedule that works with your life and your writing style. This will ensure that your writing gets done and that you move forward on your projects.</p>

					<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sales/lab/icon-check.png" width="48" height="48" alt="Icon Check" class="pushleft noborder">
					<p><strong>FOURTH: Get the Most Out of Your Writing</strong></p>
					<p>Merely stringing words together is not enough.</p>
					<p>Learn Jeff's Three Bucket System to go from blank page to finished book.</p>
					<p>Learn the tools and strategies to write efficiently, so that you get the most out of your creative time.</p>
					<p>This is writing advice you’ve never heard before.</p>

					<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sales/lab/icon-check.png" width="48" height="48" alt="Icon Check" class="pushleft noborder">
					<p><strong>FIFTH: Create Your Personal Book Roadmap</strong></p>
					<p>Tie it all together to create your custom <strong>Productive Writer</strong> Book Roadmap.</p>
					<p>We’ll walk you through exactly how to create a system that will ensure you finish your book in the next 90 days. This will make achieving your goals a normal part of your life.</p>

					<h3>Productive Writer offers 6 weeks of training, 30 daily emails, and 30+ video lessons with huge benefits — and huge results:</h3>
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sales/pw/5screens.png" width="610" height="203" alt="5screens" class="noborder">
					<br /><br />
					<ul>
						<li><strong>The 5 Draft System</strong>. How to write a book you can be proud of.
						<li><strong>Take Control of Your Schedule</strong>. Exactly how you take back control of your schedule and ensure that you make time for writing.</li>
						<li><strong>The Micro Habit Method</strong>. Science backed methods for creating new writing habits and making it automatic to start getting your work done.</li>
						<li><strong>Beat Decision Fatigue</strong>. A method Tim learned from President Obama about how systems can save you.
						<li><strong>Speed Up Your Writing</strong>. Learn how to get your words down faster, and stop wasting time staring at a blinking cursor.</li>
						<li><strong>Change Your Mindset</strong>. A simple habit that will change the way you see yourself and grow your confidence as a writer.</li>
						<li><strong>Make Your Writing an Automatic Habit</strong>. The proven way to create the habit of writing consistently—and make creativity an automatic part of your life.</li>
						<li><strong>The Three Biggest Enemies of Productivity</strong>. How to identify them, and how to beat them.</li>
						<li><strong>The Magic Wand Technique</strong>. The method for creating a schedule and a plan for reaching your writing goals.</li>
						<li><strong>The Idea Handbook</strong>. The one tool we use to ensure that we never lose track of what we should be writing, and that we always have things to write about.</li>
						<li><strong>The Three Bucket System</strong>. Jeff's exact writing system he used to write five books.</li>
					</ul>
					<div class="bonuses">
						
						<h3 style="margin-top:.4em;">Free Bonuses Included</h3>
						
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sales/pw/screens.png" width="750" height="192" alt="Screens" class="noborder">

						<p>Along with the video lessons and daily emails, you’ll get access to the worksheets, spreadsheets, and PDFs we’ve created to help authors reach their writing and productivity goals.</p>
						
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sales/pw/icon-manu2pub.png" width="200" height="205" alt="Icon Manu2pub" class="noborder alignright">
						<p><strong>Manuscript to Published Checklist</strong><p>
						<p>Once your manuscript is done, now what?</p>
						<p>This checklist will take you through the entire process of turning your manuscript into a published book, both ebook and print.</p>
						<p>Everything you need from start to finish, including suggested resources for book conversion, layout, design, and other necessities.</p>
						
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sales/pw/icon-productivityist-workbook.png" width="200" height="131" alt="Icon Manu2pub" class="noborder alignright">
						<p><strong>The Productivityist Workbook</strong><br />by Mike Vardy, founder of Productivityist</p>
						<p>The Productivityist Workbook is a great starting point for those new to the world of personal productivity and want to spend more time being productive rather than simply doing productive. It is divided into four sections that devote time to specific areas that can impact your productivity: Time Management, Task Management, Email Management, and Idea Management.</p>
						
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sales/pw/icon-nightowlproductivity.png" width="200" height="205" alt="Icon Nightowlproductivity" class="noborder alignright">
						<p><strong>The Night Owl Action Plan</strong><br />by Mike Vardy, founder of Productivityist</p>
						<p>The Night Owl Action Plan offers the tactics, tools, and tips you need to level up your late night productivity so that you can keep up with those early birds. He even offers practical approaches on how to deal with the various circumstances that may have “wired you” as a night owl in the first place, whether it be the schedule of your day job, a new addition to the family, or a long-standing pattern of late nights and not-so-early mornings.</p>
						
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sales/pw/icon-video-map.png" width="150" height="111" alt="Icon Video Map" class="noborder alignright">
						<p><strong>90 Day Book Roadmap</strong></p>
						<p>Exactly what you should be doing each step of the way to finish your book in 90 days.</p>
						<p>Includes Productive Writer coursework along with guides on how to get your writing done, even with a busy schedule.</p>
						
						<!--
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sales/pw/icon-video-clock.png" width="150" height="111" alt="Icon Video Map" class="noborder alignright">
						<p><strong>The <em>Get Up Early</em> System</strong></p>
						<p>We are <em>not</em> a morning people. Everything in me wants to stay up late and roll out of bed at the last possible minute.</p>
						<p>However, over the last 5 years I've developed a system that ensures I get out of bed early enough to get my writing done before most people wake up.</p>
						<p>I'll show you:</p>
						<ul>
							<li>My 4 hour training system that will change the way you wake up.</li>
							<li>How the first 15 minutes of every day can be optimized towards finishing your book.</li>
							<li>The 1 thing that gets me out of bed even when I'm exhausted.</li>
							<li>The best alarm clocks that force you out of bed.</li>
						</ul>
						-->
							
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sales/pw/icon-video-friend.png" width="150" height="111" alt="Icon Video Map" class="noborder alignright">
						<p><strong>The Writing Accountability System that <em>Works</em></strong></p>
						<p>It's easy to get excited and partner with another writer on keeping each other accountable to the daily word count.</p>
						<p>Unfortunately, far to often, these agreements don't actually work in the long run.</p>
						<p>We'll teach you the common pitfalls, how to avoid them, and the 3 steps towards an accountability system that works.</p>
						
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sales/pw/icon-video-beat.png" width="150" height="111" alt="Icon Video Map" class="noborder alignright">
						<p><strong>Top 8 Ways to Beat Distraction</strong></p>
						<p>Whether it's Facebook, family, email, or fear, distractions crop up and keep us from writing.</p>
						<p>We share the top eight ways you can beat distractions before they start.</p>
						<p>Use these to keep your writing time focused on finishing your book.</p>
						
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sales/pw/icon-video-toolbox.png" width="150" height="111" alt="Icon Video Map" class="noborder alignright">
						<p><strong>Productive Writer Toolkit</strong></p>
						<p>Every tool, device, and app we use to get our writing done.</p>
						<p>Take out the guesswork on what you need to be productive. We provide the exact tools you need and where to get them.</p>
						
					</div>

					<h3>What’s next for you?</h3>

					<p><strong>Are you going to finish your book?</strong></p>
					<p>Now is the time to make the decision about what’s next for you as a writer.</p>
					<p>You can keep on doing what you’ve been doing, and keep getting the same results. You can keep pushing your dream back day after day.</p>
					<p><strong>Or you can learn from our experience working with 100-plus writers, and being obsessed with productivity — and save yourself years of frustration and spinning your wheels.</strong></p>
					<p>To reach your goals of finishing your book, you have to make a change.</p>
					<p>We spent thousands of dollars and years of work to get the level of productivity training we're offering you with this program. </p>
					<p>This is what you’ve been looking for: proven methods used by bestselling authors that will have you getting your writing done and out into the world, on a steady basis.</p>
					
					<div class="gaurantee">
						<h3>When you join Productive Writer, you get our personal guarantee.</h3>

						<p><strong>You have our 30-day, full-course access, 100% money-back guarantee.</strong></p>
					
						<div style="text-align:center;"><p><img class="noborder" style="display:inline;" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sales/lab/icon-guarantee.png" width="128" height="128" alt="Icon Guarantee"></p></div>
					
						<p><strong>The short version</strong>: If you don’t love <strong>Productive Writer</strong>, then we want to give you your money back.</p>
						<p>The reason we can give such a strong guarantee is because we know Productive Writer really works.</p>
						<p>This is the system we’ve developed that will finally help you break out of your rut and start moving forward.</p>
						<p>But if you go through <strong>Productive Writer</strong> and are unhappy with it within the first 30 days, simply send us an email and we’ll immediately refund you your money.</p>
						<p>The only thing we’ll ask of you is some feedback on why you’re asking for the refund, so we can make the program even better and more applicable to more authors.</p>
					</div>
					<br /><br />
					<?php get_template_part( 'page-sales-prodwrite-tiers' ); ?>

					<h3>Frequently Asked Questions</h3>
					
					<iframe src="https://player.vimeo.com/video/179224919?color=000000&title=0&byline=0&portrait=0" width="666" height="375" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

					<p><strong>Q. How is the program delivered?</strong> </p>
					<p><strong>A.</strong> Every weekday for six weeks, you will receive an email with access to the next lesson’s video. Along with the video are daily next steps, so you can put the content into action.</p>
					
					<p><strong>Q. What if I can’t commit to the next six weeks?</strong> </p>
					<p><strong>A.</strong> The content will be rolled out and made available to you over the next six weeks. But once you receive it, you will have lifetime access to it.</p>
					
					<p><strong>Q: Can I be successful with this program?</strong></p>
					<p><strong>A:</strong> First, we have to define the word success.</p>

					<p>If you’re hoping to a find a silver bullet that requires very little work on your part and still guarantees you'll become a prolific writer, then this program isn't for you.</p>

					<p>However, if you are looking for a system that has a proven track record of helping writers get more done, then Productive Writer is a perfect fit!</p>
					
					<p><strong>Q: How long do I have access to the course?</strong></p>
					<p><strong>A:</strong> You have unlimited, lifetime access to this version of the course. You can retake the course as many times as you'd like. All the material will be continuously updated.</p>
					
					<p><strong>Q: How much time does the course take?</strong></p>
					<p><strong>A:</strong> Each lesson is designed to be taken in less than 20 minutes. However, each day's application may take longer.</p>
					
					<?php get_template_part( 'page-sales-prodwrite-tiers' ); ?>
				</div>
			</div>
		</article>


		<section class="footer">
			<div class="wrapper">
				<div>
					<div class="sitemap">
						&nbsp;
					</div>
					<div class="training">
						&nbsp;
					</div>
					<div class="totop">
						<h4 class="scrollToTop">Back to Top</h4>
						<p>© Common Insights - Tim Grahl</p>
						<img src="<?php bloginfo('template_directory'); ?>/img/icon-backtotop.png" class="scrollToTop" width="46" height="46" alt="Icon Backtotop">
					</div>
				</div>
			</div>		
		</section>
		<script src="<?php bloginfo('template_directory'); ?>/js/jquery.fitvids.js" type="text/javascript" charset="utf-8"></script>
		<script src="<?php bloginfo('template_directory'); ?>/js/jquery.countdown.min.js" type="text/javascript" charset="utf-8"></script>
		<script type="text/javascript">
		jQuery(".puttime").countdown('2016/08/26 01:59:59', function(event) {
			jQuery(this).text(event.strftime('%-D day%!D, %-H hour%!H, %-M minute%!M, and %-S second%!S'));
		});
		jQuery(document).scroll(function() {
		  var y = jQuery(this).scrollTop();
		  if (y > 4000) {
		    jQuery('.showbutton').fadeIn();
		  } else {
		    jQuery('.showbutton').fadeOut();
		  }
		});

		jQuery(".topbuy").click(function(e) { 
			jQuery('html, body').animate({scrollTop : jQuery("#toptier").offset().top-120},800);
		});
		jQuery(document).ready(function(){
		// Target your #container, #wrapper etc.
		    jQuery(".salespage").fitVids();
		});
		</script>
		


			<script type="text/javascript">
			jQuery(document).ready(function(){
				//Click event to scroll to top
				jQuery('.scrollToTop').click(function(){
					jQuery('html, body').animate({scrollTop : 0},800);
					return false;
				});
				jQuery('.salespage').css("margin-top",jQuery(".fixedhead").outerHeight());
			});
			</script>
		</body>
		</html>
