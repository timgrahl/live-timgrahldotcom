<?php
/*
* Template Name: Articles Page
*/
?>

<?php get_header(); ?>
		<?php while ( have_posts() ) : the_post(); ?>

			<article class="articlepage">
				<div class="wrapper">
					<?php the_title( sprintf( '<h1 class="entry-title">', esc_url( get_permalink() ) ), '</h1>' ); ?>

					<div class="entry">
						<div class="newsletterbox">
							<h2>Latest Articles</h2>
							<p>Every week I send out new articles with the latest book marketing tactics and strategies that bestselling authors are using.</p>
							<form id="ck_subscribe_form" class="ck_subscribe_form" action="https://app.convertkit.com/landing_pages/9910/subscribe" data-remote="true">
								<input type="hidden" value="{&quot;form_style&quot;:&quot;naked&quot;,&quot;embed_style&quot;:&quot;inline&quot;,&quot;embed_trigger&quot;:&quot;scroll_percentage&quot;,&quot;scroll_percentage&quot;:&quot;70&quot;,&quot;delay_seconds&quot;:&quot;10&quot;,&quot;display_position&quot;:&quot;br&quot;,&quot;display_devices&quot;:&quot;all&quot;,&quot;days_no_show&quot;:&quot;15&quot;,&quot;converted_behavior&quot;:&quot;show&quot;}" id="ck_form_options"></input>
								<input type="hidden" name="id" value="9910" id="landing_page_id"></input>
					        	<input type="text" name="first_name" class="ck_first_name" id="ck_firstNameField" placeholder="First Name"></input>
								<input type="email" name="email" class="ck_email_address" id="ck_emailField" placeholder="Email Address" required></input>
								<input type="hidden" name="SOURCE" value="articles page" />
								<button type="submit">Sign Me Up!</button>
							</form>
						</div>
						
						<h2>Most Popular Articles</h2>
						<?php
						$pop_posts = new WP_Query(array(
							'category_name' => 'popular',
							'posts_per_page'	=> '-1',
							));
						if ($pop_posts->have_posts()) : 
							?>
							<ol>
							<?php
							while ($pop_posts->have_posts()) : $pop_posts-> the_post();
							?>
								<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
							<?php
							endwhile;
							?>
							</ol>
							<?php
						endif;
						?>
						
						<h2>Full Archive</h2>
						<?php foreach(posts_by_year() as $year => $posts) : ?>
						  <h2><?php echo $year; ?></h2>

						  <ul>
						    <?php foreach($posts as $post) : setup_postdata($post); ?>
						      <li>
						        <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
						      </li>
						    <?php endforeach; ?>
						  </ul>
						<?php endforeach; ?>
					</div>
				</div>
			</article>

		<?php endwhile; // end of the loop. ?>
<?php get_footer(); ?>
