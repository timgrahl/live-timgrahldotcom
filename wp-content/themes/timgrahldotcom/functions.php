<?php
include("mailchimp/mailchimp.php");

add_theme_support( 'post-thumbnails', array( 'post', 'page', 'resources' ) );

// Replaces the excerpt "more" text by a link
function new_excerpt_more($more) {
       global $post;
	return ' [...]<br /><br /><span class="readmore"><a class="moretag" href="'. get_permalink($post->ID) . '">Read More &#x279e;</a></span>';
}
add_filter('excerpt_more', 'new_excerpt_more');

function posts_by_year() {
  // array to use for results
  $years = array();

  // get posts from WP
  $posts = get_posts(array(
    'numberposts' => -1,
    'orderby' => 'post_date',
    'order' => 'DESC',
    'post_type' => 'post',
    'post_status' => 'publish',
	'date_query' => array(
			array(
				'after'     => 'January 1st, 2010',
				'inclusive' => true,
			),
		),
  ));

  // loop through posts, populating $years arrays
  foreach($posts as $post) {
    $years[date('Y', strtotime($post->post_date))][] = $post;
  }

  // reverse sort by year
  krsort($years);

  return $years;
}

//

function timgrahldotcom_notice_admin_bar_render() {
	global $wp_admin_bar;
	if (get_option('siteurl') == 'http://timgrahl.com') {
		$wp_admin_bar->add_menu( array(
			'parent' => false, // use 'false' for a root menu, or pass the ID of the parent menu
			'id' => 'timgrahldotcom-alert', // link ID, defaults to a sanitized title value
			'title' => __("<span style='color:red;'>YOU ARE ON TIMGRAHL.COM! NO CHANGES!!!</span>"), // link title
			'href' => admin_url( 'options-reading.php' ), // name of file
		));
	}
}
add_action( 'wp_before_admin_bar_render', 'timgrahldotcom_notice_admin_bar_render' ); 


// Add Shortcode
function tgmodal_shortcode( $atts ) {
	global $post;
	// Attributes
	$atts = shortcode_atts(
		array(
			'button' => 'Click Here to Subscribe',
			'ctaa' => 'Free 30 Day Course',
			'ctab' => 'Sign up below to receive a free 30 day course to connect with readers and sell more books.'
		), $atts );

	// Code
	ob_start();
	?>
	<div class="modal">
	  <label for="modal-1">
		  <div class="modal-trigger btn"><?php echo $atts['button']; ?></div>
	  </label>
	  <input class="modal-state" id="modal-1" type="checkbox" />
		<div class="modal-window">
			<div class="modal-inner">
			    <label class="modal-close" for="modal-1"></label>
			    <h2><?php echo $atts['ctaa']; ?></h2>
			    <p><?php echo $atts['ctab']; ?></p>
				<form action="/thanks" method="get" class="regform" novalidate>
					<input class="ename" name="FNAME" type="text" placeholder="first name" />
					<input class="emailaddy" name="EMAIL" type="text" placeholder="your email address here" />
					<button type="submit" class="resourcereg">Get Started</button>
					<input type="hidden" name="SOURCE" value="<?php echo get_the_title(); ?>">
				</form>
			</div>
		</div>
	</div>
	<?php
	return ob_get_clean();
}
add_shortcode( 'tgmodal', 'tgmodal_shortcode' );


add_filter( 'wp_default_scripts', 'dequeue_jquery_migrate' );

function dequeue_jquery_migrate( &$scripts){
	if(!is_admin()){
		$scripts->remove( 'jquery');
		$scripts->add( 'jquery', false, array( 'jquery-core' ), '1.10.2' );
	}
}

function disable_wp_emojicons() {

  // all actions related to emojis
  remove_action( 'admin_print_styles', 'print_emoji_styles' );
  remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
  remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
  remove_action( 'wp_print_styles', 'print_emoji_styles' );
  remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
  remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
  remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );

  // filter to remove TinyMCE emojis
  add_filter( 'tiny_mce_plugins', 'disable_emojicons_tinymce' );
}
add_action( 'init', 'disable_wp_emojicons' );

function disable_emojicons_tinymce( $plugins ) {
  if ( is_array( $plugins ) ) {
    return array_diff( $plugins, array( 'wpemoji' ) );
  } else {
    return array();
  }
}
?>