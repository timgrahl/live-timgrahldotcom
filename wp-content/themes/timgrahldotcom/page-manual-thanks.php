<?php
/**
 * Template Name: Post Optin Carrot Content
 *
 */
if(isset($_POST['id'])) {
    $general_options = get_option('_wp_convertkit_settings');
    $api_key         = $general_options && array_key_exists("api_key", $general_options) ? $general_options['api_key'] : null;
    $CKapi       = new ConvertKitAPI($api_key);
	$member_info = $CKapi->form_subscribe(
	        $_POST['id'],
	        array(
	          'email' => $_POST['email'],
	          'fname' => $_POST['first_name']
	        )
	      );

	$pageId = $_POST['prev_id'];

	setcookie('tgcookie', "1", time() + (86400 * 30), "/"); // 86400 = 1 day
	setcookie('tgemail', $_POST['email'], time() + ((86400 * 365) * 5), "/"); // 86400 = 1 day

}
get_header(); ?>
		<?php while ( have_posts() ) : the_post(); ?>

			<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.2/css/font-awesome.min.css">
			<style>
				.sumome-share-client-wrapper{ display: none !important; }
				.share .social{list-style: none; margin: 0;}
				.share .social li{
					float: left;
					width: 20%;
				}
				.share .social li a{
					position: relative;
					height: 48px;
					line-height: 36px;
					font-size: 28px;
					display: list-item;
					list-style-type: none;
					padding: 5px 0;
					text-align: center;
					color: #fff;
				}
				.share .social li a.facebook{background-color: #3b5998;}
				.share .social li a.twitter{background-color: #00aced;}
				.share .social li a.google-plus{background-color: #dd4b39;}
				.share .social li a.pinterest{background-color: #c92228;}
				.share .social li a.envelope{background-color: #726C6C;}

			</style>
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<div class="wrapper">
					<?php the_title( sprintf( '<h1 class="entry-title">', esc_url( get_permalink() ) ), '</h1>' ); ?>

					<div class="entry">

						<?php
						/* translators: %s: Name of current post */
						the_content( sprintf(
							__( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'timgrahldotcom' ),
							the_title( '<span class="screen-reader-text">"', '"</span>', false )
						) );
						?>

						<hr style="width: 80%; margin: 20px auto;" />

						<p>Know someone who might find this useful? Share the love...</p>

						<div class="share share-article clearfix">
							<ul class="social">
								<li><a class="facebook" target="_blank" href="https://twitter.com/intent/tweet?text=<?php urlencode(the_title($pageId)); ?>&url=<?= get_permalink($pageId); ?>"><i class="fa fa-twitter" title="Twitter"></i></a></li>
								<li><a class="twitter" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?= get_permalink($pageId); ?>"><i class="fa fa-facebook" title="Facebook"></i></a></li>
								<li><a class="google-plus" target="_blank" href="https://plus.google.com/share?text=<?php urlencode(the_title($pageId)); ?>&url=<?= get_permalink($pageId); ?>"><i class="fa fa-google-plus" title="Google Plus"></i></a></li>
								<li><a class="pinterest" target="_blank" href="https://pinterest.com/pin/create%2Fbutton/?&text=<?php urlencode(the_title($pageId)); ?>&url=<?= get_permalink($pageId); ?>"><i class="fa fa-pinterest" title="Pinterest"></i></a></li>
								<li><a class="envelope" target="_blank" href="mailto:user@domain.com?subject=<?= get_permalink($pageId); ?>"><i class="fa fa-envelope" title="Email"></i></a></li>
							</ul>
						</div>

						<hr style="width: 80%; margin: 60px auto;" />
						
						<div style="text-align: center"><a href="<?= wp_get_attachment_url( $_POST['file_id']) ?>" class="btn btn-blue">Download Your Bonus Material</a></div>

					</div>
				</div>
			</article>

		<?php endwhile; // end of the loop. ?>
<?php get_footer(); ?>
