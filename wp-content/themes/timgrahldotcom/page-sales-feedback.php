<?php
/**
 * Template Name: Sales Page Feedback
 */

get_header(); ?>
		<?php while ( have_posts() ) : the_post(); ?>

			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<div class="wrapper">
					<?php the_title( sprintf( '<h1 class="entry-title">', esc_url( get_permalink() ) ), '</h1>' ); ?>

					<div class="entry">
					<?php
						/* translators: %s: Name of current post */
						the_content( sprintf(
							__( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'timgrahldotcom' ),
							the_title( '<span class="screen-reader-text">"', '"</span>', false )
						) );
					?>
						<!-- CSS -->
						<link href="<?php bloginfo('template_directory'); ?>/css/wufoo/structure.css" rel="stylesheet">
						

						<!-- JavaScript -->
						<script src="<?php bloginfo('template_directory'); ?>/js/wufoo.js"></script>

						<!--[if lt IE 10]>
						<script src="https://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
						<![endif]-->

						<form id="form25" name="form25" class="wufoo topLabel page" accept-charset="UTF-8" autocomplete="off" enctype="multipart/form-data" method="post" novalidate="" action="https://tgrahl.wufoo.com/forms/mzu1lk616pqzyd/#public">

									<p class="desc" id="title1" for="Field1" style="margin-bottom:0;">Why didn't you join Launch a Bestseller? Be brutally honest. I don't want to waste your time with stuff that's not helpful to you.</p>
									<div style="margin-bottom:25px;">
										<textarea id="Field1" name="Field1" class="field textarea small" spellcheck="true" rows="10" cols="50" tabindex="1" onkeyup=""></textarea>
									</div>
									
									<p class="desc" id="title2" for="Field2" style="margin-bottom:0;">What is currently your biggest road block in your writing career? What's that one problem you would love for me to help you solve?</p>
									<div>
										<textarea id="Field2" name="Field2" class="field textarea small" spellcheck="true" rows="10" cols="50" tabindex="2" onkeyup=""></textarea>
									</div>
								
									<div style="display:none;">
										<input id="Field4" name="Field4" type="email" spellcheck="false" class="field text medium" value="<?php echo $_GET['EMAIL'] ?>" maxlength="255" tabindex="3">
									</div>
								
									<div>
										<input id="saveForm" name="saveForm" class="btTxt submit" type="submit" value="Submit">
									</div>
									
									<div style="display:none;">
										<label for="comment">Do Not Fill This Out</label> 
										<textarea name="comment" id="comment" rows="1" cols="1"></textarea> <input type="hidden" id="idstamp" name="idstamp" value="9LA5lqWeDlsFRSdYHVmthBWPUWE9KI+xVbP182otPjg=">
									</div>
								
						</form>


					</div>
				</div>
			</article>
		<?php endwhile; // end of the loop. ?>
<?php get_footer(); ?>