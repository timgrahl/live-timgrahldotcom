<?php get_header(); ?>
	<header class="homeheader">
		<div class="wrapper">
			<section class="bigasstim">
				<img src="<?php bloginfo('template_directory'); ?>/img/bigasstim.png" alt="Tim Grahl" />
			</section>
			<section class="topform">
				<h1>I'll show you how to sell more books.</h1>
				<form id="ck_subscribe_form" class="ck_subscribe_form" action="https://app.convertkit.com/landing_pages/9709/subscribe" data-remote="true">
			      	<input type="hidden" value="{&quot;form_style&quot;:&quot;naked&quot;,&quot;embed_style&quot;:&quot;inline&quot;,&quot;embed_trigger&quot;:&quot;scroll_percentage&quot;,&quot;scroll_percentage&quot;:&quot;70&quot;,&quot;delay_seconds&quot;:&quot;10&quot;,&quot;display_position&quot;:&quot;br&quot;,&quot;display_devices&quot;:&quot;all&quot;,&quot;days_no_show&quot;:&quot;15&quot;,&quot;converted_behavior&quot;:&quot;show&quot;}" id="ck_form_options"></input>
			     	<input type="hidden" name="id" value="9709" id="landing_page_id"></input>
					<input type="text" name="email" class="ck_email_address emailaddy" id="ck_emailField" placeholder="your email address here" required></input>
					<input name="SOURCE" type="hidden" value="Homepage Top Box" />
					<button type="submit">Sign Me Up!</button>
				</form>				
				<div>I'll show you the exact techniques I've used to build author platforms that launch #1 New York Times bestselling books.</div>
			</section>
		</div>
	</header>
	<section class="resourcesbook">
		<div class="wrapper">
			<section class="resources">
				<div>
					<h3>Resources</h3>
				</div>
				<div class="rbimage">
					<a href="/resources"><img src="<?php bloginfo('template_directory'); ?>/img/home-resources.png" width="316" height="235" alt="Home Resources"></a>
				</div>
				<div class="rbbutton">
					<a class="btn btn-grey" href="/resources">Learn More</a>
				</div>
			</section>
			<section class="mybook">
				<div>
					<h3>My Book</h3>
				</div>
				<div class="rbimage">
					<a href="http://bit.ly/yf1000c"><img src="<?php bloginfo('template_directory'); ?>/img/home-book.png" width="150" height="256" alt="Home Book"></a>
				</div>
				<div class="rbbutton">
					<a class="btn btn-grey" href="http://bit.ly/yf1000c">Buy Now</a>&nbsp;<a class="btn btn-grey" href="http://first1000copies.com">Learn More</a>
				</div>
			</section>
		</div>
	</section>
	<section class="quotes">
		<div class="wrapper">
			<div class="quotechar">
				<img src="<?php bloginfo('template_directory'); ?>/img/quote-char.png" width="45" height="36" alt="Quote Char">
			</div>
			<div class="quote">
				<span class="quote0">&quot;If I could give an aspiring writer one piece of advice, it would be to read this book.&quot;<br /><br /></span>
				<span class="quote1 disabled">&quot;Tim Grahl is fast becoming a legend, almost single-handedly changing the way authors around the world spread ideas and connect with readers.&quot;</span>
				<span class="quote2 disabled">&quot;I watched in awe this year as Tim Grahl had 5 clients on the New York Times bestseller list in the same week. There is no one I trust more to learn about book marketing.&quot;</span>
			</div>
			
			<div class="quoteperson">
				<div class="person">
					<img class="person0 color" src="<?php bloginfo('template_directory'); ?>/img/howey-color.png" width="101" height="101" alt="Howey Color">
					<p>
						HUGH HOWEY<br />
						<span>Wool series</span>
					</p>
				</div>
				<div class="person">
					<img class="person1 color desaturate" src="<?php bloginfo('template_directory'); ?>/img/pink-color.png" width="101" height="101" alt="Pink Color">
					<p>
						DANIEL PINK<br />
						<span>To Sell Is Human</span>
					</p>
				</div>
				<div class="person">
					<img class="person2 color desaturate" src="<?php bloginfo('template_directory'); ?>/img/slim-color.png" width="101" height="101" alt="Slim Color">
					<p>
						PAMELA SLIM<br />
						<span>Body of Work</span>
					</p>
				</div>
			</div>
		</div>
		<script type="text/javascript">
		jQuery(document).ready(function(){
			var tid = setInterval(mycode, 8000);
			var counter = 1;
			function mycode() {
				jQuery(".person img").addClass("desaturate");
				jQuery(".person" + counter%3).removeClass("desaturate");
				jQuery(".quote span").hide();
				jQuery(".quote" + counter%3).fadeIn("1000", function() {
			  		jQuery(this).show();
				});
				counter++;
			}
		});
	
		</script>
	</section>
	<section class="blogs">
		<div class="wrapper">
			<h3><a href="/articles">Tim's Blog</a></h3>
			<div class="frontblogs">
				<?php 
				query_posts( 'posts_per_page=3' );
				$c = 1;
				if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				<section class="blogpost <?php if($c==1) : ?>first<?php elseif($c==2) : ?>mid<?php else : ?>last<?php endif; ?>">
					<span class="date"><?php the_time('F jS, Y'); ?></span>
					<h2><a href="<?php the_permalink(); ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
					<p><?php the_excerpt(); ?></p>
				</section>
				<? $c++;endwhile;endif; ?>
			</div>
			<a href="/articles" class="btn btn-blue homegetmore">Get More</a>
		</div>
	</section>
	<?php get_footer(); ?>