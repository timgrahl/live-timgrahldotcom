<div class="tiers" id="toptier">
	<h4>Option 1</h4>
	<h3>Launch a Bestseller</h3>
	<p>When you join Launch a Bestseller you get access to the full course including:</p>
	<ul>
		<li><strong>50+ lessons HD video and downloadable audio</strong>. All accessible in our easy-to-use, fully mobile optimized custom learning platform.</li>
		<li><strong>My client vault</strong>. The exact email copy I use for myself and my clients when doing outreach, getting blurbs and more.</li>
		<li><strong>Custom launch roadmap</strong>. I customize your roadmap through the course based on your current platform and launch plan.</li>
		<li><strong>Step-by-step checklist and timeline</strong>. Also customized to your launch plan, I give you a walkthrough on every thing that needs to be done and when it needs to be done.</li>
		<li><strong>Tech walkthroughs</strong>. Don't let technology hold you back. I provide step-by-step walkthroughs on setting up your backend launch systems.</li>
		<li><strong>All bonus lessons and downloadable materials</strong>. Learn about traditional bestseller lists, how I sold 10,000 copies of my book, how to work with your publisher, how to leverage LinkedIn and much more!</li>
		<li><strong>Interviews with top experts</strong>. Hear in-depth interviews with my fellow book marketing experts.</li>
		<li><strike><strong>12 Live Masterclass Q&amp;A and Member Launch Feedback</strong>. Join me on a monthly live webinar to ask your questions live plus have me give feedback on your launch plan.</strike><br />(Available in Option 2)</li>
		<li><strike><strong>The Productive Writer</strong>. <em>$197 value</em>. Learn the productivity secrets of top writers and finish your book in 60 days. A 5-week course complete with worksheets, step-by-step plan, and 60 day timeline to finish your book and reach your goals!</strike><br />(Available in Option 2)</li>
	</ul>
	<div style="text-align:center;">
		<? if($earlybird == 1) { ?>
			<p>$78/month for 12 payments</p>
			<p><strong>Early Bird pricing ends in...</strong><br /><span class="puttime"></span></p>
			<p><a href="https://timgrahl.samcart.com/products/launch-a-bestseller---tier-1i5dwp" class="btn-blue">Get Instant Access Now</a></p>
			<p><img class="noborder" style="display:inline;" src="<?php bloginfo('template_directory'); ?>/img/sales/lab/ccs.png" width="175" height="36" alt="Ccs"></p>
			<p><a href="https://timgrahl.samcart.com/products/launch-a-bestseller---tier-1---single-payment62zrm">or, Pay in full - $847</a></p>
		<? } else { ?>
			<p>$91/month for 12 payments</p>
			<p>The cart closes in...<br /><span class="puttime"></span></p>
			<p><a href="https://timgrahl.samcart.com/products/launch-a-bestseller---tier-1i5dwp" class="btn-blue">Get Instant Access Now</a></p>
			<p><img class="noborder" style="display:inline;" src="<?php bloginfo('template_directory'); ?>/img/sales/lab/ccs.png" width="175" height="36" alt="Ccs"></p>
			<p><a href="https://timgrahl.samcart.com/products/launch-a-bestseller---tier-1---single-payment62zrm">or, Pay in full - $997</a></p>
		<? } ?>
	</div>
</div>

<div class="tiers">
	<h4>Option 2</h4>
	<h3>Launch a Bestseller<br />+ 12 Master Classes<br />+ The Productive Writer</h3>
	
	<div style="text-align:center;margin-bottom:25px;"><img class="noborder" style="margin:0 auto;" src="<?php bloginfo('template_directory'); ?>/img/sales/lab/bestvalue.png" width="250" height="53" alt="Bestvalue"></div>
	
	<p>When you join Launch a Bestseller you get access to the full course including:</p>
	<ul>
		<li><strong>12 Live Masterclass Q&amp;A and Member Launch Feedback</strong>. Join me on a monthly live webinar to ask your questions live plus have me give feedback on your launch plan.</li>
		<li><strong>The Productive Writer</strong>. <em>$197 value</em>. Learn the productivity secrets of top writers and finish your book in 60 days. A 5-week course complete with worksheets, step-by-step plan, and 60 day timeline to finish your book and reach your goals!</li>
		<li><strong>50+ lessons HD video and downloadable audio</strong>. All accessible in our easy-to-use, fully mobile optimized custom learning platform.</li>
		<li><strong>My client vault</strong>. The exact email copy I use for myself and my clients when doing outreach, getting blurbs and more.</li>
		<li><strong>Custom launch roadmap</strong>. I customize your roadmap through the course based on your current platform and launch plan.</li>
		<li><strong>Step-by-step checklist and timeline</strong>. Also customized to your launch plan, I give you a walkthrough on every thing that needs to be done and when it needs to be done.</li>
		<li><strong>Tech walkthroughs</strong>. Don't let technology hold you back. I provide step-by-step walkthroughs on setting up your backend launch systems.</li>
		<li><strong>All bonus lessons and downloadable materials</strong>. Learn about traditional bestseller lists, how I sold 10,000 copies of my book, how to work with your publisher, how to leverage LinkedIn and much more!</li>
		<li><strong>Interviews with top experts</strong>. Hear in-depth interviews with my fellow book marketing experts.</li>
	</ul>							
	
	<div style="text-align:center;">
		<? if($earlybird == 1) { ?>
			<p>$155/month for 12 payments</p>
			<p><strong>Early Bird pricing ends in...</strong><br /><span class="puttime"></span></p>
			<p><a href="https://timgrahl.samcart.com/products/launch-a-bestseller---tier-2ot3k3" class="btn-blue">Get Instant Access Now</a></p>
			<p><img class="noborder" style="display:inline;" src="<?php bloginfo('template_directory'); ?>/img/sales/lab/ccs.png" width="175" height="36" alt="Ccs"></p>
			<p><a href="https://timgrahl.samcart.com/products/launch-a-bestseller---tier-2---single-paymentzjqwi">or, Pay in full - $1697</a></p>
		<? } else { ?>
			<p>$183/month for 12 payments</p>
			<p>The cart closes in...<br /><span class="puttime"></span></p>
			<p><a href="https://timgrahl.samcart.com/products/launch-a-bestseller---tier-2ot3k3" class="btn-blue">Get Instant Access Now</a></p>
			<p><img class="noborder" style="display:inline;" src="<?php bloginfo('template_directory'); ?>/img/sales/lab/ccs.png" width="175" height="36" alt="Ccs"></p>
			<p><a href="https://timgrahl.samcart.com/products/launch-a-bestseller---tier-2---single-paymentzjqwi">or, Pay in full - $1997</a></p>
		<? } ?>
	</div>
</div>

<div class="tiers">
	<h4>Option 3</h4>
	<h3>Coaching Package</h3>
	<h3>Launch a Bestseller<br />+ 3 Live, 1-hour Coaching Calls<br />+ 12 Master Classes<br />+ The Productive Writer</h3>
	<p>When you join Launch a Bestseller you get access to the full course including:</p>
	<ul>
		<li><strong>3 Live, 1-hour Coaching Calls</strong>. Usually priced at $500/hr, you get three 1-hour calls with me to get 1-on-1 coaching and planning for your platform and launch.</li>
		<li><strong>12 Live Masterclass Q&amp;A and Member Launch Feedback</strong>. Join me on a monthly live webinar to ask your questions live plus have me give feedback on your launch plan.</li>
		<li><strong>The Productive Writer</strong>. <em>$197 value</em>. Learn the productivity secrets of top writers and finish your book in 60 days. A 5-week course complete with worksheets, step-by-step plan, and 60 day timeline to finish your book and reach your goals!</li>
		<li><strong>50+ lessons HD video and downloadable audio</strong>. All accessible in our easy-to-use, fully mobile optimized custom learning platform.</li>
		<li><strong>My client vault</strong>. The exact email copy I use for myself and my clients when doing outreach, getting blurbs and more.</li>
		<li><strong>Custom launch roadmap</strong>. I customize your roadmap through the course based on your current platform and launch plan.</li>
		<li><strong>Step-by-step checklist and timeline</strong>. Also customized to your launch plan, I give you a walkthrough on every thing that needs to be done and when it needs to be done.</li>
		<li><strong>Tech walkthroughs</strong>. Don't let technology hold you back. I provide step-by-step walkthroughs on setting up your backend launch systems.</li>
		<li><strong>All bonus lessons and downloadable materials</strong>. Learn about traditional bestseller lists, how I sold 10,000 copies of my book, how to work with your publisher, how to leverage LinkedIn and much more!</li>
		<li><strong>Interviews with top experts</strong>. Hear in-depth interviews with my fellow book marketing experts.</li>
	</ul>
	<div style="text-align:center;">
		<? if($earlybird == 1) { ?>
			<p>$229/month for 12 payments</p>
			<p><strong>Early Bird pricing ends in...</strong><br /><span class="puttime"></span></p>
			<p><a href="https://timgrahl.samcart.com/products/launch-a-bestseller---coachingZhDFb" class="btn-blue">Get Instant Access Now</a></p>
			<p><img class="noborder" style="display:inline;" src="<?php bloginfo('template_directory'); ?>/img/sales/lab/ccs.png" width="175" height="36" alt="Ccs"></p>
			<p><a href="https://timgrahl.samcart.com/products/launch-a-bestseller---coaching">or, Pay in full - $2497</a></p>
		<? } else { ?>
			<p>$275/month for 12 payments</p>
			<p>The cart closes in...<br /><span class="puttime"></span></p>
			<p><a href="https://timgrahl.samcart.com/products/launch-a-bestseller---coachingZhDFb" class="btn-blue">Get Instant Access Now</a></p>
			<p><img class="noborder" style="display:inline;" src="<?php bloginfo('template_directory'); ?>/img/sales/lab/ccs.png" width="175" height="36" alt="Ccs"></p>
			<p><a href="https://timgrahl.samcart.com/products/launch-a-bestseller---coaching">or, Pay in full - $2997</a></p>
		<? } ?>
	</div>
</div>