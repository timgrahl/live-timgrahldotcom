<?php get_header(); ?>


			<article class="articlepage">
				<div class="wrapper">
					<h1 class="page-title"><?php printf( __( 'Search Results for: %s', 'shape' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
					<div class="entry">
	              	<?php if ( have_posts() ) : ?>

		                <?php /* Start the Loop */ ?>
						<ol>
		                <?php while ( have_posts() ) : the_post(); ?>
							<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
		                <?php endwhile; ?>
						</ol>
		                <?php shape_content_nav( 'nav-below' ); ?>

		            <?php else : ?>

		                <h3>I couldn't find anything.<br /><br />Give it another try...</h3>
						<form role="search" method="get" class="search-form search-bar" action="<?php echo home_url( '/' ); ?>">
						  <input type="search" placeholder="Search For..." value="<?php echo get_search_query() ?>" name="s" title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" />
						  <button type="submit">
						    <img src="https://raw.githubusercontent.com/thoughtbot/refills/master/source/images/search-icon.png" alt="Search Icon">
						  </button>
						</form>
						<h3>or, check out one of my popular articles...</h3>
						<h2>Most Popular Articles</h2>
						<?php
						$pop_posts = new WP_Query(array(
							'category_name' => 'popular',
							'posts_per_page'	=> '-1'
							));
						if ($pop_posts->have_posts()) : 
							?>
							<ol>
							<?php
							while ($pop_posts->have_posts()) : $pop_posts-> the_post();
							?>
								<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
							<?php
							endwhile;
							?>
							</ol>
							<?php
						endif;?>
							

		            <?php endif; ?>
					</div>		
				</div>
			</article>
<?php get_footer(); ?>
