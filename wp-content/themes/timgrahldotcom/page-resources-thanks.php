<?php
/**
 * Template Name: Resources Thanks
 *
 */

$loggedin = false;
if(isset($_POST['RESURL'])) {
    $general_options = get_option('_wp_convertkit_settings');
    $api_key         = $general_options && array_key_exists("api_key", $general_options) ? $general_options['api_key'] : null;
    $CKapi       = new ConvertKitAPI($api_key);
	$member_info = $CKapi->form_subscribe(
	        $_POST['id'],
	        array(
	          'email' => $_POST['email'],
	          'fname' => $_POST['first_name']
	        )
	      );
	 $tag_info = $CKapi->tag_subscribe(
	  	        $_POST['CKID'],
	  	        array(
	  	          'email' => $_POST['email'],
	  	          'fname' => $_POST['first_name']
	  	        )
	  	      );
    print_r($tag_info);
	setcookie('tgcookie', "1", time() + (86400 * 30), "/"); // 86400 = 1 day
	setcookie('tgemail', $_POST['email'], time() + ((86400 * 365) * 5), "/"); // 86400 = 1 day
	header("Location: " . get_permalink($_POST['RESURL']) . "?start=true");
}

get_header(); ?>
		<?php while ( have_posts() ) : the_post(); ?>

			<?php get_template_part( 'content', 'single' ); ?>

		<?php endwhile; // end of the loop. ?>
<?php get_footer(); ?>
