<?php
/**
 * Template Name: Resources
 */

get_header(); ?>
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<div class="wrapper">
				<?php the_title( sprintf( '<h1 class="entry-title">', esc_url( get_permalink() ) ), '</h1>' ); ?>
				
				
				<div class="resources-section resources-courses">
					<h2>Courses</h2>
					<?php 
					$resources = new WP_Query(array(
								'posts_per_page' => '-1',
								'post_type'	=> 'resources',
								'type'		=>	'course',
								'orderby'	=>	'menu_order',
								'order'		=>	'ASC',
								));
					if ($resources->have_posts()) : 
						$counter = 0;
						while ($resources->have_posts()) {
							$counter++;
							$resources->the_post();
							?>
							<div class="resource-single<?php if($counter > 3) { ?> extra-course<?php } ?>">
								<h3><?php the_title(); ?></h3>
								<div class="rbimage">
									<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_post_thumbnail('medium', array('title' => $post->post_title)); ?></a>
								</div>
								<div class="rbbutton">
									<a class="btn btn-grey" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">Get the Resource</a>
								</div>
							</div>
							<?php
						}
						if($counter > 3) {
							?>
							<a class="btn btn-yellow seemorecourses" href="<?php the_permalink(); ?>">SEE MORE COURSES</a>
							<?php
						}
					endif;
					?>		
				</div>
				
				<div class="resources-section resources-guides">
					<h2>Guides</h2>
					<?php 
					$resources = new WP_Query(array(
								'posts_per_page' => '-1',
								'post_type'	=> 'resources',
								'type'		=>	'guides',
								'orderby'	=>	'menu_order',
								'order'		=>	'ASC',
								));
					if ($resources->have_posts()) : 
						$counter = 0;
						while ($resources->have_posts()) {
							$counter++;
							$resources->the_post();
							?>
							<div class="resource-single<?php if($counter > 3) { ?> extra-guide<?php } ?>">
								<h3><?php the_title(); ?></h3>
								<div class="rbimage">
									<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_post_thumbnail('medium', array('title' => $post->post_title)); ?></a>
								</div>
								<div class="rbbutton">
									<a class="btn btn-grey" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">Get the Resource</a>
								</div>
							</div>
							<?php
						} // end while
						if($counter > 3) {
							?>
							<a class="btn btn-blue seemoreguides" href="<?php the_permalink(); ?>">SEE MORE GUIDES</a>
							<?php
						}
					endif;
					?>		
				</div>
				
				<div class="resources-section resources-tools">
					<h2>Recommended Tools</h2>
					<?php 
					$resources = new WP_Query(array(
								'posts_per_page' => '-1',
								'post_type'	=> 'resources',
								'type'		=>	'recommended-tool',
								'orderby'	=>	'menu_order',
								'order'		=>	'ASC',
								));
					if ($resources->have_posts()) : 
						$counter = 0;
						while ($resources->have_posts()) {
							$counter++;
							$resources->the_post();
							?>
							<div class="resource-single<?php if($counter > 3) { ?> extra-tool<?php } ?>">
								<h3><?php the_title(); ?></h3>
								<div class="rbimage">
									<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_post_thumbnail('medium', array('title' => $post->post_title)); ?></a>
								</div>
								<div class="rbbutton">
									<a class="btn btn-grey" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">Get the Resource</a>
								</div>
							</div>
							<?php
						}
						if($counter > 3) {
							?>
							<a class="btn btn-darkyellow seemoretools" href="<?php the_permalink(); ?>">SEE MORE TOOLS</a>
							<?php
						}
					endif;
					?>		
				</div>
			
			
			</div>
		</article>
		<script type="text/javascript">
		jQuery(document).ready(function(){
			jQuery('.seemorecourses').click(function(){
				jQuery('.extra-course').toggle("slow");
				jQuery('.seemorecourses').toggle(0);
				return false;
			});
			jQuery('.seemoreguides').click(function(){
				jQuery('.extra-guide').toggle("slow");
				jQuery('.seemoreguides').toggle(0);
				return false;
			});
			jQuery('.seemoretools').click(function(){
				jQuery('.extra-tool').toggle("slow");
				jQuery('.seemoretools').toggle(0);
				return false;
			});
		});
		</script>
<?php get_footer(); ?>
