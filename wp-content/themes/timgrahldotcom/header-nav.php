
		<?php if(!is_home()) { ?>
		<header class="header">
			<div class="wrapper">
				<div class="logo"><a href="<?php bloginfo('url'); ?>">Tim Grahl</a></div>
				<div class="topnav">
					<nav role="navigation">
						<ul id="js-centered-navigation-menu" class="centered-navigation-menu show">
							<li class="nav-link"><a href="<?php bloginfo('url'); ?>">Home</a></li>
							<li class="nav-link"><a href="/about">About</a></li>
							<li class="nav-link"><a href="/articles">Articles</a></li>
							<li class="nav-link"><a href="/resources/">Resources</a></li>
							<li class="nav-link"><a href="/books">Books</a></li>
							<li class="nav-link"><a href="/contact">Contact</a></li>
						</ul>
					</nav>
				</div>
				<div class="starthere">
					<a href="/start-here">Start Here</a>
				</div>
			</div>
		</header>
		<?php } ?>
