		<section class="footer">
			<div class="wrapper">
				<div>
					<div class="sitemap">
						<h4>Site Map</h4>
						<ul>
							<li><a href="/about">About</a></li>
							<li><a href="/articles">Articles</a></li>
							<li><a href="/resources">Resources</a></li>
							<li><a href="/books">Books</a></li>
							<li><a href="/contact">Contact</a></li>
							<li><a href="/start-here">Start Here</a></li>
						</ul>
						
						<form role="search" method="get" class="search-form search-bar" action="<?php echo home_url( '/' ); ?>">
						  <input type="search" placeholder="Search For..." value="<?php echo get_search_query() ?>" name="s" title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" />
						  <button type="submit">
						    <img src="https://raw.githubusercontent.com/thoughtbot/refills/master/source/images/search-icon.png" alt="Search Icon">
						  </button>
						</form>
					</div>
					<div class="training">
						<h4>Training</h4>
						<a class="btn btn-blue" href="http://authorguides.com/">Login</a>
						<button class="darkblue disabled"><a href="#">Learn More</a></button>
					</div>
					<div class="totop">
						<h4 class="scrollToTop">Back to Top</h4>
						<p>© Common Insights - Tim Grahl</p>
						<img src="<?php bloginfo('template_directory'); ?>/img/icon-backtotop.png" class="scrollToTop" width="46" height="46" alt="Icon Backtotop">
					</div>
				</div>
			</div>		
		</section>