<?php
/**
 * Template Name: Sales Page
 */
date_default_timezone_set('America/Los_Angeles');

// This is the function that kicks to the cart closed page. If there's no cookie then we have to go off the $get[d].
if(isset($_COOKIE['tgLABcookie'])){ 
	$cookiepieces = array();
	// Piece [0] is the number of days left when the cookie was set and [1] is the date the cookie was set
	$cookiepieces = explode(",",$_COOKIE['tgLABcookie']);

	// This bit figures out how many days have alapsed since the cookie was set
	$now = time(); 
	$your_date = strtotime($cookiepieces[1]);
	$datediff = $now - $your_date;
	$dadiff = floor($datediff / (60 * 60 * 24));

	// This finds out if more days have alapsed since the cookie was set than the cart is open. So if there was only two days left and we're now three days later, the cart is closed.
	$answer = (($cookiepieces[0]-1) - $dadiff);
	
	if($answer < 0) {
		header("Location: http://timgrahl.com/launch-a-bestseller/");
	}	
}



//defaulting to 11 days that the cart is open
$daysleft = $tgLAB = 11;

//did we pass a number of days? If so, save that as daysleft. If not, grab the cookie and make that daysleft. If not, make daysleft 11.
if(isset($_GET['d'])){ $daysleft = $_GET['d']; }
elseif(isset($_COOKIE['tgLABcookie'])){ $daysleft = $_COOKIE['tgLABcookie']; }
else{ $daysleft = 11; }

// Is there a cookie yet? if so, find out if the cookie is newer or the $get[d] is newer. I want to makes sure not to reset the cookie if someone clicks an old link.
// If there's no cookie yet, go ahead and set it to daysleft
// In the cookie I save the daysleft plus when the cookie was set. I need to do this in order to close the cart.
if(isset($_COOKIE['tgLABcookie'])){ 
	$tgLAB = $_COOKIE['tgLABcookie'];
	if($daysleft < $tgLAB) {
		setcookie("tgLABcookie", $daysleft . "," . date("Y-m-d"), time() + (86400 * 365), "/"); // 86400 = 1 day
	} else {
		$daysleft = $tgLAB;
	}
} else {
	setcookie("tgLABcookie", $daysleft . "," . date("Y-m-d"), time() + (86400 * 365), "/"); // 86400 = 1 day
}




// Kick over the earlybird pricing after 7 days
if($daysleft > 7) {
	$earlybird = 1;
} else {
	$earlybird = 0;
}

// makes the math easier for the count down timer
$daysleft -= 1;
// formats the date for the count down timer
$counterdowner = date("Y-m-d",strtotime("+".$daysleft."days"));

$thesiteurl = get_site_url();
?>

<!DOCTYPE html>
<html lang="en-US"
 xmlns:fb="http://ogp.me/ns/fb#">
    <head>
        <meta charset="UTF-8" />
        <title>Launch a Bestseller | Tim Grahl</title>
        <link rel="profile" href="http://gmpg.org/xfn/11" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="google-site-verification" content="inL2BzG2ZTKEYL75v4rHe0jAuxS6Mu4LifWV-HjpS7M" />
		<!-- FAVICON STUFF -->
		<link rel="apple-touch-icon" sizes="57x57" href="<?php echo $thesiteurl; ?>/wp-content/themes/timgrahldotcom/img/favicon/apple-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="<?php echo $thesiteurl; ?>/wp-content/themes/timgrahldotcom/img/favicon/apple-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="<?php echo $thesiteurl; ?>/wp-content/themes/timgrahldotcom/img/favicon/apple-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="<?php echo $thesiteurl; ?>/wp-content/themes/timgrahldotcom/img/favicon/apple-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="<?php echo $thesiteurl; ?>/wp-content/themes/timgrahldotcom/img/favicon/apple-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="<?php echo $thesiteurl; ?>/wp-content/themes/timgrahldotcom/img/favicon/apple-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="<?php echo $thesiteurl; ?>/wp-content/themes/timgrahldotcom/img/favicon/apple-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="<?php echo $thesiteurl; ?>/wp-content/themes/timgrahldotcom/img/favicon/apple-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="<?php echo $thesiteurl; ?>/wp-content/themes/timgrahldotcom/img/favicon/apple-icon-180x180.png">
		<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo $thesiteurl; ?>/wp-content/themes/timgrahldotcom/img/favicon/android-icon-192x192.png">
		<link rel="icon" type="image/png" sizes="32x32" href="<?php echo $thesiteurl; ?>/wp-content/themes/timgrahldotcom/img/favicon/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="96x96" href="<?php echo $thesiteurl; ?>/wp-content/themes/timgrahldotcom/img/favicon/favicon-96x96.png">
		<link rel="icon" type="image/png" sizes="16x16" href="<?php echo $thesiteurl; ?>/wp-content/themes/timgrahldotcom/img/favicon/favicon-16x16.png">
		<link rel="manifest" href="<?php echo $thesiteurl; ?>/wp-content/themes/timgrahldotcom/img/favicon/manifest.json">
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="<?php echo $thesiteurl; ?>/wp-content/themes/timgrahldotcom/img/favicon/ms-icon-144x144.png">
		<meta name="theme-color" content="#ffffff">
		<meta name="google-site-verification" content="GkYNHGMrRq8AH9s1SyoP2D_aeKwUE3EHptfMAr4nPqA" />
		<!-- END FAVICON STUFF -->
		
        <link rel="stylesheet" href="<?php echo $thesiteurl; ?>/wp-content/themes/timgrahldotcom/style.css?v=1" type="text/css" media="screen" />
		<link rel="stylesheet" href="<?php echo $thesiteurl; ?>/wp-content/themes/timgrahldotcom/print.css" type="text/css" media="print" />
        <link rel="pingback" href="<?php echo $thesiteurl; ?>/xmlrpc.php" />

				<script>
		(function(d) {
		  var tkTimeout=3000;
		  if(window.sessionStorage){if(sessionStorage.getItem('useTypekit')==='false'){tkTimeout=0;}}
		  var config = {
		    kitId: 'yul1bwz',
		    scriptTimeout: tkTimeout
		  },
		  h=d.documentElement,t=setTimeout(function(){h.className=h.className.replace(/\bwf-loading\b/g,"")+"wf-inactive";if(window.sessionStorage){sessionStorage.setItem("useTypekit","false")}},config.scriptTimeout),tk=d.createElement("script"),f=false,s=d.getElementsByTagName("script")[0],a;h.className+="wf-loading";tk.src='//use.typekit.net/'+config.kitId+'.js';tk.async=true;tk.onload=tk.onreadystatechange=function(){a=this.readyState;if(f||a&&a!="complete"&&a!="loaded")return;f=true;clearTimeout(t);try{Typekit.load(config)}catch(e){}};s.parentNode.insertBefore(tk,s)
		})(document);
		</script>
        
<!-- All in One SEO Pack 2.2.6.2 by Michael Torbert of Semper Fi Web Design[121,156] -->
<link rel="author" href="http://google.com/+TimGrahl23" />

<link rel="canonical" href="<?php echo $thesiteurl; ?>/programs/launch-a-bestseller/" />
<!-- /all in one seo pack -->
<link rel="alternate" type="application/rss+xml" title="Tim Grahl &raquo; Launch a Bestseller Comments Feed" href="<?php echo $thesiteurl; ?>/programs/launch-a-bestseller/feed/" />
		
<script type='text/javascript' src='<?php echo $thesiteurl; ?>/wp-includes/js/jquery/jquery.js?ver=1.11.2'></script>
<script type='text/javascript' src='<?php echo $thesiteurl; ?>/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.2.1'></script>

<meta property="fb:app_id" content="130357656259"/>	
		<script src="<?php echo $thesiteurl; ?>/wp-content/themes/timgrahldotcom/js/modernizr.custom.76227.js" type="text/javascript" charset="utf-8"></script>
		<script src="<?php echo $thesiteurl; ?>/wp-content/themes/timgrahldotcom/js/jquery.cookie.js" type="text/javascript" charset="utf-8"></script>
		<script src="<?php echo $thesiteurl; ?>/wp-content/themes/timgrahldotcom/js/min/includes-min.js" type="text/javascript" charset="utf-8"></script>
    </head>
		<div class="fixedhead">
			<? if($earlybird == 1) { ?>Early Bird pricing ends in...<? } else { ?>The cart closes in...<? } ?> &nbsp;&nbsp;<span class="showbutton" style="display:none;"><span class="topbuy">Buy Now</span></span><br /><span class="puttime"></span>
		</div>
		<article id="post-<?php the_ID(); ?>" class="salespage lab-sales">
			<div class="topper">
				<div class="tcontent">
					<h1>How Anyone Can Launch<br />a Bestselling Book</h1>
					<iframe src="https://player.vimeo.com/video/154625375?color=000000&title=0&byline=0&portrait=0" width="571" height="321" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
					<h2>The hardest part of writing a book isn’t writing it.</h2>
					<h2>It’s launching it.</h2>
				</div>
				<div style="clear:left">&nbsp;</div>
			</div>
			<div class="wrapper">
				<div class="entry">
					<p>Putting a book out into the world is terrifying. There are so many things that are poured into that moment.</p>
				
					<p>You’ve worked on this project for months, or years.</p>

					<p>Your family has encouraged you. Your friends have doubted you.</p>

					<p>You’ve spent money you don’t have on editing and cover design.</p>

					<p>You’ve dreamed about this moment.</p>

					<p>And now--you’re finally launching your book!</p>
					<h3>But when the time comes, what do you do to make that launch successful?</h3>
					<p>Maybe you’ve Googled it. I did.</p>

					<p><strong>Here’s some of the advice I found:</strong></p>
					<ul>
						<li>Embed a Retweet button in a free chapter of the book</li>
						<li>Leverage photos on Facebook</li>
						<li>Post to free eBook sites</li>
						<li>Hold a few GoodReads giveaways</li>
						<li>Encourage word-of-mouth</li>
					</ul>
					<p><em><strong>This</strong></em> is why it’s so hard to launch your book.</p>
				
					<p>You know that most of the advice you find isn’t actually going to work.</p>

					<p>Sure, adding a retweet button or putting photos on Facebook might sell a handful of copies.</p>

					<p>But you don’t want to sell a handful of copies!</p>

					<p>You want to have a <em>successful launch</em>.</p>

					<h3>What do you really want as an author?</h3>

					<p>Of course you dream of having a big-time <em>New York Times</em> bestselling book one day. That’d be great, obviously.</p>
				
					<p>But really, all you want is to sell enough copies to keep writing.</p>

					<p>Maybe even get to the point where you can support your family.</p>

					<p>Get out of the rat race and <em>actually have time to write</em>.</p>

					<p>Sell enough books to show yourself that it was worth all this time and effort, and that you’re not an idiot for having this dream in the first place.</p>

					<p><strong>But once again, we circle back around to the problem . . .</strong></p>

					<p><em>How do you actually launch your book?</em></p>

					<p>No matter how much you Google and read and ask your author friends, all that advice never seems to crystallize into an actual <em>plan</em>.</p>

					<p>So your book comes out, and you post about it on Facebook and Twitter, and on your blog.</p>

					<p>You get excited as some sales start rolling in, and you see your Amazon ranking start to spike.</p>

					<p>You run past the #100,000 rank in your Amazon category.</p>

					<p>Then you break past #50,000.</p>

					<p>Then #20,000!</p>

					<p>Somewhere around the #18,000 mark, it’s midnight on your launch day, and you’re exhausted. So you go to bed, excited to see where you’ll be in the morning.</p>

					<p>You might even put your laptop next to the bed, so you can check your ranking first thing.</p>

					<p>But when you check again in the morning, it’s dropped back down to around the #20,000 mark.</p>

					<p><strong>You post about the book a few more times on Facebook, trying not to sound too desperate.</strong></p>

					<p>Your book’s ranking drops to #30,000.</p>

					<p>Then to #50,000.</p>
					
					<p>Then...</p>

					<p>The fear sets in. And the shame.</p>

					<p><em>The realization that this book is going to be a failure in terms of sales, and that you have no idea how you’re going to get closer to your dream of being a full-time writer.</em></p>

					<p>A week later, and all it takes to get you excited is a single sale coming in.</p>

					<p>And you’re not alone. Because according to industry stats:</p>

					<h3>Most books sell less than 250 copies in their first year.</h3>

					<p>That applies to both indie and traditionally published books.</p>
				
					<p>250 copies!</p>

					<p>If you’re making $2 a book . . . dear god, that’s only $500!</p>

					<p>That doesn’t even cover your editing and design expenses, never mind all the time you spent writing!</p>

					<p>And it definitely doesn’t get you any closer to your goal of writing full-time.</p>
					
					<h3>“My Amazon ranking is sitting at #176,037.<br /><em><strong>God</strong></em>, I’m so embarrassed!”</h3>
					
					<p><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sales/lab/amazonrank.png" width="700" height="121" alt="Amazonrank"></p>

					<p>And that’s not the sad part. Here’s what’s so bad about launching a book that doesn’t sell:</p>
					
					<p><strong>It takes you even <em>further</em> away from your goal of being a successful writer.</strong></p>

					<p>The support from your family and friends starts to falter.</p>

					<p>Maybe you should just be happy teaching middle school English the rest of your life.</p>

					<p>Or sitting in a cubicle answering phones.</p>

					<p>Or serving coffee in a diner.</p>

					<p>Now it’s harder than ever to sit down and write.</p>

					<p>Because — what’s the point? You’re just going to put another book out into the world, sell a few dozen copies, and be done.</p>

					<p>And every day you don’t move towards your goal, you’re getting further away from it.</p>

					<p><em>But what if that wasn’t the case?</em></p>

					<h3>What if, instead of launching a book and hoping it sells, you had an actual concrete plan — a proven way to sell your book?</h3>

					<p>And not just 250 copies, but enough to make a dent in your dreams.</p>

					<p>Pay some bills with the money. Quit your full-time job.</p>

					<p><strong>Become a bestselling author.</strong></p>

					<p>Not just bestselling-in-some-obscure-Amazon-category . . . but a legit <em>New York Times</em> or <em>Wall Street Journal</em> or <em>Washington Post</em> or <em>USA Today</em> bestseller.</p>

					<p>Sound impossible? I used to think so too . . .</p>

					<h3>Hi, I’m Tim Grahl.</h3>

					<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sales/lab/hiimtim.png" width="228" height="228" alt="Hiimtim" class="Hiimtim">
					<p>I’ve spent the last 8 years working with authors to help them connect with readers, build their platforms, and sell more books.</p>

					<p>During that journey, I’ve worked directly with more than 150 authors of all kinds. I’ve worked with fiction and nonfiction authors. From big-time bestselling authors, to writers trying to get their first project off the ground.</p>

					<p>I’ve also done consulting work for hundreds of other successful authors, and trained tens of thousands of authors through my online courses and trainings.</p>

					<p>During that time, I’ve launched dozens of bestselling books — including multiple #1 bestsellers on major lists such as the <em>New York Times</em> and <em>Wall Street Journal</em>, among others.</p>

					<p><strong>But, of course, that’s not where I started.</strong></p>

					<p>Like everyone else, I started out knowing nothing about book marketing.</p>

					<p>I assumed that in order to be successful, you have to have a big-time publisher that will put out a ton of money to promote your book, and a big New York publicist who can get you on the Today show.</p>

					<h3>Then I met the guy who was about to become the bestselling author of <em><strong>I Will Teach You To Be Rich</strong></em> </h3>

					<p>Way back in 2008, I started working with Ramit Sethi behind-the-scenes, helping with his website and other technology. When he landed a book contract, he hired me to help continue to build his author platform.</p>

					<p>I worked with him that whole year, leading up to his launch.</p>

					<p>And honestly — I didn’t think much would come of it. He was one of my many clients. I worked hard for him, but come on! It wasn’t like the book was going to go that far.</p>

					<p>He didn’t have a publicist.</p>

					<p>He didn’t have ads going in any magazines.</p>

					<p>He wasn’t on a speaking tour.</p>

					<p>And he definitely wasn’t getting interviewed on the <em>Today Show</em>.</p>

					<p>And then . . . </p>

					<p><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sales/lab/ramitbestseller.png" width="864" height="460" alt="Ramitbestseller"></p>

					<p>That’s right. </p>

					<p>His book debuted at #9 on the <em>New York Times</em> bestseller list.</p>

					<h3>What just happened here?</h3>

					<p>What I didn’t know, was that Ramit had found the secret to successful book launches — and it wasn’t spending $50k on a publicist.</p>

					<p>Or buying ads in Times Square.</p>

					<p>He’d done it all with the same low-cost, low-maintenance tools you have available for your marketing.</p>

					<p><strong>The difference is, he had an actual Step-by-Step Plan to use those tools in the <em>right way</em>.</strong></p>
					
					<p><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sales/lab/steps.png" width="850" height="170" alt="Steps" class="noborder"></p>
					
					<h3 style="font-size:30px;">That experience put me on quest — to find out how he’d broken all the big-money-owns-publishing rules, and had still launched a bestseller.</h3>

					<p><strong>I had just three questions:</strong></p>
					
					<img class="alignright noborder" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sales/lab/icon-exclaim.png" width="128" height="128" alt="Icon Exclaim">
					<ul>
						<li>Was it a fluke? Did Ramit just get lucky?</li>
						<li>If not, then WTF did he do, to pull this off?</li>
						<li>And finally, <em>could it work for other authors</em>?</li>
					</ul>

					<p><strong>That was 7 years ago, and I can now say without a doubt that:</strong></p>
					
					<img class="alignright noborder" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sales/lab/icon-check2.png" width="128" height="128" alt="Icon check2">
					<ul>
						<li>No, it was <em>not</em> a fluke.</li>
						<li>I figured out what he did, and </li>
						<li>I have absolutely seen those methods work great for other authors. And they have continued to work, again and again.</li>
					</ul>

					<p>Working with Ramit set me on the path to finding a solution to the book launch problem — one that can work for any author. </p>
					
					<p>And over the last 7 years, I’ve developed those methods into a proven, step-by-step system that works consistently and predictably.</p>
					
					<p>Here’s just a few of the results reached by clients who have hired me to plan their book launches:</p>

					<ul>
						<li>Daniel Pink’s <em>To Sell Is Human</em> hit #1 on the <em>New York Times</em>, <em>Wall Street Journal</em> and <em>Washington Post</em> bestseller lists -- all at the same time.</li>
						<li>Chip and Dan Heath’s book <em>Decisive</em> debuted at #2 on the <em>New York Times</em> bestseller list.</li>
						<li>Hugh Howey’s novel <em>Dust</em> debuted at #7 on the <em>New York Times</em> bestseller list.</li>
						<li>Charles Duhigg’s <em>The Power of Habit</em> debuted at #3 on the <em>New York Times</em> bestseller list.</li>
						<li>Sally Hogshead’s <em>How the World Sees You</em> debuted at #2 on the <em>New York Times</em> bestseller list</li>
					</ul>

					<p>But here’s what’s so great about all of this: These methods I cracked into don’t only work for big-time, already-established authors. </p>
					
					<p><strong>They also work for new authors trying to get a book off the ground for the first time — for example:</strong></p>
					
					<ul>
						<li>My book <em>Your First 1000 Copies</em> is a niche indie-published book that sold more than 10,000 copies in its first year.</li>
						<li>Michael Bunker’s novel <em>Pennsylvania</em> sold more than 80,000 copies last year — and he became a <em>USA Today</em> bestseller!</li>
					</ul>


					<h3>So now the question is, How do <em><strong>you</strong></em> get to that point?</h3>

					<p>First let’s find out why you’re not already having successful book launches.</p>
					
					<p>There’s tons of free information out there about book marketing and book launches. So why hasn’t it worked for you?</p>

					<p>You have the book. You can put it on Amazon, the most popular bookstore in the world, for free.</p>

					<p><strong>But...</strong></p>
					
					<ul>
						<li>Do you know how to find your audience?</li>
					
						<li>Do you know how to get them to buy?</li>
	
						<li>Do you know how to get them to buy now, instead of later?</li>
	
						<li>Do you know how to get other authors to help promote your book?</li>
	
						<li>Do you know how to leverage social media in ways that deliver book sales?</li>
					</ul>

					<p>Theoretically, you can find a lot of answers online and in book marketing books. So why isn’t that advice working for you?</p>
					
					<h3>There are 3 reasons why you haven’t found success yet:</h3>

					<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sales/lab/icon-check.png" width="48" height="48" alt="Icon Check" class="pushleft noborder">
					<p><strong>1. The unknown unknowns are killing you.</strong></p>
					
					<p>There are so many things that go into a successful launch that you don’t even <em>know</em> about. Sure, you might absorb a few good ideas on Twitter and Facebook, and maybe a blog post or two. But that’s just the tip of a huge iceberg. What you can see is just the beginning.</p>

					<p>Successful book launches are well planned and well timed to have the biggest impact possible. And if you miss any of the important pieces, the whole thing can fall apart.</p>

					<p>Trust me. I’ve <em>seen</em> them fall apart.</p>

					<p>And it’s always because of things we didn’t know that we didn’t know.</p>

					<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sales/lab/icon-check.png" width="48" height="48" alt="Icon Check" class="pushleft noborder">
					<p><strong>2. You have no start-to-finish, chronological plan.</strong></p>

					<p>Successful launches <strong>are not random events</strong>.</p>

					<p>Authors don’t throw together a few Facebook updates and blog posts the night before, then watch their rankings skyrocket the next day.</p>

					<p>There is a <em>proven plan</em>, enacted from start to finish, that gets their books selling fast.</p>

					<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sales/lab/icon-check.png" width="48" height="48" alt="Icon Check" class="pushleft noborder">
					<p><strong>3. You have no idea what to say.</strong></p>

					<p>Even if I dropped an email list of 50,000 fans and a Twitter following of 100,000 on you tomorrow, you’d probably be paralyzed, because you have no idea what to say to actually get people to buy.</p>

					<p>Writing the kind of content that sells is simple — but <em>hard</em>.</p>

					<p>You have to know what to say and when to say it, to get people to buy.</p>


					<p><strong>Once you have these three things in place, you’ll have a successful book launch.</strong></p>

					<p>Do you want to launch a book and know that it’s going to sell? That’s what these authors have in common:</p>

					<ul>
						<li>Hugh Howey</li>
						<li>Daniel Pink</li>
						<li>Tim Ferriss</li>
						<li>Ryan Holiday</li>
						<li>Michael Bunker</li>
						<li>Charles Duhigg</li>
						<li>Josh Kaufman</li>
						<li>Nathan Barry</li>
						<li>Charles Hoehn</li>
						<li>Gretchen Rubin</li>
						<li>Jeff Goins</li>
						<li>Steve Scott</li>
						<li>Joanna Penn</li>
					</ul>

					<p>Those are just a handful of the authors I’ve worked with, studied, or interviewed to figure out what works and what doesn’t.</p>
					
					<p>And I’ve taken every single thing I’ve learned and tested it in the real world, over and over, to see what actually works.</p>
					
					<p><strong>Here’s what authors say about my work:</strong></p>
					
					<div class="testimonial">
						<img class="alignright noborder" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sales/lab/head-penn.png" width="100" height="100" alt="Head Penn">
						<p>"Tim has a proven methodology for launching books to the bestseller lists, and works with traditionally published authors and indies like Hugh Howey. For creatives who need more structure in their launches, Tim's step-by-step method is super helpful!" <span>Joanna Penn, <em>New York Times</em> and <em>USA Today</em> bestselling author</span></p>
					</div>
					
					<div class="testimonial">
						<img class="alignright noborder" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sales/lab/head-pink.png" width="100" height="100" alt="Head Pink">
						<p>"Tim Grahl is fast becoming a legend, almost single-handedly changing the way authors around the world spread ideas and connect with readers." <span>Daniel Pink, #1 New York Times bestselling author of <em>To Sell Is Human</em></span></p>
					</div>
					
					<div class="testimonial">
						<img class="alignright noborder" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sales/lab/head-goins.png" width="100" height="100" alt="Head Goins">
						<p>"Tim knows his stuff — he’s the go-to guy I call EVERY time I’m about the launch a book. Listen to him. You won’t regret it." <span>Jeff Goins, bestselling author of <em>The Art of Work</em></span></p>
					</div>
					
					<div class="testimonial">
						<img class="alignright noborder" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sales/lab/head-slim.png" width="100" height="100" alt="Head Slim">
						<p>"I watched in awe this year as Tim Grahl had 5 clients on the New York Times bestseller list in the same week. There is no one I trust more to learn about book marketing." <span>Pamela Slim, bestselling author of <em>Body of Work</em> and <em>Escape From Cubicle Nation</em></span></p>
					</div>
					
					<div class="testimonial">
						<img class="alignright noborder" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sales/lab/head-burkus.png" width="100" height="100" alt="Head Burkus">
						<p>"When I launched my first book, we focused a lot of attention on publicity, and we got some amazing media (including interviews on national television). But the actual sales of the book didn’t match the popularity of the message. With Tim’s help, I’ve figured out what was missing and and designed a system that will draw attention, and sales, to my next book." <span>David Burkus, author of <em>The Myths of Creativity</em> and a contributor to <em>Forbes</em> &amp; <em>Harvard Business Review</em></span></p>
					</div>
					
					<div class="testimonial">
						<img class="alignright noborder" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sales/lab/head-sisson.png" width="100" height="100" alt="Head Sisson">
						<p>"I first met Tim right when I was in the process of launching my book and he gave me some invaluable last minute advice that I believe helped my ‘official’ launch be even more successful. Tim laser focused in on the advanced techniques I could use to leverage my community and fans and ensure my book was a bestseller. I still turn to him now for insights into what’s working for authors and how to continue the momentum I’ve created and that’s invaluable." <span>Natalie Sisson, bestselling author of <em>The Suitcase Entrepreneur</em></span></p>
					</div>
					
					<div class="testimonial">
						<img class="alignright noborder" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sales/lab/head-johns.png" width="100" height="100" alt="Head Johns">
						<p>"I've independently published fiction and non-fiction books, and I learned the hard way just how difficult marketing a book can be. As such, I was looking for information not only on how professionals market books, but on how they launch them to bestseller status. I was familiar with Tim Grahl from his other book marketing course, so when I heard he was releasing a new one, Launch a Bestseller, I signed up the first chance I got.<br /><br />I knew the course was going to be fantastic, but what Tim delivered blew me away.<br /><br />I'm currently in the process of preparing to launch my latest novel, and thanks to Tim's course, I have a step-by-step system that I'm implementing in order to ensure that, this time, my launch is a success. Tim has clarified what I need to do, and the timeline I need to do it in.<br /><br />His course has given me confidence in my marketing and provided a proven the system that I can follow.<br /><br />Tim has worked with some of the top authors out there. In Launch a Bestseller, he shares the techniques he uses to market best selling books, while offering the course at a price an independent author like myself can afford. I highly recommend this course."  <span>Kevin T. Johns, www.kevintjohns.com</span></p>
					</div>
					<div class="labintro">
						<h3>Introducing...</h3>

						<p><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sales/lab/LABsalespage.jpg" width="678" height="397" alt="Launch a Bestseller"></p>
					</div>

					<p><strong>Launch a Bestseller</strong> is the only program available that has been tried and tested over dozens of bestselling launches, across both fiction and nonfiction genres.</p>
					
					<p>As soon as you join, you’ll access more than 8 hours of video lessons that take you start-to-finish through creating your own book launch plan.</p>

					<p>You’ll learn the exact strategies, timing, tactics, and content to use to successfully launch your bestseller.</p>

					<p>This isn’t about selling a couple dozen extra books. You’re going to bust through that “250 copies” ceiling and launch a book that sells <em>thousands</em> of copies.</p>


					<h3>Why is Launch a Bestseller different?</h3>

					<p>There are hundreds of books, blog posts, podcasts, and other courses out there that offer book marketing plans.</p>

					<p>So what makes <strong>Launch a Bestseller</strong> matter?</p>

					<p><strong>There are 3 fundamental things that set Launch a Bestseller apart from the rest:</strong></p>
					
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sales/lab/icon-check.png" width="48" height="48" alt="Icon Check" class="pushleft noborder">
					<p><strong>1. It’s been tested in the Real World.</strong></p>

					<p>Most of book marketing advice is based on people’s assumptions and speculations about what will work. They read some books and talk to a few successful authors, and decide that now they’re an expert. </p>

					<p>Unfortunately, that’s why most of their advice doesn’t work, and you’ve probably wasted your own time and money trying that advice.</p>

					<p>The plan laid out in <strong>Launch a Bestseller</strong> is not my best guess on what will work. It’s a proven system that has worked over and over again. Every single piece of this program has been real-world tested on actual book launches. I don’t teach anything in this plan until I see proof that it will work — for any author.</p>

					<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sales/lab/icon-check.png" width="48" height="48" alt="Icon Check" class="pushleft noborder">
					<p><strong>2. It’s been tested across many authors and genres.</strong></p>

					<p>There are many authors out there sharing their methods for finding success. In most cases, they are working very hard to be helpful. And some of their advice may work for you. </p>

					<p>But the problem is, it’s only been tested once! It’s worked for that particular author — for their personality, their strengths and weaknesses, their genre, their writing, their connections. </p>

					<p>What are the odds that it’s going to work exactly the same for you?</p>

					<p>The system laid out in Launch a Bestseller has been proven to work for hundreds of authors from all walks of life, in all genres, of all ages. I’ve worked directly with almost 200 authors, and seen what works and what doesn’t, across a huge spectrum. </p>

					<p>When I built the system that became <strong>Launch a Bestseller</strong>, I threw out the methods that don’t work for everyone, and kept only what works reliably, for any author.</p>

					<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sales/lab/icon-check.png" width="48" height="48" alt="Icon Check" class="pushleft noborder">
					<p><strong>3. It’s been tested through years of launches.</strong></p>

					<p>The vast majority of book marketing advice is based on a very short window of success — tactics that are here today and gone tomorrow. Someone found a loophole in how things work and sold a bunch of books, but by the time you’re learning it, it doesn’t work anymore.</p>

					<p>Not so, with Launch a Bestseller! The system you’ll learn has not only worked over and over for years. </p>

					<p>It’s based on <strong>fundamental ideas in marketing and human psychology that have been proven</strong> over decades of research and solid results.</p>

					<p>This isn’t a collection of flash-in-the-pan tactics. It’s a start-to-finish system that is proven and reliable.</p>


					<h3>This is a customizable step-by-step plan that takes you from start to finish.</h3>

					<p>The <strong>Launch a Bestseller</strong> program will provide you with a plan you can follow step by step, to create your own launch.</p>

					<p>You are a not a one-size-fits-all author, so <strong>Launch a Bestseller</strong> is not a one-size-fits-all program.</p>

					<p>When you access Launch a Bestseller, you will take a short survey that will provide you with a <strong>custom roadmap that is just for your launch</strong>.</p>

					<p>Plus, you’ll have unlimited access to <strong>Launch a Bestseller</strong>, so you can create a new custom roadmap each time you launch a new book.</p>
					
					<h3>A peek inside of Launch a Bestseller</h3>
					
					<p><img class="noborder" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sales/lab/labscreens.png" width="800" height="451" alt="Labscreens"></p>

					<p>Inside of <strong>Launch a Bestseller</strong> you will find high-definition video lessons, downloadable worksheets and checklists, interviews with top experts, and your custom roadmaps, all available in mobile-friendly, easy-to-use format.</p>
					
					<p>Each of the modules is laid out to take you from Beginner to Expert, and to a Finished Plan. </p>

					<p>The lessons include additional downloads and resources, with space for personal note-taking, to help you build your final launch plan.</p>

					<p>From start to finish, I’ve designed <strong>Launch a Bestseller</strong> to be easy to use and fun to learn, so you can move quickly towards your goal of having a successful book launch.</p>

					<!--
					<h3>Build Your Custom Launch Roadmap</h3>

					<p>Every author is unique and every book is unique, which is why I’ve developed 4 methods for book launches.</p>

					<p><strong>The 4 Launch Plans are:</strong></p>
					
					<p><strong>1. The Long Game Launch</strong></p>

					<p>If this is your first book and you have <em>no</em> platform, then this is the Long Game Launch for you. </p>

					<p>I will walk you through how to use your book to build your platform over time, and not only sell a lot of copies of <em>this</em> book, but set you up to have a <em>big</em> launch for your next book.</p>

					<p>I provide step-by-step instructions on exactly how to connect with influencers who can help you promote your book. </p>

					<p>I also walk you through connecting with fans — making sure people can find you, connect with you long term, and buy <em>all</em> of your books.</p>

					<p>If your book is already out and sales are dropping, I’ll share what I did when sales for my book, <em>Your First 1000 Copies</em>, were falling fast after the initial launch.</p>

					<p>I was able to pull it out of a nose dive, and ended up selling more than 10,000 copies in the first year. <em>And it’s still selling every single day</em>!</p>

					<p><strong>2. The Influencer Launch</strong></p>

					<p>Do you have connections to influencers who are willing to help promote your book, but have no author platform of your own to work from?</p>

					<p>The Influencer Launch leverages those relationships. This is how many, many bestsellers are created.</p>

					<p>If you’re a well-known speaker or a business consultant with a book, but have never built your own platform, you can use the Influencer Launch to leverage those relationships to launch a successful book, <em>and</em> build your platform for your business and next book.</p>

					<p><strong>3. The List Launch</strong></p>

					<p>Are you directly connected to fans through your email list or social media? </p>

					<p>The List Launch is my step-by-step system to get your fans to buy your book now instead of later. </p>

					<p>I provide the exact strategy, tools, tactics, and copy I have used to put books at #1 on the <em>New York Times</em> bestseller list.</p>

					<p><strong>4. The Bestseller Launch</strong></p>

					<p>Have you built connections with your fans and influencers?</p>

					<p>This is the entire plan, from start to finish. </p>

					<p>I’ve talked to many authors who have worked really hard to build their platform, yet when it comes time to launch, they have no idea how to get their fans to buy and their friends to promote.</p>

					<p>This is the entire system, including everything you need to leverage your platform to put your book at the top of the bestsellers lists.</p>
					-->

					<h3>How it works:</h3>
					
					<p><iframe src="https://player.vimeo.com/video/154626097?color=000000&title=0&byline=0&portrait=0" width="571" height="321" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></p>

					<img class="alignright noborder" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sales/lab/icon-brain.png" width="128" height="128" alt="Icon Brain">
					<p><strong>FIRST: The Plan – Know What To Do</strong></p>
					
					<p>Based on the information you give me, I will provide you with the exact plan for your book launch, complete with checklists for what needs to be done.</p>

					<p>Don’t know how to find the right websites for promoting your book?</p>

					<p>I provide a step-by-step process for finding the right places.</p>

					<p>Don’t know how to drive pre-orders for your book?</p>

					<p>I show you how the biggest bestselling authors have used simple techniques to drive thousands of book sales, before their book is even published. I go as far as walking you through the technology, copy, and exact tactics to make it happen.</p>

					<p>This solves one of the biggest challenges you face with a book launch: Knowing what to do.</p>

					<img class="alignright noborder" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sales/lab/icon-watch.png" width="128" height="128" alt="Icon Watch">
					<p><strong>SECOND: The Timeline – Know When To Do It</strong></p>

					<p>It’s great to have a checklist of what needs to be done. But you also need to know when it needs to be done.</p>

					<p>I’ll provide you with a timeline that includes everything that needs to be done, and when, covering:</p>

					<ul>
						<li>6 to 9 months before launch</li>
						<li>3 to 6 months before launch</li>
						<li>2 months before launch</li>
						<li>1 month before launch</li>
						<li>Launch day</li>
						<li>Post-launch (this is ignored by most authors, but is extremely important for the success of your <em>next</em> book)</li>
					</ul>

					<img class="alignright noborder" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sales/lab/icon-map.png" width="128" height="128" alt="Icon Map">
					<p><strong>THIRD: The Roadmap – Your Step-by-Step How-to</strong></p>
					
					<p>There is a <strong>lot</strong> of content inside of <strong>Launch a Bestseller</strong>. It can be overwhelming to try to go through it all at once.</p>

					<p>So I provide custom road map based on your launch on how to go through the content to get the most out of it.</p>

					<img class="alignright noborder" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sales/lab/icon-airplane.png" width="128" height="128" alt="Icon Airplane">
					<p><strong>FOURTH: The Execution – Make It Happen! </strong></p>

					<p>Once you have your plan, built a timeline, and filled it in with the step-by-step plans, you are ready to go!</p>

					<p>Start at the beginning of your plan and work all the way through it.</p>

					<p>If you get stuck at any part of the process, take advantage of your unlimited access to the course material, and go back through the lessons. </p>

					<p>Or join me for the monthly Q&amp;A calls that will help you get back on track (more on that below).</p>

					<img class="alignright noborder" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sales/lab/icon-stat.png" width="128" height="128" alt="Icon Stat">
					<p><strong>FIFTH: Your Next Book</strong></p>

					<p>When your next book is ready to launch, you can reset your <strong>Launch a Bestseller</strong> program and build a new custom roadmap that is perfect for your new book. </p>

					<p>In fact, your ongoing, unlimited access allows you to build plans for an unlimited number of books.</p>
					
					
					<h3>Launch a Bestseller offers 5 modules and 50+ lessons with huge benefits — and huge results:</h3>
					
					<p><img class="noborder" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sales/lab/5screens.png" width="610" height="203" alt="5screens"></p>
					
					<ul>
						<li><strong>Make it easy to promote your book</strong>. Your step-by-step guide on building a professional Media Kit that will stand out to influencers — in a way that’s easy to manage and completely free.</li>
					
						<li><strong>Find the best influencers</strong>. I show you exactly how you can find the big name influencers who are perfect for helping you sell a lot of copies.</li>

						<li><strong>Craft the right pitch</strong>. Form letters and press releases don’t work any more. You have to know how to craft a pitch that will get a “Yes!” My simple steps will help you avoid the mistakes that tank 99% of author’s campaigns. I even give you the exact copy I have used, to huge success for myself and my clients.</li>

						<li><strong>Grow your network fast</strong>. How to turn one “Yes!” from an influencer into three more, then nine more influencers promoting your book. It’s simple, I give you the exact email content to use.</li>

						<li><strong>Get the best blurbs</strong>. Almost every author makes 3 major mistakes when asking for a blurb. I show you how to avoid these mistakes and give you the exact emails I send to authors to get big-name blurbs.</li>

						<li><strong>Watch me do it</strong>. Learn all the tactics, then watch over my shoulder as I go from finding influencers, to crafting the perfect pitch, to getting a “Yes!” and then turning that “Yes!” into three more.</li>

						<li><strong>Build the perfect book launch webpage</strong>. Almost every book launch page I see is missing key elements that drive sales. I’ll show you how to make sure you have a book launch page that will drive sales. I’ll even show you a fantastic tool that makes it easier than ever to build out your page.</li>

						<li><strong>Learn the hidden system to getting thousands of pre-orders</strong>. Like I mentioned, the vast majority of a successful launch campaign components are hidden from view. I’ll reveal the hidden parts and exactly how they work.</li>

						<li><strong>Save huge amounts of time through automation</strong>. No need to handle every single pre-order yet get. I have a start-to-finish method for automating your system. (Even some of the biggest authors don’t know how to do this!)</li>

						<li><strong>Technology made easy</strong>. If the idea of dealing with technology terrifies you, have no fear! I do complete walkthroughs on the hardest parts. And almost every action that I recommend uses tools that are absolutely free.</li>

						<li><strong>Leverage social media the right way</strong>. We’ve all done it — post constantly to our social media account, and watch as nothing happens. But there is an effective way to use social media. I’ll show you how.</li>

						<li><strong>Create shareable assets</strong>. Why won’t people share your book with their social media followers? I tell you why, then show you exactly how to create posts that people will be excited to share.</li>

						<li><strong>Launch teams</strong>. This where I see most authors wasting huge amounts of time. I’ll show you how to organize a launch team in a way that is effective and timesaving.</li>

						<li><strong>Keep track of what you learn</strong>. I built my own custom note-taking app into Launch a Bestseller. You can take notes as you go through the lessons, change them later, and print out your final custom download of all your notes. This makes your customized planning a snap!</li>
					</ul>
					
					<div class="bonuses">
						<h3 style="margin-top:.4em;">Exclusive Bonus</h3>

						<p><img class="noborder" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sales/lab/bonus-penn.png" alt="Joanna and Tim"></p>
						
						<h4 style="text-align:center;margin-bottom:.4em;">Launching a Fiction Bestseller</h4>
						
						<p>Joanna Penn recently hit the <strong><em>USA Today</em> bestseller list</strong> for the second time. She did this by following a systematic, repeatable system.</p>
						
						<p>In this exclusive bonus only available during this launch, Tim Grahl interviews Joanna to get the specific, behind-the-scenes, step-by-step process she used. This interview is currently not available anywhere else.</p>
						
						<p>Joanna and Tim discuss:</p>
						
						<ul>
							<li>The "Ad Stacking" Method</li>
							<li>How to use paid advertising successfully</li>
							<li>How to get picked for BookBub</li>
							<li>What a fiction author platform looks like</li>
							<li>How fiction is different from non-fiction launches</li>
							<li>Start-to-finish how Joanna launched a <em>USA Today</em> bestseller</li>
						</ul>

						<h3 style="margin-top:.4em;">Bonus Lessons Included</h3>
						
						<p><img class="noborder" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sales/lab/screens.png" width="750" height="192" alt="Screens"></p>

						<p>Your custom <strong>Launch a Bestseller</strong> plan will help you get all the crucial pieces in place. But questions will inevitably come up through the process and trust me, I’ve heard them all!</p>
					
						<p style="margin-bottom:0px;">In these bonus lessons, I take on both the common and obscure questions, answering areas such as:</p>
						
						
						<p><a name="coachabestseller">&nbsp;</a></p>
						<br /><br />
						<h4>Coach a Bestseller - $397 Value</h4>
						
						<img class="noborder center" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sales/lab/coachabestseller.png" width="300" height="185" alt="Binder">
						
						<p><br />David Burkus is the author of <em>The Myths of Creativity</em> and the upcoming book <em>Under New Management</em>.</p>
						
						<p>Listen in as I coach David through the entire Launch a Bestseller system. I answer his questions and give direct feedback on how he can set himself up to have a bestselling launch.</p>
						
						<p>Ever wonder what goes on behind-closed-doors when I do my coaching? Now you will know.</p>
						
						<p>Inside of <em>Coach a Bestseller</em> you receive:</p>
						<ul>
							<li>4+ hours of audio that are live recordings of my coaching sessions with David Burkus.</li>
							<li>Walkthroughs for each of the <em>Launch a Bestseller</em> modules.</li>
							<li>The exact email copy David is using for his book launch (PLUS, my feedback and changes to make it better).</li>
						</ul>
						
						<p><a class="topbuy">Make sure you join <em>Launch a Bestseller</em> now</a> because the <em>Coach a Bestseller</em> bonus expires in... <span class="puttime"></span>.</p>
					
						
						<h4>Fiction Success with Rob Dircks</h4>
						<p style="text-align:center;"><img class="noborder center" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sales/lab/fiction-success.png" alt="Fiction Success" /></p>
						</p>

						<p>Less than a year ago, Rob released his first self-published novel, <em>Where the Hell is Tesla?.</em> Now, almost a year later, he has sold 10,000 copies. How did he do this?
						</p>
						<p>In this exclusive interview, you get the behind-the-scenes on how this "nobody" author sold so many copies AND how he has set himself up for success with his next book.
						</p>
						<h4> The Magic 10k Number with Shawn Coyne</h4>
						</p>
						<p style="text-align:center;"><img class="noborder center" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sales/lab/10kmagic.png" alt="10k" /></p>
						<p>Shawn Coyne has been an editor for 25+ years. He has worked at many of the big publishing houses and now runs his own publisher. Through his experience publishing many long time bestselling books, he has found that 10,000 copies seems to be a magic number to set your book up to keep selling.
						</p>
						<p>In this interview, Shawn discusses exactly how he found this number, why it works, and how you can take advantage of a marketing strategy that has tripled the amount of successful books he releases.
						</p>
						<p>Along with this, I share the technical step-by-step to setup the system that Shawn describes.
						</p>
						<p>This is a bonus you won't want to miss.
						</p>
						<h4>Launch a Bestseller with Jeff Goins</h4>
						</p>
						<p style="text-align:center;"><img class="noborder center" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sales/lab/lablive.png" alt="Jeff Goins" /></p>
						<p>Last year, I worked with Jeff Goins to launch his book <em>The Art of Work</em><span class="redactor-invisible-space"> which became and instant bestseller. In this live training, Jeff and I will teach you the behind-the-scenes on his launch, what worked, what didn't, and what he is going to do for his next book.</span>
						</p>
							
						<p><strong>How Traditional Bestseller Lists <em>Really</em> Work</strong> </p>

						<p>If you think the <em>New York Times</em>, <em>Wall Street Journal</em>, <em>Washington Post</em>, <em>USA Today</em> and other traditional bestseller lists simply pick “whatever books are selling the most,” you couldn’t be more wrong. </p>

						<p>There is a <em>lot</em> of confusion around how these lists actually work, and I will help you understand the inner workings, including:</p>

						<ul>
							<li>The right timing — when your books need to sell</li>
							<li>Which books count, and which books don’t</li>
							<li>How the <em>New York Times</em> bestseller list is different from all the others</li>
							<li>The number of sales it takes to hit the list</li>
							<li>When you should go for these lists, and when you shouldn’t</li>
						</ul>

						<p><strong>LinkedIn Influencer Connection</strong> </p>
				
						<p>LinkedIn is the only professional social media platform, and if you know how to use it right, you can build connections with influencers who are currently far beyond your reach.</p>

						<p><strong>The Survey Edge</strong> </p>

						<p>I’ll share two simple surveys that can ensure you are doing promotion the right way, and driving as many sales as possible.</p>

						<p><strong>Kill the Guesswork</strong> </p>

						<p>How many sales did that guest post drive? Did that email to my fans sell any books at all? Most of the time you have no idea. All you can see is one big sales number, and just guess at where those sales came from. </p>

						<p>I show you how to track your sales, so you know exactly what is working and what’s not.</p>

						<p><strong>Leverage Your Publisher</strong> </p>

						<p>All the things your publisher won’t tell you about what’s really going on behind-the-scenes. </p>

						<p>I share the how they decide who gets marketing support and funding, and who doesn’t. I also show you how you can move up that list — and how you can get your publisher to pay for your marketing expenses.</p>

						<p><strong>Success With a Publicist</strong> </p>

						<p>Should you hire a publicist? And if so, how can you make sure you’re not throwing your money down a black hole? I’ll walk you through each step of this process.</p>

						<p><strong>Book Trailers: The Pros, Cons, and How-tos</strong> </p>

						<p>I’ll give you exact criteria for judging whether or not you should do a book trailer for your particular book, and if so, how to do it right. I also show four examples of great trailers, and explain how you can do the same.</p>

						<p><strong>The Indie Secrets Training</strong></p>
					
						<p>This is a one-hour course I taught with Michael Bunker on the three-part launch system he used to sell more than 80,000 copies of his novel Pennsylvania.</p>
					
						<h3>Bonus Downloads<br /><span style="font-size:.8em;font-weight:800;">I’m Opening My Client Content Vault</span></h3>

						<img class="alignright noborder" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sales/lab/binder.png" width="436" height="361" alt="Binder">
						<p>Over nearly a decade of working with hundreds of authors and launching dozens of bestsellers, I’ve created a whole suite of templates, copy, PDFs, spreadsheets, and much more that I’ve only ever shared with clients who were paying me many thousands of dollars. </p>
					
						<p>This is the first time I’m sharing them all, in one program, including:</p>

						<ul>
							<li><strong>PDF Checklist</strong>. Your list that helps you keep track of what’s done and what still needs to be done, to ensure you never miss a crucial step.</li>
	
							<li><strong>PDF Timeline</strong>. Your custom checklist, laid out in chronological order so you know exactly when to do every action for your launch.</li>
	
							<li><strong>Outreach Emails</strong>. The exact emails I’ve used to get major coverage for my client’s books.</li>
	
							<li><strong>Outreach Template</strong>. The exact spreadsheet I use to organize and track a successful outreach campaign — this simplifies the entire process for you.</li>
	
							<li><strong>Technology Roadmap</strong>. There are thousands of tools out there for email marketing, automation, social media, outreach, etc. I show you exactly what I use, why I use it, and how you can use it too.</li>
	
							<li><strong>Blurb email copy</strong>. The exact emails to send, to request blurbs the right way. </li>
	
							<li><strong>Post-launch checklist</strong>. The template I use to deconstruct book launches after they’re over. This ensures the success of your <em>next</em> launch.</li>
						</ul>
					</div>


					<h3>Expert Interviews</h3>

					<p>I’ve brought in my friends and colleagues to add their knowledge, expertise, and experience to <strong>Launch a Bestseller</strong>:</p>
					
					<img class="alignright noborder" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sales/lab/head-hoehn.png" width="144" height="144" alt="Head Hoehn">
					<p><strong>Charlie Hoehn</strong> - Author of <em>Recession Proof Graduate</em>. I first met Charlie when we worked together on Ramit Sethi’s book launch (see above). He went on to work for Tim Ferriss as his Director of Special Projects, and helped launch Ferriss’s major bestselling books. He’s seen the behind-the-scenes on many huge bestselling launches, and will share the major lessons he’s learned.</p>

					<img class="alignright noborder" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sales/lab/head-kaufman.png" width="144" height="144" alt="Head Kaufman">
					<p><strong>Josh Kaufman</strong> - Author of <em>The Personal MBA</em> and <em>The First 20 Hours</em>. The king of automating book sales, Josh has built a system for his books that have kept them selling year after year. Now he’s breaking away from traditional publishing, so he can be free to go all-in as an indie publisher. Learn his techniques for systematizing book sales so they keep on selling.</p>

					<img class="alignright noborder" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sales/lab/head-barry.png" width="144" height="144" alt="Head Barry">
					<p><strong>Nathan Barry</strong> - Author of <em>Authority</em>, Nathan self-publishes his books, and doesn’t make them available on Amazon or any other online retailer. Oh — and he charges four times more than other books in his space, and has seen huge success as an author. How does he do it? He shares his methods in this interview.</p>

					<img class="alignright noborder" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sales/lab/head-ducker.png" width="144" height="144" alt="Head Ducker">
					<p><strong>Chris Ducker</strong> - Author of <em>Virtual Freedom</em>, Chris will speak on the fact that book launches have a ton of moving pieces. Many people bring in an assistant or friend to help, which only ends up causing more stress. </p>

					<p>Chris is the expert on how to work with assistants the right way, and save a lot of time in the process. He’ll share the steps you can take to get help during your launch  — help that actually helps.</p>

					<h3>Where are you going next?</h3>

					<p><iframe src="https://player.vimeo.com/video/154625374?color=000000&title=0&byline=0&portrait=0" width="571" height="321" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></p>

					<p>Now is the time to make a decision on what is next for you as a writer.</p>
					
					<p>You can keep on doing what you’ve been doing, and getting the same results. Or you can take the shorter, easier route.</p>

					<p>You can keep hammering away, making mistake after mistake, hoping to one day figure it out on your own. </p>

					<p>Or you can learn from my 8 years of experience, of making my own mistakes and learning what works and what doesn’t, then helping more than 100 authors succeed — and save yourself years of wasted time and thousands of wasted dollars.</p>

					<p>To reach your goals of being a successful author, you’re going to have to step out and do something different.</p>


					<div class="gaurantee">
						<h3 style="margin-top:.4em;">When you join Launch a Bestseller, you get my personal guarantee.</h3>

						<p><h4>You have my 60-day, full-course access,<br />100% money-back guarantee.</h4></p>
						
						<div style="text-align:center;"><p><img class="noborder" style="display:inline;" src="<?php echo get_stylesheet_directory_uri(); ?>/img/sales/lab/icon-guarantee.png" width="128" height="128" alt="Icon Guarantee"></p></div>
						
						<p>The short version: If you don’t love Launch a Bestseller, then I want to give you your money back.</p>

						<p>The reason I can give such a strong guarantee is because I know the Launch a Bestseller system works.</p>

						<p>It isn’t something I dreamed up one day and started selling as a nice idea.</p>

						<p>This is the exact step-by-step system I’ve used over and over to put books at the top of the bestseller lists. I’ve seen it work in the real world, with real books, so I know it can work for your books.</p>

						<p>But if you go through Launch a Bestseller and are unhappy with it within the first 60 days, simply send me an email, and I’ll immediately refund  your money.</p>

						<p>The only thing I’ll ask of you, is some feedback on why you’re asking for the refund, so I can make the program even more applicable to more authors.</p>
					</div>


					<h3>There are 3 ways to take advantage of<br />Launch a Bestseller.</h3>

					<p style="text-align:center;"><strong>Which is right for you?</strong></p>
					
					<?php include(locate_template( 'page-sales-tiers.php' )); ?>

					<h3>Frequently Asked Questions</h3>

					<p><strong>Q: What if I’m just getting started? Or have no fans? Or my book is already out?</strong></p>
					
					<p><strong>A:</strong> This is the situation most authors find themselves in. Every author is brand new at some point! </p>

					<p>In Launch a Bestseller, I outline the Long Game launch that I have helped authors use to make their first book a success, while setting themselves up to have huge success for their next book.</p>


					<p><strong>Q: Is this another of the same-old “Get your book to #1 in some obscure Amazon category so you can call yourself a bestseller” scam?</strong></p>

					<p><strong>A:</strong> Absolutely not. I’m not into fake accolades that don’t mean anything. My focus is on selling books. Launch a Bestseller will help you craft a plan that will sell a lot of books and make you a legitimate bestseller.</p>


					<p><strong>Q: Can I be successful with this program?</strong></p>

					<p><strong>A:</strong> First, we have to define the word success.</p>

					<p>If you’re hoping to a find a silver bullet that requires very little work on your part and still guarantees a New York Times bestseller, then you will absolutely not be successful with Launch a Bestseller.</p>

					<p>However, if you are looking for a system that has a proven track record of launching bestsellers, and you’re ready to put the work in, then yes — Launch a Bestseller will help make your book a success!</p>
					
					<p><strong>Q: How does the course work?</strong></p>
					
					<p><strong>A:</strong> The course is built on a customized system designed to make the course easy and fun to take. The 50+ lessons are based on a video formats that are also downloadable in MP3 audio. Additionally, the email templates, checklists, and spreadsheets are downloadable for you to use. The site is also optimized for mobile.</p>
					
					<p><strong>Q: How long do I have access to the course?</strong></p>
					
					<p><strong>A:</strong> You have unlimited, lifetime access to this version of the course. You can retake the course as many times as you'd like for additional book launches. All the material will be continuously updated.</p>
					
					<p><strong>Q: How much time does the course take?</strong></p>
					
					<p><strong>A:</strong> The course is designed to take at your own pace. If you have a book launch around the corner, you can go through it quickly. If you have time to plan, you can go through it at a slower pace. I recommend setting aside at least 2-3 hours a week to work through material and make your personal launch plan.</p>
					
					<p><strong>Q: Do you guarantee I'll have a New York Times bestselling book?</strong></p>
					
					<p><strong>A:</strong> No one can guarantee that you will have a top bestselling book. I do not even guarantee my clients that they will reach New York Times bestselling status, yet they still pay me over five figures to consult on their launch. I can guarantee if you work through the content and put it into practice, you will see huge progress towards your goals of having a top bestselling book.</p>
					
					<hr />
					
					<?php include(locate_template( 'page-sales-tiers.php' )); ?>
				</div>
			</div>
		</article>


		<section class="footer">
			<div class="wrapper">
				<div>
					<div class="sitemap">
						&nbsp;
					</div>
					<div class="training">
						&nbsp;
					</div>
					<div class="totop">
						<h4 class="scrollToTop">Back to Top</h4>
						<p>© Common Insights - Tim Grahl</p>
						<img src="<?php bloginfo('template_directory'); ?>/img/icon-backtotop.png" class="scrollToTop" width="46" height="46" alt="Icon Backtotop">
					</div>
				</div>
			</div>		
		</section>
		<script type="text/javascript">
		jQuery(document).ready(function(){
			!function(){var analytics=window.analytics=window.analytics||[];if(analytics.invoked)window.console&&console.error&&console.error("Segment snippet included twice.");else{analytics.invoked=!0;analytics.methods=["trackSubmit","trackClick","trackLink","trackForm","pageview","identify","group","track","ready","alias","page","once","off","on"];analytics.factory=function(t){return function(){var e=Array.prototype.slice.call(arguments);e.unshift(t);analytics.push(e);return analytics}};for(var t=0;t<analytics.methods.length;t++){var e=analytics.methods[t];analytics[e]=analytics.factory(e)}analytics.load=function(t){var e=document.createElement("script");e.type="text/javascript";e.async=!0;e.src=("https:"===document.location.protocol?"https://":"http://")+"cdn.segment.com/analytics.js/v1/"+t+"/analytics.min.js";var n=document.getElementsByTagName("script")[0];n.parentNode.insertBefore(e,n)};analytics.SNIPPET_VERSION="3.0.0";
				window.analytics.load("SjbpZGMQ3N1bl7jgOpRxsjl5etkdZNGn");
			window.analytics.page();
			  }}();
		  }
		</script>
		<script src="<?php bloginfo('template_directory'); ?>/js/jquery.fitvids.js" type="text/javascript" charset="utf-8"></script>
		<script src="<?php bloginfo('template_directory'); ?>/js/jquery.countdown.min.js" type="text/javascript" charset="utf-8"></script>

  		<script src="<?php bloginfo('template_directory'); ?>/js/moment-with-locales.min.js" type="text/javascript" charset="utf-8"></script>
  		<script src="<?php bloginfo('template_directory'); ?>/js/moment-timezone-with-data-2010-2020.min.js" type="text/javascript" charset="utf-8"></script>
		<script type="text/javascript">
		var nextYear = moment.tz("<? echo $counterdowner; ?> 23:59", "US/Pacific");
		jQuery(".puttime").countdown(nextYear.toDate(), function(event) {
			jQuery(this).text(event.strftime('%-D day%!D, %-H hour%!H, %-M minute%!M, and %-S second%!S'));
		});
		jQuery(document).scroll(function() {
		  var y = jQuery(this).scrollTop();
		  if (y > 4000) {
		    jQuery('.showbutton').fadeIn();
		  } else {
		    jQuery('.showbutton').fadeOut();
		  }
		});
		
		jQuery(".uaplearnmore").click(function(e) { 
			jQuery('.uaplearnbox').fadeIn();
		});

		jQuery(".topbuy").click(function(e) { 
			jQuery('html, body').animate({scrollTop : jQuery("#toptier").offset().top-120},800);
		});
		jQuery(document).ready(function(){
		// Target your #container, #wrapper etc.
		    jQuery(".salespage").fitVids();
		});
		</script>
		


			<script type="text/javascript">
			jQuery(document).ready(function(){
				//Click event to scroll to top
				jQuery('.scrollToTop').click(function(){
					jQuery('html, body').animate({scrollTop : 0},800);
					return false;
				});
				jQuery('.salespage').css("margin-top",jQuery(".fixedhead").outerHeight());
			});
			</script>
		</body>
		</html>
