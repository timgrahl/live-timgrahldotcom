<div class="tierswrapper">

	<div class="tiers" id="toptier">
		<h3>The Productive Writer</h3>
		<p>When you join <strong>The Productive Writer</strong> you get access to the full course including:</p>
		<ul>
			<li><strong>Everything you need to finish your book in the next 90 days.</strong></li>
			<li><strong>30+ lessons HD video and downloadable audio</strong>. All accessible in our easy-to-use, fully mobile optimized custom learning platform.</li>
			<li><strong>6 weeks of daily emails</strong>. Includes the email, video lesson, and daily application to get started right away.</li>
			<li><strong>Step-by-step checklists and worksheets</strong>. Customized to your productivity plan, we give you a walkthrough on every thing that needs to be done and when it needs to be done.</li>
			<li><strong>All of the Bonus Lessons</strong>. Specific bonus lessons that will help you finish your book.</li>
			<li><strong>Lifetime Access</strong>. You have unlimited access to all of the material to take at your own pace and reference long into the future.</li>
			<li><strike><strong>8-Week Group Support</strong>. A Private Facebook group dedicated to support and discussion for the complete duration of the course.</strike></li>
			<li><strike><strong>Weekly Live Q&amp;A Calls with Jeff and Tim</strong>. Ask your questions and get direct feedback during the course so you get the most out of building your productive writer system.</strike></li>
		</ul>
		<div style="text-align:center;">
			<h3>$197</h3>
			<p><strong>The Productive Writer closes in...</strong><br /><span class="puttime"></span></p>
			<p><a href="https://timgrahl.samcart.com/products/productive-writer" class="btn-blue">Get Instant Access Now</a></p>
			<p><img class="noborder" style="display:inline;" src="<?php bloginfo('template_directory'); ?>/img/sales/lab/ccs.png" width="175" height="36" alt="Ccs"></p>
			<p><a href="https://timgrahl.samcart.com/products/productive-writer---payment-plan">or, $57/month for 4 payments</a></p>
		</div>
	</div>
	<div class="tiers" id="toptier">
		<h3>The Productive Writer<br />VIP Access</h3>
		<p>When you join <strong>The Productive Writer</strong> you get access to the full course including:</p>
		<ul>
			<li><strong>Everything you need to finish your book in the next 90 days.</strong></li>
			<li><strong>30+ lessons HD video and downloadable audio</strong>. All accessible in our easy-to-use, fully mobile optimized custom learning platform.</li>
			<li><strong>6 weeks of daily emails</strong>. Includes the email, video lesson, and daily application to get started right away.</li>
			<li><strong>Step-by-step checklists and worksheets</strong>. Customized to your productivity plan, we give you a walkthrough on every thing that needs to be done and when it needs to be done.</li>
			<li><strong>All of the Bonus Lessons</strong>. Specific bonus lessons that will help you finish your book.</li>
			<li><strong>Lifetime Access</strong>. You have unlimited access to all of the material to take at your own pace and reference long into the future.</li>
			<li><strong>8-Week Group Support</strong>. A Private Facebook Group dedicated to support and discussion for the complete duration of the course.</li>
			<li><strong>Weekly Live Q&amp;A Calls with Jeff and Tim</strong>. Ask your questions and get direct feedback during the course so you get the most out of building your productive writer system.</li>
		</ul>
		<div style="text-align:center;">
			<h3>$497</h3>
			<p><strong>The Productive Writer closes in...</strong><br /><span class="puttime"></span></p>
			<p><a href="https://timgrahl.samcart.com/products/productive-writer-vip-access" class="btn-blue">Get Instant Access Now</a></p>
			<p><img class="noborder" style="display:inline;" src="<?php bloginfo('template_directory'); ?>/img/sales/lab/ccs.png" width="175" height="36" alt="Ccs"></p>
			<p><a href="https://timgrahl.samcart.com/products/productive-writer---plan-vip-access">or, $137/month for 4 payments</a></p>
		</div>
	</div>
</div>