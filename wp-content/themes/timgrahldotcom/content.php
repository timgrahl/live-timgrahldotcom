<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="wrapper">
		<?php the_title( sprintf( '<h1 class="entry-title">', esc_url( get_permalink() ) ), '</h1>' ); ?>
		
		<div class="vcard author post-author"><p>by <span class="fn"><?php the_author(); ?></span></p></div>

		<div class="entry">

			<?php if(get_post_meta($post->ID, 'nobanners', true) != 'true') : get_template_part( 'partials/banner', 'header' ); endif; ?>

		<?php
			/* translators: %s: Name of current post */
			the_content( sprintf(
				__( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'timgrahldotcom' ),
				the_title( '<span class="screen-reader-text">"', '"</span>', false )
			) );
		?>

			<?php if(get_post_meta($post->ID, 'nobanners', true) != 'true') : get_template_part( 'partials/banner', 'footer' ); endif; ?>
		
		<?php if(is_single()) {
			$thecat = 'tips';
			if(in_category('news')) {
				$thecat = 'news';
			}
			$shareurl = 'http://outthinkgroup.com/'.$thecat.'/'.$post->post_name;
			?>
			<div class="readercomments">
			<?php
			echo do_shortcode('[fbcomments url="'.$shareurl.'" count="on" countmsg="comments"]');
			?>
			</div>
			<?php
		 } 
		 ?>
		 <?php if(is_single()) { ?>
		 <div class="post-date updated"><p><?php the_date(); ?></p></div>
		 <?php } ?>
		</div>
	</div>
</article>