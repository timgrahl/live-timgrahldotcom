<?php
/**
 * Template Name: Conquering Fear
 *
 */

get_header(); ?>
		<?php while ( have_posts() ) : the_post(); ?>
		<article class="conqfear">
			<div class="headwrap1">
				<img src="<?php bloginfo('template_directory'); ?>/img/logo-conqfear.png" alt="Conquering Fear" />
			</div>
			<div class="headwrap2">
				<?php the_title( sprintf( '<h1 class="entry-title">', esc_url( get_permalink() ) ), '</h1>' ); ?>
			</div>
			<div class="wrapper">

				<div class="entry">
				<?php
					the_content();
				?>

				<br />
				<div class="feature-list">
					<h2>Want the full course?</h2>
					<p>In the Conquering Fear course, you'll learn:</p>
					<ul>
					 	<li>The true definition of fear.</li>
						<li>How to identify where fear is holding you back.</li>
						<li>Step-by-Step process to overcome fear and put out your greatest work.</li>
						<li>The two ways top performers conquer their fear.</li>
						<li>The perspective that will change everything.</li>
						<li>The one question that changed my life forever.</li>
					</ul>
					<p class="ta-center"><strong>And it's 100% Free!</strong></p>
					<p class="text-center"><button class="greybutton"><a href="https://my.leadpages.net/leadbox/142fa29f3f72a2%3A13df66b56b46dc/5742571661819904/" target="_blank" style="color:#fff">Access Conquering Fear</a></button><script data-leadbox="142fa29f3f72a2:13df66b56b46dc" data-url="https://my.leadpages.net/leadbox/142fa29f3f72a2%3A13df66b56b46dc/5742571661819904/" data-config="%7B%7D" type="text/javascript" src="//my.leadpages.net/leadbox-831.js"></script></p>
				</div><!--end feature-list -->
				<hr class="clr" />
				<?php echo do_shortcode('[fbcomments]'); ?>
				</div>
			</div>
		</article>
		<?php endwhile; // end of the loop. ?>
<?php get_footer(); ?>
