<?php
/**
 * Template Name: Hacking Amazon
 *
 */

get_header(); ?>
		<?php while ( have_posts() ) : the_post(); ?>
		<article class="hackamz">
			<div class="headwrap1">
				<img src="<?php bloginfo('template_directory'); ?>/img/logo-hacking-amazon.png" alt="Hacking Amazon" />
			</div>
			<div class="headwrap2">
				<?php the_title( sprintf( '<h1 class="entry-title">', esc_url( get_permalink() ) ), '</h1>' ); ?>
			</div>
			<div class="wrapper">

				<div class="entry">
				<?php
					the_content();
				?>

				<br />
				<!--
				<div class="feature-list">
					<h2>Want the full course?</h2>
					<p>In the Hacking Amazon course, you'll learn:</p>
					<ul>
						<li class="pdf">Guarantee a #1 Category Bestseller</li>

						<li class="mic">Launch with 25+ Customer Reviews</li>

						<li class="pdf">Get 1-Star Reviews Removed</li>

						<li class="plus">Much, much more!</li>
					</ul>
					<p class="ta-center"><strong>And it's 100% Free!</strong></p>
					<p class="text-center"><button class="greybutton"><a href="https://my.leadpages.net/leadbox/145c10c73f72a2%3A13df66b56b46dc/5642906408845312/" target="_blank" style="color:#fff">Access Hacking Amazon</a></button><script data-leadbox="145c10c73f72a2:13df66b56b46dc" data-url="https://my.leadpages.net/leadbox/145c10c73f72a2%3A13df66b56b46dc/5642906408845312/" data-config="%7B%7D" type="text/javascript" src="//my.leadpages.net/leadbox-810.js"></script></p>
					
				</div>
				-->
				<hr class="clr" />
				<?php echo do_shortcode('[fbcomments]'); ?>
				</div>
			</div>
		</article>
		<?php endwhile; // end of the loop. ?>
<?php get_footer(); ?>
