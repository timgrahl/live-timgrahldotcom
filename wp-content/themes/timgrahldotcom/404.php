<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @package Tim Grahl Dot Com
 */

get_header(); ?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="wrapper">
		<h1 class="page-title"><?php _e( 'Oops! That page can&rsquo;t be found.', 'timgrahldotcom' ); ?></h1>

		<div class="entry">
		
		<p>I've been notified of the error, but for now sign up for the email list, search for something else or chece out one of my popular articles.</p> 
			
		<div class="newsletterbox">
			<h2>Latest Articles</h2>
			<p>Every week I send out new articles with the latest book marketing tactics and strategies that bestselling authors are using.</p>
			<form action="http://timgrahl.com/thanks" target="_blank" novalidate>
				<input class="emailaddy" name="EMAIL" type="text" placeholder="your email address here" />
				<button type="submit">Sign Me Up!</button>
			</form>
		</div>
		<p>&nbsp;</p>
		<?php get_search_form(); ?>
		
		<h2>Most Popular Articles</h2>
		<?php
		$pop_posts = new WP_Query(array(
			'category_name' => 'popular',
			'posts_per_page'	=> '-1'
			));
		if ($pop_posts->have_posts()) : 
			?>
			<ol>
			<?php
			while ($pop_posts->have_posts()) : $pop_posts-> the_post();
			?>
				<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
			<?php
			endwhile;
			?>
			</ol>
			<?php
		endif;
		?>
		
		</div>
	</div>
</article>

<?php get_footer(); ?>
