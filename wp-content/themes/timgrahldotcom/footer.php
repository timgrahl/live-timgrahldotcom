
		<?php 
		if ( !is_page_template( 'page-sales.php' ) ) {
			get_template_part( 'footer-nav' );
		}
		?>
	</body>
	<script type="text/javascript">
	if(!jQuery.cookie('tgemail')) {
		analytics.identify('<?php echo session_id(); ?>');
	} else {
		analytics.identify(jQuery.cookie('tgemail'));
	}
	</script>
	<script src="//load.sumome.com/" data-sumo-site-id="3ca5e11df1e04c01a48166aa86989d0330521927aa3d3625ee74d2c7132361d9" async="async"></script>
	<?php wp_footer(); ?>
	<script type="text/javascript">
	jQuery(document).ready(function(){
		//Click event to scroll to top
		jQuery('.scrollToTop').click(function(){
			jQuery('html, body').animate({scrollTop : 0},800);
			return false;
		});
	});
	</script>

	<script type="text/javascript">
		(function() {
			window._pa = window._pa || {};
			// _pa.orderId = "myOrderId"; // OPTIONAL: attach unique conversion identifier to conversions
			// _pa.revenue = "19.99"; // OPTIONAL: attach dynamic purchase values to conversions
			// _pa.productId = "myProductId"; // OPTIONAL: Include product ID for use with dynamic ads
			var pa = document.createElement('script'); pa.type = 'text/javascript'; pa.async = true;
			pa.src = ('https:' == document.location.protocol ? 'https:' : 'http:') + "//tag.marinsm.com/serve/573d6ab6d311543ea20000a7.js";
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(pa, s);
		})();
	</script>
	
</body>
</html>