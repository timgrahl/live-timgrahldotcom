<?php
/**
 * Template Name: Prod Writer Sales Page Closed
 */
$thesiteurl = get_site_url();
?>

<!DOCTYPE html>
<html lang="en-US"
 xmlns:fb="http://ogp.me/ns/fb#">
    <head>
        <meta charset="UTF-8" />
        <title>Launch a Bestseller | Tim Grahl</title>
        <link rel="profile" href="http://gmpg.org/xfn/11" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="google-site-verification" content="inL2BzG2ZTKEYL75v4rHe0jAuxS6Mu4LifWV-HjpS7M" />
		<!-- FAVICON STUFF -->
		<link rel="apple-touch-icon" sizes="57x57" href="<?php echo $thesiteurl; ?>/wp-content/themes/timgrahldotcom/img/favicon/apple-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="<?php echo $thesiteurl; ?>/wp-content/themes/timgrahldotcom/img/favicon/apple-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="<?php echo $thesiteurl; ?>/wp-content/themes/timgrahldotcom/img/favicon/apple-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="<?php echo $thesiteurl; ?>/wp-content/themes/timgrahldotcom/img/favicon/apple-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="<?php echo $thesiteurl; ?>/wp-content/themes/timgrahldotcom/img/favicon/apple-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="<?php echo $thesiteurl; ?>/wp-content/themes/timgrahldotcom/img/favicon/apple-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="<?php echo $thesiteurl; ?>/wp-content/themes/timgrahldotcom/img/favicon/apple-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="<?php echo $thesiteurl; ?>/wp-content/themes/timgrahldotcom/img/favicon/apple-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="<?php echo $thesiteurl; ?>/wp-content/themes/timgrahldotcom/img/favicon/apple-icon-180x180.png">
		<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo $thesiteurl; ?>/wp-content/themes/timgrahldotcom/img/favicon/android-icon-192x192.png">
		<link rel="icon" type="image/png" sizes="32x32" href="<?php echo $thesiteurl; ?>/wp-content/themes/timgrahldotcom/img/favicon/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="96x96" href="<?php echo $thesiteurl; ?>/wp-content/themes/timgrahldotcom/img/favicon/favicon-96x96.png">
		<link rel="icon" type="image/png" sizes="16x16" href="<?php echo $thesiteurl; ?>/wp-content/themes/timgrahldotcom/img/favicon/favicon-16x16.png">
		<link rel="manifest" href="<?php echo $thesiteurl; ?>/wp-content/themes/timgrahldotcom/img/favicon/manifest.json">
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="<?php echo $thesiteurl; ?>/wp-content/themes/timgrahldotcom/img/favicon/ms-icon-144x144.png">
		<meta name="theme-color" content="#ffffff">
		<meta name="google-site-verification" content="GkYNHGMrRq8AH9s1SyoP2D_aeKwUE3EHptfMAr4nPqA" />
		<!-- END FAVICON STUFF -->
		
        <link rel="stylesheet" href="<?php echo $thesiteurl; ?>/wp-content/themes/timgrahldotcom/style.css?v=2" type="text/css" media="screen" />
		<link rel="stylesheet" href="<?php echo $thesiteurl; ?>/wp-content/themes/timgrahldotcom/print.css" type="text/css" media="print" />
        <link rel="pingback" href="<?php echo $thesiteurl; ?>/xmlrpc.php" />

				<script>
		(function(d) {
		  var tkTimeout=3000;
		  if(window.sessionStorage){if(sessionStorage.getItem('useTypekit')==='false'){tkTimeout=0;}}
		  var config = {
		    kitId: 'yul1bwz',
		    scriptTimeout: tkTimeout
		  },
		  h=d.documentElement,t=setTimeout(function(){h.className=h.className.replace(/\bwf-loading\b/g,"")+"wf-inactive";if(window.sessionStorage){sessionStorage.setItem("useTypekit","false")}},config.scriptTimeout),tk=d.createElement("script"),f=false,s=d.getElementsByTagName("script")[0],a;h.className+="wf-loading";tk.src='//use.typekit.net/'+config.kitId+'.js';tk.async=true;tk.onload=tk.onreadystatechange=function(){a=this.readyState;if(f||a&&a!="complete"&&a!="loaded")return;f=true;clearTimeout(t);try{Typekit.load(config)}catch(e){}};s.parentNode.insertBefore(tk,s)
		})(document);
		</script>
        
<!-- All in One SEO Pack 2.2.6.2 by Michael Torbert of Semper Fi Web Design[121,156] -->
<link rel="author" href="http://google.com/+TimGrahl23" />

<link rel="canonical" href="<?php echo $thesiteurl; ?>/programs/launch-a-bestseller/" />
<!-- /all in one seo pack -->
<link rel="alternate" type="application/rss+xml" title="Tim Grahl &raquo; Launch a Bestseller Comments Feed" href="<?php echo $thesiteurl; ?>/programs/launch-a-bestseller/feed/" />
		
<script type='text/javascript' src='<?php echo $thesiteurl; ?>/wp-includes/js/jquery/jquery.js?ver=1.11.2'></script>
<script type='text/javascript' src='<?php echo $thesiteurl; ?>/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.2.1'></script>

<meta property="fb:app_id" content="130357656259"/>	
		<script src="<?php echo $thesiteurl; ?>/wp-content/themes/timgrahldotcom/js/modernizr.custom.76227.js" type="text/javascript" charset="utf-8"></script>
		<script src="<?php echo $thesiteurl; ?>/wp-content/themes/timgrahldotcom/js/jquery.cookie.js" type="text/javascript" charset="utf-8"></script>
		<script src="<?php echo $thesiteurl; ?>/wp-content/themes/timgrahldotcom/js/min/includes-min.js" type="text/javascript" charset="utf-8"></script>
    </head>
	<body>
	<article id="post-<?php the_ID(); ?>" class="salespage lab-sales lab-closed">
			<div class="topper">
				<div class="tcontent">
					<h1>Finish Your Book in the Next 90 Days</h1>
					<h2>And create the writing habits for a lifetime.</h2>
					
					<!-- Begin MailChimp Signup Form -->
					<div id="mc_embed_signup">
						<div class="closebox">
							<ul>
								<li>How do you make writing an automatic habit?
								</li>
								<li>How do you break out of the negative mindsets that hold you back?
								</li>
								<li>Can you really fit your writing into a crazy life schedule?
								</li>
							</ul>
						</div>
						<div class="mcform">
							<form action="//timgrahl.us2.list-manage.com/subscribe/post?u=92b107dc9343f5f0c569d686c&amp;id=3b677df94f" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate="">
								<div id="mc_embed_signup_scroll">
									<div class="mc-field-group">
										<label for="mce-FNAME">First Name</label> <input type="text" value="" name="FNAME" class="" id="mce-FNAME">
									</div>
									<div class="mc-field-group">
										<label for="mce-EMAIL">Email Address</label> <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">
									</div>
									<div id="mce-responses" class="clear">
										<div class="response" id="mce-error-response" style="display:none"></div>
										<div class="response" id="mce-success-response" style="display:none"></div>
									</div><!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
									<div style="position: absolute; left: -5000px;">
										<input type="text" name="b_92b107dc9343f5f0c569d686c_78c878bcfd" tabindex="-1" value="">
									</div>
									<div class="clear">
										<input class="btn btn-orange" type="submit" value="TELL ME MORE" name="subscribe" id="mc-embedded-subscribe" class="button">
									</div>
								</div>
							</form>
						</div>
					</div>

				</div>
				<div style="clear:left">&nbsp;</div>
			</div>
			<div class="wrapper">
				<div class="entry">
					</div>

					<!--End mc_embed_signup-->
				</div>
			</div>
		</article>


		<section class="footer">
			<div class="wrapper">
				<div>
					<div class="sitemap">
						&nbsp;
					</div>
					<div class="training">
						&nbsp;
					</div>
					<div class="totop">
						<h4 class="scrollToTop">Back to Top</h4>
						<p>© Common Insights - Tim Grahl</p>
						<img src="<?php bloginfo('template_directory'); ?>/img/icon-backtotop.png" class="scrollToTop" width="46" height="46" alt="Icon Backtotop">
					</div>
				</div>
			</div>		
		</section>
		<script type="text/javascript">
		jQuery(document).ready(function(){
			!function(){var analytics=window.analytics=window.analytics||[];if(analytics.invoked)window.console&&console.error&&console.error("Segment snippet included twice.");else{analytics.invoked=!0;analytics.methods=["trackSubmit","trackClick","trackLink","trackForm","pageview","identify","group","track","ready","alias","page","once","off","on"];analytics.factory=function(t){return function(){var e=Array.prototype.slice.call(arguments);e.unshift(t);analytics.push(e);return analytics}};for(var t=0;t<analytics.methods.length;t++){var e=analytics.methods[t];analytics[e]=analytics.factory(e)}analytics.load=function(t){var e=document.createElement("script");e.type="text/javascript";e.async=!0;e.src=("https:"===document.location.protocol?"https://":"http://")+"cdn.segment.com/analytics.js/v1/"+t+"/analytics.min.js";var n=document.getElementsByTagName("script")[0];n.parentNode.insertBefore(e,n)};analytics.SNIPPET_VERSION="3.0.0";
				window.analytics.load("SjbpZGMQ3N1bl7jgOpRxsjl5etkdZNGn");
			window.analytics.page();
			  }}();
		  }
		</script>
		<script src="<?php bloginfo('template_directory'); ?>/js/jquery.fitvids.js" type="text/javascript" charset="utf-8"></script>
		<script src="<?php bloginfo('template_directory'); ?>/js/jquery.countdown.min.js" type="text/javascript" charset="utf-8"></script>
		<script type="text/javascript">
		jQuery(".puttime").countdown('2015/06/05 02:59:59', function(event) {
			jQuery(this).text(event.strftime('%-D day%!D, %-H hour%!H, %-M minute%!M, and %-S second%!S'));
		});
		jQuery(document).scroll(function() {
		  var y = jQuery(this).scrollTop();
		  if (y > 4000) {
		    jQuery('.showbutton').fadeIn();
		  } else {
		    jQuery('.showbutton').fadeOut();
		  }
		});

		jQuery(".topbuy").click(function(e) { 
			jQuery('html, body').animate({scrollTop : jQuery("#toptier").offset().top-120},800);
		});
		jQuery(document).ready(function(){
		// Target your #container, #wrapper etc.
		    jQuery(".salespage").fitVids();
		});
		</script>
		


			<script type="text/javascript">
			jQuery(document).ready(function(){
				//Click event to scroll to top
				jQuery('.scrollToTop').click(function(){
					jQuery('html, body').animate({scrollTop : 0},800);
					return false;
				});
				jQuery('.salespage').css("margin-top",jQuery(".fixedhead").outerHeight());
			});
			</script>
		</body>
		</html>
