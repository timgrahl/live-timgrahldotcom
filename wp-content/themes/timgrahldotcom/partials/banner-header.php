<!-- Banner: Header -->

<?php
if (!isset($_COOKIE["tgcookie"])) { 
?>
<div class="banner-box">
    <div class="banner-wrapper">
        <h3>Learn How to Sell More Books</h3>
        <div class="card" id="blog_banner_card_1">
            <div class="card__front">
                <p>Get my 7-day course on how to connect with readers, build your platform, and <strong>sell more books</strong>.</p>
                <button class="btn" onclick="jQuery('#blog_banner_card_1').addClass('flipped');">Get Free Access Now</button>
            </div>
            <div class="card__back animated fast slideInUp">
                <p>Just let me know where I should send this <strong>free</strong> course.</p>
                <script src="https://app.convertkit.com/assets/CKJS4.js?v=21"></script>
                <!--  Form starts here  -->
                <form id="ck_subscribe_form" class="ck_subscribe_form" action="https://app.convertkit.com/landing_pages/50531/subscribe" data-remote="true">
                    <input type="hidden" value="{&quot;form_style&quot;:&quot;naked&quot;,&quot;embed_style&quot;:&quot;inline&quot;,&quot;embed_trigger&quot;:&quot;scroll_percentage&quot;,&quot;scroll_percentage&quot;:&quot;70&quot;,&quot;delay_seconds&quot;:&quot;10&quot;,&quot;display_position&quot;:&quot;br&quot;,&quot;display_devices&quot;:&quot;all&quot;,&quot;days_no_show&quot;:&quot;15&quot;,&quot;converted_behavior&quot;:&quot;show&quot;}" id="ck_form_options">
                    <input type="hidden" name="id" value="50531" id="landing_page_id">
                    <div style="display: none"><input class="optIn ck_course_opted" name="course_opted" type="checkbox" id="optIn" checked /></div>

                    <div class="ck_errorArea">
                        <div id="ck_error_msg" style="display:none">
                            <p>There was an error submitting your subscription. Please try again.</p>
                        </div>
                    </div>
                    <div class="ck_control_group ck_email_field_group">
                        <label class="ck_label" for="ck_emailField" style="display: none">Email Address</label>
                        <input type="text" name="first_name" class="ck_first_name" id="ck_firstNameField" placeholder="First Name">
                        <input type="email" name="email" class="ck_email_address" id="ck_emailField" placeholder="Email Address" required>
                    </div>

                    <button class="subscribe_button ck_subscribe_button btn fields" id="ck_subscribe_button">
                        Get Free Access Now
                    </button>
                </form>
            </div>
        </div>

    </div>
    <img src="<?php bloginfo('template_directory'); ?>/img/7day.png" class="thumb" style="left: -10px; bottom: -30px;" />
</div>
<? } ?>