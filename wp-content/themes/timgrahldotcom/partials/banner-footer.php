<?php
// Adding in features for us to be able
// to use the custom optin magnets
// ---------------------------------------------

$bannerFooter = array(
    'form_id'   => get_field('custom_optin_magnet_form_id') ?: 50531,
    'title'     => get_field('custom_optin_magnet_title') ?: 'Now it\'s your turn',
    'body'      => get_field('custom_optin_magnet_body') ?: '<p>I\'d like to send you a FREE 7-day course on how to sell more books.
            Just let me know where I should send this <strong>free</strong> course.</p>',
    'image_url' => get_field('custom_optin_magnet_image') ?: get_bloginfo('template_directory') . '/img/bigasstim.png',
    'cta'       => get_field('custom_optin_magnet_cta') ?: 'Get Free Access Now',
    'file_id'   => get_field('custom_optin_magnet_file') ?: null,
);

$bannerFooter['action_url'] = get_field('use_custom_optin_magnet') ? '/manual-thanks/' : "https://app.convertkit.com/landing_pages/{$bannerFooter['form_id']}/subscribe"

?>
<div class="banner-box banner-footer">
    <h4 class="banner-intro">I've helped dozens of authors hit the NYT and WSJ Lists.</h4>
    <div class="banner-wrapper">
        <h3><?= $bannerFooter['title'] ?></h3>
        <?= $bannerFooter['body'] ?>
        <script src="https://app.convertkit.com/assets/CKJS4.js?v=21"></script>
        <!--  Form starts here  -->
        <form id="<?= get_field('use_custom_optin_magnet') ? '' : 'ck_subscribe_form' ?>" class="ck_subscribe_form" method="POST" action="<?= $bannerFooter['action_url'] ?>" data-remote="true">
            <input type="hidden" value="{&quot;form_style&quot;:&quot;naked&quot;,&quot;embed_style&quot;:&quot;inline&quot;,&quot;embed_trigger&quot;:&quot;scroll_percentage&quot;,&quot;scroll_percentage&quot;:&quot;70&quot;,&quot;delay_seconds&quot;:&quot;10&quot;,&quot;display_position&quot;:&quot;br&quot;,&quot;display_devices&quot;:&quot;all&quot;,&quot;days_no_show&quot;:&quot;15&quot;,&quot;converted_behavior&quot;:&quot;show&quot;}" id="ck_form_options">
            <input type="hidden" name="id" value="<?= $bannerFooter['form_id'] ?>" id="landing_page_id">
            <input type="hidden" name="file_id" value="<?= $bannerFooter['file_id'] ?>">
            <input type="hidden" name="prev_id" value="<?= get_the_ID() ?>">
            <div style="display: none"><input class="optIn ck_course_opted" name="course_opted" type="checkbox" id="optIn" checked /></div>

            <div class="ck_errorArea">
                <div id="ck_error_msg" style="display:none">
                    <p>There was an error submitting your subscription. Please try again.</p>
                </div>
            </div>
            <div class="ck_control_group ck_email_field_group">
                <label class="ck_label" for="ck_emailField" style="display: none">Email Address</label>
                <input type="text" name="first_name" class="ck_first_name" id="ck_firstNameField" placeholder="First Name">
                <input type="email" name="email" class="ck_email_address" id="ck_emailField" placeholder="Email Address" required>
            </div>

            <button class="subscribe_button ck_subscribe_button btn fields" id="ck_subscribe_button">
                <?= $bannerFooter['cta'] ?>
            </button>
        </form>
    </div>

    <?php if(get_field('use_custom_optin_magnet')): ?>
        <img src="<?= $bannerFooter['image_url'] ?>" class="thumb" style="left: 10px;top: 66px;width: 248px;" />
    <?php else: ?>
        <img src="<?= $bannerFooter['image_url'] ?>" class="thumb" style="left: -72px;bottom: -36px;width: 415px;" />
    <?php endif; ?>

</div>