<?php
/**
 * Template Name: Email Thanks
 *
 */
setcookie("tgcookie", "1", time() + (86400 * 365), "/"); // 86400 = 1 day
get_header(); ?>
		<?php while ( have_posts() ) : the_post(); ?>

			<?php get_template_part( 'content', 'single' ); ?>

		<?php endwhile; // end of the loop. ?>
<?php get_footer(); ?>
