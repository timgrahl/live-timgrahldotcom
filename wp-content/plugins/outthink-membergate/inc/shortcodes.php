<?php
	function otmg_login_form() {
	
		$otmg_settings = get_option('otmg_settings');
		ob_start(); ?>
		<form class="otmg_form otmg_login_form" method="post" accept-charset="utf-8">
			<?php
//			print_r($_GET]);
			$string = $_SERVER['QUERY_STRING'];
			parse_str($string, $obj);
//			print_r($obj);
			if($notice or $error) {
				$noticevars = explode(',',$notice);
				echo '<p class="notice">';
				foreach ($noticevars as $key => $value) {
					switch ($value) {
						case 'email_req':
							echo '<strong>Your email is requred</strong>';
							break;
						default:
							break;
					}
				}
				echo $error;
				echo '</p>';
			} // end check for errors and notices
			?>
			<label for="email">Email</label>
			<input type="text" name="email" placeholder="Email" value="<?php echo $email; ?>" id="email">
			<input class="btn" type="submit" value="Login">
			<input type="hidden" name="method" value="login" id="method">
		</form>
		<?php
		$return = ob_get_clean();
		return $return;
	}
	
	function otmg_register_form() {
		$otmg_settings = get_option('otmg_settings');
$action = plugin_dir_url(dirname(__FILE__)).'is_register_members.php';
		$return = '';
		ob_start(); ?>
		<form class="otmg_form otmg_register_form" action="" method="post" accept-charset="utf-8">
			<?php
//			print_r($_GET]);
			$string = $_SERVER['QUERY_STRING'];
			parse_str($string);
			if($notice or $error) {
				$noticevars = explode(',',$notice);
				echo '<p class="notice">';
				foreach ($noticevars as $key => $value) {
					switch ($value) {
						case 'email_req':
							echo '<strong>Your email is requred</strong>';
							break;
						default:
							break;
					}
				}
				echo $error;
				echo '</p>';
			} // end check for errors and notices
			?>
			<label for="fname">First Name</label>
			<input type="text" name="fname" placeholder="First Name" value="<?php echo $fname; ?>" id="first_name">
			<label for="email">Email</label>
			<input type="email" name="email" placeholder="Email"  value="<?php echo $email; ?>" id="email">
			<input type="submit" class="btn" value="Register Now!">
			<input type="hidden" name="method" value="register" id="method">
		</form>
		<?php
		$return .= ob_get_clean();
		return $return;
	}
	
	
// [otmg_login]
function otmg_login($atts) {
		extract(shortcode_atts(array(), $atts));
		// setting up membergate settings array
		return otmg_login_form();
}
add_shortcode("otmg_login", "otmg_login");
// end shortcode

// [otmg_register]
function otmg_register($atts) {
		extract(shortcode_atts(array(), $atts));
		return otmg_register_form();
}
add_shortcode("otmg_register", "otmg_register");
// end shortcode