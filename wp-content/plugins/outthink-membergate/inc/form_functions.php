<?php
function otmg_is_login_process($otmg_settings) {

	//build our application object
	$app = new iSDK;
	
	if ($app->cfgCon("OTMG_Plugin")) {
		$returnFields = array('Id', 'FirstName', 'LastName');
		/* process form data */
			$notice = '';
			// loading up the email field from form data
			$email = $_POST['email'];
			// checking to see if the email exists, if not, building a "notice" variable that will send back to the form.
			if ($email == 'Email' or empty($email)) {
				$notice .= 'email_req';
			}
			// if the email addresss has been filled in, $notice will be empty
			if (empty($notice)) {
				// Add a contact using the ContactService.add method
				$returnFields = array('Id', 'FirstName', 'LastName');
				$contact = $app->findByEmail($email, $returnFields);
//				var_dump($contact);
				if (is_string($contact)) {
					if (strpos($contact, 'ERROR') !== false) {
						$pos = 'true';
					} 
				}
				// if there is an error
				if( $pos ) {
					$fields = '?email='.$email;
					$string = $fields.'&error='.urlencode('There was an error connecting to the server. Please let the site owner know of the problem.');
					header('Location: '.get_permalink($otmg_settings['login_page']).$string);
				
				// otherwise, if $contact is not empty, we'll assume the contact is good, and redirect them to the protected page
				} elseif (!empty($contact)) {
					
					// The contact was in the system, so cookie and redirect
					setcookie("otmg_member", 'true', strtotime( '+30 days' ), '/');
					header('Location: '.get_permalink($otmg_settings['protected_page']));
				} else {
					
					// The email is not registered, so redirect them back to the login page, fill in the email field, and prompt them with a message
					$fields = '?email='.$email;
					$string = $fields.'&error='.urlencode('Your email is not registered. Register to sign in.');
					header('Location: '.get_permalink($otmg_settings['login_page']).$string);
				}
				
		// the following is triggered if the email is invalid (probably won't get past the browser validation)
		} else {
			// kick back the notice and have them fill in the email again.
			$string = '?notice='.$notice;
			header('Location: '.get_permalink($otmg_settings['login_page']).$string);
		}
	}
	
} // end otmg_is_login_process()

function otmg_mc_login_process($otmg_settings) {
	$apiKey = $otmg_settings['api_key'];
	$api = new MCAPI($apiKey);
	$listId = $otmg_settings['mc_list_id'];
	/* process form data */
		$notice = '';
		// loading up the email field from form data
		$email = $_POST['email'];
		// checking to see if the email exists, if not, building a "notice" variable that will send back to the form.
		if ($email == 'Email' or empty($email) or !is_email($email)) {
			$notice .= 'email_req';
		}
		// if the email addresss has been filled in, $notice will be empty
		if (empty($notice)) {
			// Add a contact using the ContactService.add method
			$retval = $api->listMemberInfo( $listId, array($email) );
//				print_r($retval);
//				echo $retval['success'];

			if ($retval['success'] == 1) {
				// The contact was in the system, so cookie and redirect
				setcookie("otmg_member", 'true', strtotime( '+180 days' ), '/');
				header('Location: '.get_permalink($otmg_settings['protected_page']));

			} elseif ($retval['success'] == 0){
				// error_log('the contact is NOT in the system, so we're gonna kick them back to the login page');
				$fields = '?email='.$email;
				$string = $fields.'&error='.urlencode('Your email is not registered. Register to sign in');
//				error_log( get_permalink($otmg_settings['login_page']) . $string);
				//wp_redirect( get_permalink($otmg_settings['login_page']).$string);
				header('Location:'.get_permalink($otmg_settings['login_page']).$string);

			} elseif($api->errorCode) {
				error_log('there\'s another error -- so kick the error back to the login page');
				$fields = '?email='.$email;
				$string = $fields.'&error='.urlencode($api->errorMessage);
				header('Location: '.get_permalink($otmg_settings['login_page']).$string);

			} else {
				// error_log('for some other very unknown reason, the form process failed...so...that happened');
				$fields = '?email='.$email;
				$string = $fields.'&error='.urlencode("There was an unexpected problem, perhaps try another email address?");
				header('Location: '.get_permalink($otmg_settings['login_page']).$string);
			}
		} else {
			// kick back the notice and have them fill in the email again.
			$string = '?notice='.$notice;
			header('Location: '.get_permalink($otmg_settings['login_page']).$string);
		}
}
function otmg_is_register($otmg_settings) {
	//build our application object
	$app = new iSDK;
	$fields = $_POST;
	$contactData = array(
		'Email' => $fields['email'],
		'FirstName' => $fields['fname'],
		'LastName' => $fields['lname'],
		'Company' => $fields['company'],
		'PostalCode' => $fields['zip'],				
		'Country' => $fields['country']
	);
	// setting up some variables to be empty so I can use them later on
	$errfields = ''; $field = ''; $fieldkeys = array(); $fieldvals = array();
	// building arrays of field keys and field values from above (to echo back to the page if there's an error)
	foreach ($fields as $key => $value) {
		$fieldkeys[] = $key;
		$fieldvals[] = $value;;
	}
	// setting a count variable to grab the corresponding fields from the above arrays
	$c = 0;
	// looping through contact data key/val to see if anything is empty
	foreach ($contactData as $contactkey => $contactvalue) {
		// building the string to pass back to the registration page so the visitor doesn't have to enter it all again.
		$field .= $fieldkeys[$c].'='.$fieldvals[$c].'&';
		// If the value of a field is empty, add the error text and list the required fields on the page
		if (empty($contactvalue)) {
			$error = 'Please correct the following fields: ';
			$errfields .= $contactkey.', ';
		}
		// count the key up one at a time.
		$c++;
	}
//	print_r($contactData);
	// build the string to spit back to the membership page
	$string = '?error='.$error.$errfields.'&'.$field;

	// if we're good and there wasn't an error, go ahead and add the person to the list -- if they're already on the list, update their information with what they give us here.
	if (empty($error)) {
		if ($app->cfgCon("OTMG_Plugin")) {
	//		
	//		var_dump($registerUser);
			$registerUser = $app->addWithDupCheck($contactData, 'Email');
			if (is_int($registerUser)) {
				$taggem = $app->grpAssign($registerUser, 713);
			}
			setcookie("otmg_member", 'true', strtotime( '+30 days' ), '/');
			header('Location: '.get_permalink($otmg_settings['protected_page']));
		}
	} else {
		header('Location: '.get_permalink($otmg_settings['login_page']).$string);
	}
	
}
function otmg_mc_register($otmg_settings) {
	//build our application object
	$apiKey = $otmg_settings['api_key'];
	$api = new MCAPI($apiKey);
	$listID = $otmg_settings['mc_list_id'];
	$fields = $_POST;
	
	$contactData = array(
		'EMAIL' => $fields['email'],
		'FNAME' => $fields['fname'],
	);
	$mergeFields = array(
		'FNAME' => $fields['fname'],
	);
	$email = $fields['email'];
	// setting up some variables to be empty so I can use them later on
	$errfields = ''; $field = ''; $fieldkeys = array(); $fieldvals = array();
	// building arrays of field keys and field values from above (to echo back to the page if there's an error)
	foreach ($fields as $key => $value) {
		$fieldkeys[] = $key;
		$fieldvals[] = $value;
	}
	// setting a count variable to grab the corresponding fields from the above arrays
	$c = 0;
	// looping through contact data key/val to see if anything is empty
	foreach ($contactData as $contactkey => $contactvalue) {
		// building the string to pass back to the registration page so the visitor doesn't have to enter it all again.
		$field .= $fieldkeys[$c].'='.$fieldvals[$c].'&';
		// If the value of a field is empty, add the error text and list the required fields on the page
		$c = 0;
		if (empty($contactvalue)) {
			$error = 'Please correct the following fields: ';
			if ($contactkey == "FNAME") {
				$contactkey = 'First Name';
			}
			$errfields .= $contactkey;
			if ($c > 0) {
				$errfields .= ', ';
			}
			$c++;
		}
		// count the key up one at a time.
		$c++;
	}

	// build the string to spit back to the membership page
	$string = '?error='.$error.$errfields.'&'.$field;

	// if we're good and there wasn't an error, go ahead and add the person to the list -- if they're already on the list, update their information with what they give us here.
	if (empty($error)) {

		$retval = $api->listUpdateMember($listID, $email, $mergeFields, 'html', false);
		if ($retval == false) {
			$retval = $api->listSubscribe( $listID, $email, $mergeFields, 'html', false, false);

			if ($retval == true) {
				setcookie("otmg_member", 'true', strtotime( '+30 days' ), '/');
				header('Location: '.get_permalink($otmg_settings['protected_page']));
			}
		} else {
			setcookie("otmg_member", 'true', strtotime( '+30 days' ), '/');
			header('Location: '.get_permalink($otmg_settings['protected_page']));			
		}
	} else {
		header('Location: '.get_permalink($otmg_settings['login_page']).$string);
	}
	
}