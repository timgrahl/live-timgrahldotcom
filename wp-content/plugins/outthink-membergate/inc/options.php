<?php	
require_once( trailingslashit( plugin_dir_path( __FILE__ ) ) . '/settings_callbacks.php' );
require_once( trailingslashit( plugin_dir_path( __FILE__ ) ) . '/mcapi/MCAPI.class.php' );
class OT_Membergate_Settings {

	public function __construct() {
		add_action( 'admin_menu', array($this, 'admin_menu') );
		add_action( 'admin_init', array($this, 'admin_init') );
	}

	// initialize admin menu:
	function admin_menu() {
	    add_options_page( 'Out:think Membergate', 'Out:think Membergate', 'manage_options', 'ot_membergate', array($this, 'options_page') );
	}

	function admin_init() {
		$userinfo = (array)get_option('ot-plugin-validation');
		$otmg_settings = (array)get_option('otmg_settings');
		register_setting( 'ot_membergate', 'ot-plugin-validation' );
		register_setting( 'ot_membergate', 'otmg_settings');
	    add_settings_section( 'section-one', 'Registration Info', array($this, 'section_one_callback'), 'ot_membergate' );
		// adding the Username Field
		add_settings_field( 'user', 'Username', array('OTS_Framework','ots_text_input'), 'ot_membergate', 'section-one', array(
		    'name' => 'ot-plugin-validation[user]',
		    'value' => $userinfo['user'],
		) );
		add_settings_field( 'email', 'Email', array('OTS_Framework','ots_text_input'), 'ot_membergate', 'section-one', array(
		    'name' => 'ot-plugin-validation[email]',
		    'value' => $userinfo['email'],
		) );
		// Begin section two here
	    add_settings_section( 'section-two', 'Membergate Settings', array($this, 'section_two_callback'), 'ot_membergate' );

		/**
		* ots_page_dropdown() creates a page dropdown select field accepts parameters as array
		* 'name', 'title', 'value', 'help';
		*/
		add_settings_field( 'protected_page', 'Page to protect', array('OTS_Framework', 'ots_page_dropdown'), 'ot_membergate', 'section-two', array(
		    'name' => 'otmg_settings[protected_page]',
		    'value' => $otmg_settings['protected_page'],
			'help' => '<small>Page where protected content will be seen</small><br /><br /><strong style="font-size: 120%;">~ OR ~</strong>'
		) );
		add_settings_field( 'protected_slug', 'Slug to protect', array('OTS_Framework','ots_text_input'), 'ot_membergate', 'section-two', array(
		    'name' => 'otmg_settings[protected_slug]',
		    'value' => $otmg_settings['protected_slug'],
			'help' => '<small>If this is set, Membergate will protect anything with this slug in the URL.</small>'
		) );
		add_settings_field( 'login_page', 'Login Page', array('OTS_Framework','ots_page_dropdown'), 'ot_membergate', 'section-two', array(
		    'name' => 'otmg_settings[login_page]',
		    'value' => $otmg_settings['login_page'],
			'help' => '<small>Page where members will login and register</small>'
		) );
/** ots_checkbox() accepts parameters as array
* 'name', 'title', 'value', 'help';	
**/
		add_settings_field( 'otmg_disable_css', 'Disable Plugin CSS', array('OTS_Framework','ots_checkbox'), 'ot_membergate', 'section-two', array(
		    'name' => 'otmg_settings[otmg_disable_css]',
			'help' => '<small><em>Advanced:</em> If you check this box, it will disable the styling for the login forms.</small>',
			'value' => 'true',
			'check' => $otmg_settings['otmg_disable_css']
		));
		
		add_settings_field( 'otmg_disable_filter', 'Disable Content Filter', array('OTS_Framework','ots_checkbox'), 'ot_membergate', 'section-two', array(
		    'name' => 'otmg_settings[otmg_disable_filter]',
			'help' => '<small><em>Advanced:</em> If you check this box, the login form will not automatically be added to the login page.</small>',
			'value' => 'true',
			'check' => $otmg_settings['otmg_disable_filter']
		));
		
		add_settings_field( 'connect_method', 'Connection Method', array('OTS_Framework','ots_select'), 'ot_membergate', 'section-two', array(
		    'name' => 'otmg_settings[connect_method]',
			'title' => 'Select your connection method',
		    'value' => $otmg_settings['connect_method'],
			'options' => array(
				'infusionsoft' => 'Infusionsoft',
				'mailchimp' => 'Mailchimp',
			),
			'help' => '<small>This plugin will not work until this is set and configured.</small>'
		) );
		if (!empty($otmg_settings['connect_method']) && $otmg_settings['connect_method'] !== '0') {
			add_settings_section( 'section-three', ucfirst($otmg_settings['connect_method']).' Connection Details', array($this, 'section_three_callback'), 'ot_membergate' );
			add_settings_field( 'api_key', 'Enter your API Key', array('OTS_Framework','ots_text_input'), 'ot_membergate', 'section-three', array(
				'name' => 'otmg_settings[api_key]',
			    'value' => $otmg_settings['api_key']
			));
			if ($otmg_settings['connect_method'] == 'infusionsoft') :
				add_settings_field( 'is_app_name', 'Infusionsoft App Name', array($this, 'ots_ISapp_name'), 'ot_membergate', 'section-three' );
			elseif ($otmg_settings['connect_method'] == 'mailchimp' && !empty($otmg_settings['api_key'])) :
				add_settings_field( 'mc_list_id', 'Mailchimp List', array($this, 'ots_mc_listselect'), 'ot_membergate', 'section-three', array(
					'apikey' => $otmg_settings['api_key'],
					'value' => $otmg_settings['mc_list_id'],
				));
			endif;
			
		}

	}
	function section_one_callback() { ?>
		<p>Enter your Out:think Group username and email to enable automatic updates of this plugin.</p>
		<?php
	}
	function section_two_callback() { ?>
		<p>Please configure your plugin below.</p>
		<?php
	}
	function section_three_callback() { ?>
		<p>Connect your membergate!</p>
		<?php
	}
	
	function ots_ISapp_name() {
		$otmg_settings = (array)get_option('otmg_settings'); ?>
			<input type="text" name="otmg_settings[is_app_name]" value="<?php echo $otmg_settings['is_app_name']; ?>" id="is_app_name"><br>
			<small>This appears before infusionsoft.com on your account</small>
	<?php }
	function ots_mc_listselect($args) {
		$apiKey = $args['apikey'];
		$listID = $args['value'];
		$api = new MCAPI($apiKey);
		$retval = $api->lists();
	//	print_r($retval);
		if ($api->errorCode){
			echo "Unable to load lists! ";
			echo $api->errorMessage;
		} else { ?>
			<select name="otmg_settings[mc_list_id]" id="listID">
			<?php
				foreach ($retval['data'] as $list) { ?>
				<option value="<?php echo $list['id']; ?>"<?php if ($listID == $list['id']): ?> selected="selected"<?php endif; ?>><?php echo $list['name']. ' (' . $list['stats']['member_count'].' subs)'; ?></option>
			<?php } // end for ?>
			</select><br>
			<?php
		} 
	}
	function options_page() {
	?>	
	    <div class="wrap">
	        <h2>Out:think Membergate Options</h2>
	        <form action="options.php" method="POST">
	            <?php settings_fields( 'ot_membergate' ); ?>
	            <?php do_settings_sections( 'ot_membergate' ); ?>
	            <?php submit_button(); ?>
	        </form>
	    </div>
	    <?php
	}
	public function active() {
		/* get wp version */
		global $wp_version;
		$otpu =  new OT_Plugin_Updater();		
		$updater_data =$otpu->updater->updater_data();

		/* get current domain */
		$domain = $updater_data['domain'];
		$userinfo = get_option('ot-plugin-validation');
		$key = $userinfo['email'];
		$username = $userinfo['user'];

		$valid = "invalid";

		if( empty($key) || empty($username) ) return $valid;

		/* Get data from server */
		$remote_url = add_query_arg( array( 'plugin_repo' => $updater_data['repo_slug'], 'ahr_check_key' => 'validate_key' ), $updater_data['repo_uri'] );
		$remote_request = array( 'timeout' => 20, 'body' => array( 'key' => md5( $key ), 'login' => $username, 'autohosted' => $updater_data['autohosted'] ), 'user-agent' => 'WordPress/' . $wp_version . '; ' . $updater_data['domain'] );
		$raw_response = wp_remote_post( $remote_url, $remote_request );

		/* get response */
		$response = '';
		if ( !is_wp_error( $raw_response ) && ( $raw_response['response']['code'] == 200 ) )
			$response = trim( wp_remote_retrieve_body( $raw_response ) );

		/* if call to server sucess */
		if ( !empty( $response ) ){

			/* if key is valid */
			if ( $response == 'valid' ) $valid = 'valid';

			/* if key is not valid */
			elseif ( $response == 'invalid' ) $valid = 'invalid';

			/* if response is value is not recognized */
			else $valid = 'unrecognized';
		}

		return $valid;
	}
}
$OT_Membergate_Settings = new OT_Membergate_Settings();