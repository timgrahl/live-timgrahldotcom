=== Plugin Name ===
Requires at least: 3.7
Tested up to: 3.9
Stable tag: 1.0.1
License: GPLv2

Out:think Membergate gives users the ability to protect a resources section by use of redirection and cookies

== Description ==

Out:think Membergate gives users the ability to protect a resources section by use of redirection and cookies

== Installation ==

1. Install the plugin by uploading it through the "Plugins" menu in WordPress
2. Activate the plugin
3. Add your pages
4. Set up your 

== Frequently Asked Questions ==

N/A

== Screenshots ==

N/A

== Changelog ==

*1.0*

* Fixed some things that might cause issues with caching
* Added some contextual styling for forms, specifically added some margin to the bottom of the inputs.

*0.9*

Initial release

== Upgrade Notice ==

Upgrading will not break your exising settings or widgets.