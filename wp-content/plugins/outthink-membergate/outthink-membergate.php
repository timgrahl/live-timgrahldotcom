<?php
/*
Plugin Name: Out:think Membergate
Plugin URI: http://outthinkgroup.com/
Description: This plugin is built to allow only subscribers access to various portions of the site.
Author: Joseph Hinson of Out:think Group
Version: 1.0.1
Author URI: http://outthinkgroup.com/
*/

require_once( trailingslashit( plugin_dir_path( __FILE__ ) ) . 'inc/options.php' );
include 'inc/ot-nlsignup.php';
include 'inc/form_functions.php';

// hook for protected page to be redirected IF cookie doesn't exist
function otmg_protected_page_redirect() {
	$urlstr = $_SERVER['REQUEST_URI'];
	$otmg_settings = get_option('otmg_settings');
	if (!empty($otmg_settings['protected_slug'])) {
		$protected = strpos($urlstr, $otmg_settings['protected_slug']);
	}
	if (is_page($otmg_settings['protected_page'] or is_page($otmg_settings['login_page']) or $protected)) {
		define('DONOTCACHEPAGE', true);
		define('DONOTCACHEDB', true);
		define('DONOTMINIFY', true);
		define('DONOTCDN', true);
		define('DONOTCACHCEOBJECT', true);
	}
    if( (is_page($otmg_settings['protected_page']) or $protected) and !is_user_logged_in()) {
		 if (!isset($_COOKIE["otmg_member"])) {
	        wp_redirect( get_permalink($otmg_settings['login_page']));
		 }
	}
}
add_action( 'template_redirect', 'otmg_protected_page_redirect' );

include 'inc/shortcodes.php';

add_filter('the_content', 'otmg_content_filter');
function otmg_content_filter($content) {
	$newcontent = '';
	$otmg_settings = get_option('otmg_settings');
	// setting up just the content
	$newcontent .= $content;
	
	// if the content filter has NOT been disabled, add the login/registration forms after the content
	if (is_page($otmg_settings['login_page'])) {
		if ($otmg_settings['otmg_disable_filter'] !== 'true') {
			$newcontent .= '<div class="otmg_login_form otmg_container">'.do_shortcode('[otmg_login]').'</div>';
			$newcontent .= '<div class="otmg_register_form otmg_container">'.do_shortcode('[otmg_register]') .'</div>';
		}
	}
	return $newcontent;
}

add_action('wp_head','otmg_css');
function otmg_css() {
	$otmg_settings = get_option('otmg_settings');
	?>
	<?php
	if ($otmg_settings['otmg_disable_css'] !== 'true') { ?>
		<style type="text/css" media="screen">
		.otmg_container {
		    float: left;
		    width: 46%;
		    margin-right: 4%;
		}
		.otmg_container form {
		    padding: 30px;
		    background: #f1f1f1;
		    border: 1px solid #ddd;
		}
		.otmg_form input {
			margin-bottom: 10px;
		}
		.otmg_form label {
			display:none;
		}
		</style>
		<!--[if lte IE 7]>
		<style type="text/css" media="screen">
			.otmg_form input {
				margin-bottom: 2px;
			}
			.otmg_form input#input {
				margin-bottom: 10px;
			}
			.otmg_form label {
				display: inline-block;
			}
		</style>
		<![endif]-->
		
	<?php }
}

/**
 * Load and Activate Plugin Updater Class.
 */
function ot_membergate_plugin_updater_init() {
	// this should definitely not be here...so I'll move this in the next version
	add_filter('widget_text', 'do_shortcode');
	
	/* Load Plugin Updater */
	require_once( trailingslashit( plugin_dir_path( __FILE__ ) ) . 'inc/plugin-updater.php' );
	$userinfo = get_option('ot-plugin-validation');
	
	/* Updater Config */
	$config = array(
		'base'      => plugin_basename( __FILE__ ), //required
		'username'    => $userinfo['user'], // user login name in your site.
		'key' => $userinfo['email'],
		'repo_uri'  => 'http://outthinkgroup.com/training/aps/',
		'repo_slug' => 'outthink-membergate',
	);

	/* Load Updater Class */
	new OT_Plugin_Updater( $config );
}
add_action( 'init', 'ot_membergate_plugin_updater_init' );

add_action('send_headers', 'otmg_member_login', 10);

function otmg_member_login() {
	if ($_POST['method'] == 'login') {
		$otmg_settings = get_option('otmg_settings');
		if ($otmg_settings['connect_method'] == 'infusionsoft') {
			require_once("infusionsoft-sdk/src/isdk.php");
			otmg_is_login_process($otmg_settings);
		} elseif($otmg_settings['connect_method'] == 'mailchimp') {
			require_once('inc/mcapi/MCAPI.class.php');
			otmg_mc_login_process($otmg_settings);
		} else {
			header('Location: '.get_bloginfo('url'));
			exit;
		}
	} elseif ($_POST['method'] == 'register') {
		$otmg_settings = get_option('otmg_settings');
		if ($otmg_settings['connect_method'] == 'infusionsoft') {
			require_once("infusionsoft-sdk/src/isdk.php");
			otmg_is_register($otmg_settings);
			
		} else {
			otmg_mc_register($otmg_settings);
		}
	}

}
