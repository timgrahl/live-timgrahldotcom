<?php
/*
Plugin Name: Resources
Plugin URI: http://outthinkgroup.com
Description: This plugin adds a resources post type to the site
Author: Joseph Hinson
Version: 1.0
Author URI: http://outthinkgroup.com
*/
// adding action -- this all really needs to be in one place
add_action( 'init', 'register_resource_init' );
//custom post type for Resource
function register_resource_init() {
	$args = array(
		'label' => 'Resources',
		'description' => '',
		'public' => true,
		'exclude_from_search' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'show_in_nav_menus' => false,
		'capability_type' => 'post',
		'hierarchical' => false,
		'rewrite' => array('slug' => ''),
		'query_var' => true,
		'supports' => array('title','editor','excerpt','custom-fields','revisions','thumbnail',),
		'taxonomies' => array('type'),
		'labels' => array (
			'name' => 'Resources',
			'singular_name' => 'Resource',
			'menu_name' => 'Resources',
			'add_new' => 'Add Resource',
			'add_new_item' => 'Add New Resource',
			'edit' => 'Edit',
			'edit_item' => 'Edit Resource',
			'new_item' => 'New Resource',
			'view' => 'View Resource',
			'view_item' => 'View Resource',
			'search_items' => 'Search Resources',
			'not_found' => 'No Resources Found',
			'not_found_in_trash' => 'No Resources Found in Trash',
			'parent' => 'Parent Resource',
		),
	);
	register_post_type('resources', $args);
	// Taxonomy for "Types" -- works like tags
	register_taxonomy( 'type', 'resources', array( 'hierarchical' => true, 'label' => 'Resource Type', 'query_var' => true, 'rewrite' => array('slug' => 'resource_category'), 'singular-label' => 'Type', 'show_admin_column' => true) );	
	add_action('manage_resources_posts_custom_column', 'manage_resources_columns', 10, 2);
	add_filter('manage_edit-resources_columns', 'add_new_product_columns');	
}
// this function is building the array of columns to accept
function add_new_product_columns($product_columns) {
		$new_columns['cb'] = '<input type="checkbox" />';
		$new_columns['title'] = _x('Resource Name', 'column name');
//		$new_columns['type'] = '<a href="edit.php?post_type=resources&orderby=program_category&order=asc">Resource Type</a>';
 		$new_columns['rFile'] = __('Resource URL');
		$new_columns['featured_image'] = __('Thumbnail');
 
		return $new_columns;
	}
// Add to admin_init function - for resources post column
	function manage_resources_columns($column_name, $id) {
		global $wpdb;
		switch ($column_name) {
		case 'rFile':
			echo get_post_meta($id, 'resource_url', true);
	        break;
 
		case 'type':
			// Get number of images in gallery
			$cats = get_the_terms( $id, 'type');
			if ($cats) {
				foreach ($cats as $cat) {
					echo '<a href="edit.php?post_type=resources&orderby=type&order=asc&type='.$cat->slug.'">'.$cat->name.'</a> ';
				} // endfor

			} else {
				echo '<span style="color:red;">No Type Assigned</span>';
			}
			break;
		case 'featured_image':
		 echo '<div style="padding:10px;"><a href="post.php?post='.$id.'&action=edit">'.get_the_post_thumbnail($id, 'thumbnail').'</a></div>';
		
		break;
		default:
			break;
		} // end switch
	}

	function resource_url() {
		global $post;
		$resourceurl = get_post_meta($post->ID, 'resource_url', true);
		echo $resourceurl;
	}
	/* Define the custom box */
	
	// WP 3.0+
	add_action('add_meta_boxes', 'ot_resources_meta_box');
	
	/* Do something with the data entered */
	add_action('save_post', 'ot_resources_save_postdata');
	
	/* Adds a box to the main column on the Post and Page edit screens */
	function ot_resources_meta_box() {
	    add_meta_box( 'ot_resources_sectionid', __( 'Resource Information', 'ot_resources_textdomain' ), 'ot_resources_inner_custom_box','resources', 'normal', 'high');
	}
	
	/* Prints the box content */
	function ot_resources_inner_custom_box() {
	
	  // Use nonce for verification
	  wp_nonce_field( plugin_basename(__FILE__), 'ot_resources_noncename' );
	
		global $post;
		$resource_url = get_post_meta($post->ID, 'resource_url', true);
	
	  // The actual fields for data entry ?>
	<table border="0" cellspacing="5" cellpadding="5" width="100%">
		<tr>
		<td style="width: 120px;">
			<label for="resource_url">Resource URL:</label>
		</td>
		<td>
			<input type="text" name="resource_url" size="40" value="<?php echo $resource_url; ?>" id="resource_url"><br />
			<small>(ex: http://example.com)</small>
		</td>
		</tr>
	</table>
	<?php
	}
	
	/* When the post is saved, saves our custom data */
	function ot_resources_save_postdata( $post_id ) {
	
	  // verify this came from the our screen and with proper authorization,
	  // because save_post can be triggered at other times
	  if (isset($_POST['ot_resources_noncename'])) {
		  if ( !wp_verify_nonce( $_POST['ot_resources_noncename'], plugin_basename(__FILE__) ) )
	      	return $post_id;
	  }
	  // verify if this is an auto save routine. 
	  // If it is our form has not been submitted, so we dont want to do anything
	  if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) 
	      return $post_id;
	
	  
	  // Check permissions
	  if (isset($_POST['post_type'])) {
		  if ( 'resources' == $_POST['post_type'] ) 
		  {
		    if ( !current_user_can( 'edit_page', $post_id ) )
		        return $post_id;
		  }
		  else
		  {
		    if ( !current_user_can( 'edit_post', $post_id ) )
		        return $post_id;
		  }
	  }
	  // OK, we're authenticated: we need to find and save the data
	  $resource_url = '';
	  if (isset($_POST['resource_url'])) {
	  	$resource_url = $_POST['resource_url'];
	  }
	  // update the data
			update_post_meta($post_id, 'resource_url', $resource_url);
	}
	
	// [resources]
	function show_resources($atts) {
			extract(shortcode_atts(array(
				"orderby" => "menu_order"
	        ), $atts));
			$return = '';
			$resources = get_posts('numberposts=-1&orderby='.$orderby.'&order=ASC&post_type=resources&post_status=publish');
			if (!empty($resources)) {
				$c = 1;
				$return .='<div class="row resources-wrapper">';
				foreach ($resources as $resource) {
					$resourceurl = get_post_meta($resource->ID, 'resource_url', true);
					$return .= '
						<div class="span4 resource ta-center num-'. $c%3 .'">
					<div class="resource-inner">
							<a href="'.$resourceurl.'">'.get_the_post_thumbnail($resource->ID, "medium", array("class" => "resource-thumb")).'
								<h4 class="resource-title">'.$resource->post_title.'</h4>
							</a>
						</div>
					</div><!--end resource-inner -->';
					if ($c%3 == 0) {
						$return .= '<div class="clr hr-blue"></div>';
					}
					$c++;
				}
			$return .= '</div><!--END row-->';
			}
			return $return;
	}
	add_shortcode("resources", "show_resources");
	// end shortcode